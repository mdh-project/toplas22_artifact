#include "mcc_nhwc_krsc_npqk_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_filter[17][3][3][3];
    __shared__ float shared_in[1][4][34][3];
    __shared__ float shared_out[1][2][32][17];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c0 = b0; c0 <= 15; c0 += 13)
      for (int c2 = 0; c2 <= 223; c2 += 32) {
        if (t2 <= 2)
          for (int c4 = 0; c4 <= 3; c4 += 1)
            for (int c5 = 0; c5 <= 33; c5 += 1)
              shared_in[0][c4][c5][t2] = in[((c0 * 226 + (2 * b1 + c4)) * 226 + (c2 + c5)) * 3 + t2];
        for (int c3 = 0; c3 <= 63; c3 += 17) {
          if (t2 <= 2)
            for (int c4 = 0; c4 <= ppcg_min(16, -c3 + 63); c4 += 1)
              for (int c5 = 0; c5 <= 2; c5 += 1)
                for (int c6 = 0; c6 <= 2; c6 += 1)
                  shared_filter[c4][c5][c6][t2] = filter[(((c3 + c4) * 3 + c5) * 3 + c6) * 3 + t2];
          if (t2 <= 16 && t2 + c3 <= 63)
            for (int c5 = 0; c5 <= 1; c5 += 1)
              for (int c6 = 0; c6 <= 31; c6 += 1)
                shared_out[0][c5][c6][t2] = out[((c0 * 224 + (2 * b1 + c5)) * 224 + (c2 + c6)) * 64 + (t2 + c3)];
          __syncthreads();
          for (int c6 = 0; c6 <= 1; c6 += 1)
            for (int c8 = 0; c8 <= ppcg_min(16, -c3 + 63); c8 += 1)
              for (int c9 = 0; c9 <= 2; c9 += 1)
                for (int c10 = 0; c10 <= 2; c10 += 1)
                  for (int c11 = 0; c11 <= 2; c11 += 1)
                    shared_out[0][c6][t2][c8] += (shared_filter[c8][c10][c11][c9] * shared_in[0][c6 + c10][t2 + c11][c9]);
          __syncthreads();
          if (t2 <= 16 && t2 + c3 <= 63)
            for (int c5 = 0; c5 <= 1; c5 += 1)
              for (int c6 = 0; c6 <= 31; c6 += 1)
                out[((c0 * 224 + (2 * b1 + c5)) * 224 + (c2 + c6)) * 64 + (t2 + c3)] = shared_out[0][c5][c6][t2];
        }
      }
}
