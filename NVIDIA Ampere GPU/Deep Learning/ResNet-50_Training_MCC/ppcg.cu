#include "mcc_nhwc_krsc_npqk_stride_2_asymmetrical_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 111; c2 += 32)
      for (int c3 = 0; c3 <= 63; c3 += 32)
        for (int c6 = t1; c6 <= ppcg_min(31, -32 * b1 + 111); c6 += 4)
          for (int c7 = t2; c7 <= ppcg_min(31, -c2 + 111); c7 += 4)
            for (int c8 = 0; c8 <= 31; c8 += 1)
              for (int c9 = 0; c9 <= 2; c9 += 1)
                for (int c10 = 0; c10 <= 6; c10 += 1)
                  for (int c11 = 0; c11 <= 6; c11 += 1)
                    out[((t0 * 112 + (32 * b1 + c6)) * 112 + (c2 + c7)) * 64 + (c3 + c8)] += (filter[(((c3 + c8) * 7 + c10) * 7 + c11) * 3 + c9] * in[((t0 * 230 + (64 * b1 + 2 * c6 + c10)) * 230 + (2 * c2 + 2 * c7 + c11)) * 3 + c9]);
}
