#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    float private_C[1][1];

    if (6 * b0 + t0 <= 15) {
      private_C[0][0] = C[(6 * b0 + t0) * 4096 + (32 * b1 + t1)];
      for (int c5 = 0; c5 <= 25087; c5 += 1)
        private_C[0][0] += (A[(6 * b0 + t0) * 25088 + c5] * B[c5 * 4096 + (32 * b1 + t1)]);
      C[(6 * b0 + t0) * 4096 + (32 * b1 + t1)] = private_C[0][0];
    }
}
