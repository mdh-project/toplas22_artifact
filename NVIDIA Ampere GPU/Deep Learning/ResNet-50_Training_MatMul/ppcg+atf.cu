#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[1][1113];
    float private_C[1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (84 * b1 + t1 <= 999)
        private_C[0][0] = C[b0 * 1000 + (84 * b1 + t1)];
      for (int c2 = 0; c2 <= 2047; c2 += 1113) {
        for (int c4 = t1; c4 <= ppcg_min(1112, -c2 + 2047); c4 += 84)
          shared_A[0][c4] = A[b0 * 2048 + (c2 + c4)];
        __syncthreads();
        if (84 * b1 + t1 <= 999)
          for (int c5 = 0; c5 <= ppcg_min(1112, -c2 + 2047); c5 += 1)
            private_C[0][0] += (shared_A[0][c5] * B[(c2 + c5) * 1000 + (84 * b1 + t1)]);
        __syncthreads();
      }
      if (84 * b1 + t1 <= 999)
        C[b0 * 1000 + (84 * b1 + t1)] = private_C[0][0];
    }
}
