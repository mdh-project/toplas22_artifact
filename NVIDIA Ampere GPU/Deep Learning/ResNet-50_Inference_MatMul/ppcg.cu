#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    __shared__ float shared_A[1][686];
    float private_C[1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (72 * b0 + t0 <= 999)
        private_C[0][0] = C[0 * 1000 + (72 * b0 + t0)];
      for (int c1 = 0; c1 <= 2047; c1 += 686) {
        for (int c3 = t0; c3 <= ppcg_min(685, -c1 + 2047); c3 += 72)
          shared_A[0][c3] = A[0 * 2048 + (c1 + c3)];
        __syncthreads();
        if (72 * b0 + t0 <= 999)
          for (int c3 = 0; c3 <= ppcg_min(685, -c1 + 2047); c3 += 1)
            private_C[0][0] += (shared_A[0][c3] * B[(c1 + c3) * 1000 + (72 * b0 + t0)]);
        __syncthreads();
      }
      if (72 * b0 + t0 <= 999)
        C[0 * 1000 + (72 * b0 + t0)] = private_C[0][0];
    }
}
