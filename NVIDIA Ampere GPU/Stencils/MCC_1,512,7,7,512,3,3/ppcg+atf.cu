#include "mcc_nchw_kcrs_nkpq_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_in[1][32][3][7];
    float private_out[1][1][1][5];

    {
      if (9 * b0 + t0 <= 511) {
        private_out[0][0][0][0] = out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 0];
        private_out[0][0][0][1] = out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 1];
        private_out[0][0][0][2] = out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 2];
        private_out[0][0][0][3] = out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 3];
        private_out[0][0][0][4] = out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 4];
      }
      for (int c3 = 0; c3 <= 511; c3 += 32) {
        if (t0 <= 2)
          for (int c5 = 0; c5 <= 31; c5 += 1)
            for (int c7 = 0; c7 <= 6; c7 += 1)
              shared_in[0][c5][t0][c7] = in[((0 * 512 + (c3 + c5)) * 7 + (b1 + t0)) * 7 + c7];
        __syncthreads();
        if (9 * b0 + t0 <= 511)
          for (int c6 = 0; c6 <= 4; c6 += 1)
            for (int c7 = 0; c7 <= 31; c7 += 1)
              for (int c8 = 0; c8 <= 2; c8 += 1)
                for (int c9 = 0; c9 <= 2; c9 += 1)
                  private_out[0][0][0][c6] += (filter[(((9 * b0 + t0) * 512 + (c3 + c7)) * 3 + c8) * 3 + c9] * shared_in[0][c7][c8][c6 + c9]);
        __syncthreads();
      }
      if (9 * b0 + t0 <= 511) {
        out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 0] = private_out[0][0][0][0];
        out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 1] = private_out[0][0][0][1];
        out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 2] = private_out[0][0][0][2];
        out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 3] = private_out[0][0][0][3];
        out[((0 * 512 + (9 * b0 + t0)) * 5 + b1) * 5 + 4] = private_out[0][0][0][4];
      }
    }
}
