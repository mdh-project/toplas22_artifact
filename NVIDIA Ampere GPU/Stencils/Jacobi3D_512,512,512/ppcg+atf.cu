#include "jacobi_kernel.hu"
__global__ void kernel0(float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    for (int c0 = 2 * b0; c0 <= 509; c0 += 502)
      for (int c3 = 0; c3 <= 1; c3 += 1)
        for (int c4 = 0; c4 <= 1; c4 += 1)
          out[((c0 + c3) * 510 + (2 * b1 + c4)) * 510 + t2] = (((((((2.0f * in[((c0 + c3 + 1) * 512 + (2 * b1 + c4 + 1)) * 512 + (t2 + 2)]) + (3.0f * in[((c0 + c3 + 1) * 512 + (2 * b1 + c4 + 1)) * 512 + t2])) + (4.0f * in[((c0 + c3 + 1) * 512 + (2 * b1 + c4 + 2)) * 512 + (t2 + 1)])) + (5.0f * in[((c0 + c3 + 1) * 512 + (2 * b1 + c4)) * 512 + (t2 + 1)])) + (6.0f * in[((c0 + c3 + 2) * 512 + (2 * b1 + c4 + 1)) * 512 + (t2 + 1)])) + (7.0f * in[((c0 + c3) * 512 + (2 * b1 + c4 + 1)) * 512 + (t2 + 1)])) - (8.0f * in[((c0 + c3 + 1) * 512 + (2 * b1 + c4 + 1)) * 512 + (t2 + 1)]));
}
