#include "jacobi_kernel.hu"
__global__ void kernel0(float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    if (32 * b0 + t0 <= 509)
      for (int c2 = 0; c2 <= 509; c2 += 32)
        for (int c4 = t1; c4 <= ppcg_min(31, -32 * b1 + 509); c4 += 4)
          for (int c5 = t2; c5 <= ppcg_min(31, -c2 + 509); c5 += 4)
            out[((32 * b0 + t0) * 510 + (32 * b1 + c4)) * 510 + (c2 + c5)] = (((((((2.0f * in[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5 + 2)]) + (3.0f * in[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5)])) + (4.0f * in[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4 + 2)) * 512 + (c2 + c5 + 1)])) + (5.0f * in[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4)) * 512 + (c2 + c5 + 1)])) + (6.0f * in[((32 * b0 + t0 + 2) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5 + 1)])) + (7.0f * in[((32 * b0 + t0) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5 + 1)])) - (8.0f * in[((32 * b0 + t0 + 1) * 512 + (32 * b1 + c4 + 1)) * 512 + (c2 + c5 + 1)]));
}
