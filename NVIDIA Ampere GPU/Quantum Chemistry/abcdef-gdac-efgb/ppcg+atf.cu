#include "tc_abcdef_gdac_efgb_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_B[1][13][24][1];
    __shared__ float shared_C[1][1][16][8][1][13];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c3 = 0; c3 <= 15; c3 += 8)
      for (int c4 = 0; c4 <= 23; c4 += 1)
        for (int c5 = 0; c5 <= 15; c5 += 13) {
          if (t2 == 0)
            for (int c7 = 0; c7 <= ppcg_min(12, -c5 + 15); c7 += 1)
              for (int c8 = 0; c8 <= 23; c8 += 1)
                shared_B[0][c7][c8][0] = B[((c4 * 16 + (c5 + c7)) * 24 + c8) * 16 + b1];
          if (t2 <= 12 && t2 + c5 <= 15)
            for (int c8 = 0; c8 <= 15; c8 += 1)
              for (int c9 = 0; c9 <= 7; c9 += 1)
                shared_C[0][0][c8][c9][0][t2] = C[((((b0 * 16 + b1) * 16 + c8) * 16 + (c3 + c9)) * 24 + c4) * 16 + (t2 + c5)];
          __syncthreads();
          for (int c10 = 0; c10 <= 7; c10 += 1)
            for (int c12 = 0; c12 <= ppcg_min(12, -c5 + 15); c12 += 1)
              for (int c13 = 0; c13 <= 23; c13 += 1)
                shared_C[0][0][t2][c10][0][c12] += (A[((c13 * 16 + (c3 + c10)) * 24 + b0) * 16 + t2] * shared_B[0][c12][c13][0]);
          __syncthreads();
          if (t2 <= 12 && t2 + c5 <= 15)
            for (int c8 = 0; c8 <= 15; c8 += 1)
              for (int c9 = 0; c9 <= 7; c9 += 1)
                C[((((b0 * 16 + b1) * 16 + c8) * 16 + (c3 + c9)) * 24 + c4) * 16 + (t2 + c5)] = shared_C[0][0][c8][c9][0][t2];
        }
}
