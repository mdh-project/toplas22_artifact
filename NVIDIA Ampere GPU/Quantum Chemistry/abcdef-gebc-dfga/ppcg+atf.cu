#include "tc_abcdef_gebc_dfga_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[24][1][1][16];
    __shared__ float shared_B[1][16][24][1];
    __shared__ float shared_C[1][1][16][1][1][16];

    for (int c3 = 0; c3 <= 23; c3 += 1) {
      if (t2 == 0)
        for (int c5 = 0; c5 <= 15; c5 += 1)
          for (int c6 = 0; c6 <= 23; c6 += 1)
            shared_B[0][c5][c6][0] = B[((c3 * 16 + c5) * 24 + c6) * 24 + b0];
      for (int c4 = 0; c4 <= 15; c4 += 1) {
        for (int c5 = 0; c5 <= 23; c5 += 1)
          shared_A[c5][0][0][t2] = A[((c5 * 16 + c4) * 16 + b1) * 16 + t2];
        for (int c7 = 0; c7 <= 15; c7 += 1)
          shared_C[0][0][c7][0][0][t2] = C[((((b0 * 16 + b1) * 16 + c7) * 24 + c3) * 16 + c4) * 16 + t2];
        __syncthreads();
        for (int c12 = 0; c12 <= 15; c12 += 1)
          for (int c13 = 0; c13 <= 23; c13 += 1)
            shared_C[0][0][t2][0][0][c12] += (shared_A[c13][0][0][t2] * shared_B[0][c12][c13][0]);
        __syncthreads();
        for (int c7 = 0; c7 <= 15; c7 += 1)
          C[((((b0 * 16 + b1) * 16 + c7) * 24 + c3) * 16 + c4) * 16 + t2] = shared_C[0][0][c7][0][0][t2];
      }
    }
}
