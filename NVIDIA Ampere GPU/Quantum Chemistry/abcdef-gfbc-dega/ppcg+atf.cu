#include "tc_abcdef_gfbc_dega_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_B[3][1][24][1];
    __shared__ float shared_C[1][1][16][3][1][8];

    for (int c3 = 0; c3 <= 23; c3 += 3)
      for (int c4 = 0; c4 <= 15; c4 += 1) {
        if (t2 == 0)
          for (int c5 = 0; c5 <= 2; c5 += 1)
            for (int c7 = 0; c7 <= 23; c7 += 1)
              shared_B[c5][0][c7][0] = B[(((c3 + c5) * 16 + c4) * 24 + c7) * 24 + b0];
        __syncthreads();
        for (int c5 = 0; c5 <= 15; c5 += 8) {
          if (t2 <= 7)
            for (int c8 = 0; c8 <= 15; c8 += 1)
              for (int c9 = 0; c9 <= 2; c9 += 1)
                shared_C[0][0][c8][c9][0][t2] = C[((((b0 * 16 + b1) * 16 + c8) * 24 + (c3 + c9)) * 16 + c4) * 16 + (t2 + c5)];
          __syncthreads();
          for (int c10 = 0; c10 <= 2; c10 += 1)
            for (int c12 = 0; c12 <= 7; c12 += 1)
              for (int c13 = 0; c13 <= 23; c13 += 1)
                shared_C[0][0][t2][c10][0][c12] += (A[((c13 * 16 + (c5 + c12)) * 16 + b1) * 16 + t2] * shared_B[c10][0][c13][0]);
          __syncthreads();
          if (t2 <= 7)
            for (int c8 = 0; c8 <= 15; c8 += 1)
              for (int c9 = 0; c9 <= 2; c9 += 1)
                C[((((b0 * 16 + b1) * 16 + c8) * 24 + (c3 + c9)) * 16 + c4) * 16 + (t2 + c5)] = shared_C[0][0][c8][c9][0][t2];
        }
        __syncthreads();
      }
}
