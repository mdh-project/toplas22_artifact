#include "prl_kernel.hu"
__global__ void kernel0(int *__pet_test_40, int *id_measure, str46 *m_birthday, int *m_birthmonth, str46 *m_birthname1, str46 *m_birthname2, str46 *m_birthname3, int *m_birthyear, int *m_cin, str46 *m_firstname1, str46 *m_firstname2, str46 *m_firstname3, long *m_firstname_group1, long *m_firstname_group2, long *m_firstname_group3, chr2 *m_gender, long *m_id, str46 *m_lastname1, str46 *m_lastname2, str46 *m_lastname3, double *m_prob_birthday, double *m_prob_birthmonth, double *m_prob_birthname1, double *m_prob_birthname2, double *m_prob_birthname3, double *m_prob_birthyear, double *m_prob_cin, double *m_prob_firstname1, double *m_prob_firstname2, double *m_prob_firstname3, double *m_prob_gender, double *m_prob_lastname1, double *m_prob_lastname2, double *m_prob_lastname3, long *match_id, double *match_weight, str46 *n_birthday, int *n_birthmonth, str46 *n_birthname1, str46 *n_birthname2, str46 *n_birthname3, int *n_birthyear, int *n_cin, str46 *n_firstname1, str46 *n_firstname2, str46 *n_firstname3, long *n_firstname_group1, long *n_firstname_group2, long *n_firstname_group3, chr2 *n_gender, long *n_id, str46 *n_lastname1, str46 *n_lastname2, str46 *n_lastname3, double *n_prob_birthday, double *n_prob_birthmonth, double *n_prob_birthname1, double *n_prob_birthname2, double *n_prob_birthname3, double *n_prob_birthyear, double *n_prob_cin, double *n_prob_firstname1, double *n_prob_firstname2, double *n_prob_firstname3, double *n_prob_gender, double *n_prob_lastname1, double *n_prob_lastname2, double *n_prob_lastname3, dbl8 *probM)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    int private_n;
    int private_match;
    double private_weight;
    double private_pm;
    double private_puFirst_1;
    double private_puSecond_0;
    double private_puFirst_3;
    double private_puSecond_4;
    double private_pu_2;
    double private_puFirst_6;
    double private_puSecond_7;
    double private_pu_5;
    double private_puFirst_9;
    double private_puSecond_10;
    double private_pu_8;
    double private_puFirst_12;
    double private_puSecond_13;
    double private_pu_11;
    int private_tmp_id_measure;

    {
      for (private_n = 0; private_n < 262144; private_n += 1) {
        if (m_id[32 * b0 + t0] == n_id[private_n]) {
          continue;
        }
        private_match = 0;
        if (!private_match) {
          private_match = ((((eq_str46(m_lastname1[32 * b0 + t0].str, n_lastname1[private_n].str) && eq_str46(m_firstname1[32 * b0 + t0].str, n_firstname1[private_n].str)) && eq_str46(m_birthday[32 * b0 + t0].str, n_birthday[private_n].str)) && (m_birthmonth[32 * b0 + t0] == n_birthmonth[private_n])) && (m_birthyear[32 * b0 + t0] == n_birthyear[private_n]));
        }
        if (!private_match) {
          private_match = (((eq_str46(m_lastname1[32 * b0 + t0].str, n_lastname1[private_n].str) && (m_gender[32 * b0 + t0].values[0] == n_gender[private_n].values[0])) && (m_gender[32 * b0 + t0].values[1] == n_gender[private_n].values[1])) && (m_cin[32 * b0 + t0] == n_cin[private_n]));
        }
        if (!private_match) {
          private_match = (eq_str46(m_firstname1[32 * b0 + t0].str, n_firstname1[private_n].str) && eq_str46(m_birthday[32 * b0 + t0].str, n_birthday[private_n].str));
        }
        if (!private_match) {
          private_match = (eq_str46(m_firstname1[32 * b0 + t0].str, n_firstname1[private_n].str) && (m_birthmonth[32 * b0 + t0] == n_birthmonth[private_n]));
        }
        if (!private_match) {
          private_match = (eq_str46(m_firstname1[32 * b0 + t0].str, n_firstname1[private_n].str) && (m_birthyear[32 * b0 + t0] == n_birthyear[private_n]));
        }
        if (!private_match) {
          private_match = ((eq_str46(m_birthday[32 * b0 + t0].str, n_birthday[private_n].str) && (m_birthmonth[32 * b0 + t0] == n_birthmonth[private_n])) && (m_birthyear[32 * b0 + t0] == n_birthyear[private_n]));
        }
        if (!private_match) {
          private_match = eq_str46(m_lastname1[32 * b0 + t0].str, n_lastname1[private_n].str);
        }
        if (!private_match) {
          continue;
        }
        private_weight = 0.0;
        {
          private_pm = probM[0].values[0];
          private_weight += calculateCryptoPartLastnames(m_lastname1[32 * b0 + t0], m_lastname2[32 * b0 + t0], m_lastname3[32 * b0 + t0], n_lastname1[private_n], n_lastname2[private_n], n_lastname3[private_n], m_prob_lastname1[32 * b0 + t0], m_prob_lastname2[32 * b0 + t0], m_prob_lastname3[32 * b0 + t0], n_prob_lastname1[private_n], n_prob_lastname2[private_n], n_prob_lastname3[private_n], private_pm);
        }
        {
          private_pm = probM[0].values[1];
          private_weight += calculateCryptoPartFirstnames(m_firstname1[32 * b0 + t0], m_firstname2[32 * b0 + t0], m_firstname3[32 * b0 + t0], n_firstname1[private_n], n_firstname2[private_n], n_firstname3[private_n], m_prob_firstname1[32 * b0 + t0], m_prob_firstname2[32 * b0 + t0], m_prob_firstname3[32 * b0 + t0], n_prob_firstname1[private_n], n_prob_firstname2[private_n], n_prob_firstname3[private_n], m_firstname_group1[32 * b0 + t0], m_firstname_group2[32 * b0 + t0], m_firstname_group3[32 * b0 + t0], n_firstname_group1[private_n], n_firstname_group2[private_n], n_firstname_group3[private_n], private_pm);
        }
        {
          private_pm = probM[0].values[2];
          private_weight += calculateCryptoPartBirthnames(m_birthname1[32 * b0 + t0], m_birthname2[32 * b0 + t0], m_birthname3[32 * b0 + t0], n_birthname1[private_n], n_birthname2[private_n], n_birthname3[private_n], n_lastname1[private_n], n_lastname2[private_n], n_lastname3[private_n], m_prob_birthname1[32 * b0 + t0], m_prob_birthname2[32 * b0 + t0], m_prob_birthname3[32 * b0 + t0], n_prob_birthname1[private_n], n_prob_birthname2[private_n], n_prob_birthname3[private_n], private_pm);
        }
        {
          private_pm = probM[0].values[3];
          private_puFirst_1 = m_prob_birthday[32 * b0 + t0];
          if (private_puFirst_1 != 0.0) {
            private_puSecond_0 = n_prob_birthday[private_n];
            private_weight += calculatePartWeight(m_birthday[32 * b0 + t0], n_birthday[private_n], private_pm, private_puFirst_1, private_puSecond_0);
          }
        }
        {
          private_pm = probM[0].values[4];
          private_puFirst_3 = m_prob_birthmonth[32 * b0 + t0];
          private_puSecond_4 = n_prob_birthmonth[private_n];
          if ((private_puFirst_3 != 0.0) && (private_puSecond_4 != 0.0)) {
            private_pu_2 = (private_puFirst_3 < private_puSecond_4) ? private_puFirst_3 : private_puSecond_4;
            if (m_birthmonth[32 * b0 + t0] == n_birthmonth[private_n]) {
              private_weight += (log(private_pm / private_pu_2) / log(2.0));
            } else {
              private_weight += (log((1 - private_pm) / (1 - private_pu_2)) / log(2.0));
            }
          }
        }
        {
          private_pm = probM[0].values[5];
          private_puFirst_6 = m_prob_birthyear[32 * b0 + t0];
          private_puSecond_7 = n_prob_birthyear[private_n];
          if ((private_puFirst_6 != 0.0) && (private_puSecond_7 != 0.0)) {
            private_pu_5 = (private_puFirst_6 < private_puSecond_7) ? private_puFirst_6 : private_puSecond_7;
            if (m_birthyear[32 * b0 + t0] == n_birthyear[private_n]) {
              private_weight += (log(private_pm / private_pu_5) / log(2.0));
            } else {
              private_weight += (log((1 - private_pm) / (1 - private_pu_5)) / log(2.0));
            }
          }
        }
        {
          private_pm = probM[0].values[6];
          private_puFirst_9 = m_prob_gender[32 * b0 + t0];
          private_puSecond_10 = n_prob_gender[private_n];
          if ((private_puFirst_9 != 0.0) && (private_puSecond_10 != 0.0)) {
            private_pu_8 = (private_puFirst_9 < private_puSecond_10) ? private_puFirst_9 : private_puSecond_10;
            if ((m_gender[32 * b0 + t0].values[0] == n_gender[private_n].values[0]) && (m_gender[32 * b0 + t0].values[1] == n_gender[private_n].values[1])) {
              private_weight += (log(private_pm / private_pu_8) / log(2.0));
            } else {
              private_weight += (log((1 - private_pm) / (1 - private_pu_8)) / log(2.0));
            }
          }
        }
        {
          private_pm = probM[0].values[7];
          private_puFirst_12 = m_prob_cin[32 * b0 + t0];
          private_puSecond_13 = n_prob_cin[private_n];
          if ((private_puFirst_12 != 0.0) && (private_puSecond_13 != 0.0)) {
            private_pu_11 = (private_puFirst_12 < private_puSecond_13) ? private_puFirst_12 : private_puSecond_13;
            if (m_cin[32 * b0 + t0] == n_cin[private_n]) {
              private_weight += (log(private_pm / private_pu_11) / log(2.0));
            } else {
              private_weight += (log((1 - private_pm) / (1 - private_pu_11)) / log(2.0));
            }
          } else {
          }
        }
        private_tmp_id_measure = 0;
        if ((m_prob_lastname1[32 * b0 + t0] == 0.0) || (n_prob_lastname1[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_lastname1[32 * b0 + t0].str, n_lastname1[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_lastname2[32 * b0 + t0] == 0.0) || (n_prob_lastname2[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_lastname2[32 * b0 + t0].str, n_lastname2[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_lastname3[32 * b0 + t0] == 0.0) || (n_prob_lastname3[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_lastname3[32 * b0 + t0].str, n_lastname3[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_firstname1[32 * b0 + t0] == 0.0) || (n_prob_firstname1[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_firstname1[32 * b0 + t0].str, n_firstname1[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_firstname2[32 * b0 + t0] == 0.0) || (n_prob_firstname2[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_firstname2[32 * b0 + t0].str, n_firstname2[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_firstname3[32 * b0 + t0] == 0.0) || (n_prob_firstname3[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_firstname3[32 * b0 + t0].str, n_firstname3[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthname1[32 * b0 + t0] == 0.0) || (n_prob_birthname1[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_birthname1[32 * b0 + t0].str, n_birthname1[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthname2[32 * b0 + t0] == 0.0) || (n_prob_birthname2[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_birthname2[32 * b0 + t0].str, n_birthname2[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthname3[32 * b0 + t0] == 0.0) || (n_prob_birthname3[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_birthname3[32 * b0 + t0].str, n_birthname3[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthday[32 * b0 + t0] == 0.0) || (n_prob_birthday[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (eq_str46(m_birthday[32 * b0 + t0].str, n_birthday[private_n].str)) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_gender[32 * b0 + t0] == 0.0) || (n_prob_gender[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if ((m_gender[32 * b0 + t0].values[0] == n_gender[private_n].values[0]) && (m_gender[32 * b0 + t0].values[1] == n_gender[private_n].values[1])) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthmonth[32 * b0 + t0] == 0.0) || (n_prob_birthmonth[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (m_birthmonth[32 * b0 + t0] == n_birthmonth[private_n]) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_birthyear[32 * b0 + t0] == 0.0) || (n_prob_birthyear[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (m_birthyear[32 * b0 + t0] == n_birthyear[private_n]) {
            ++private_tmp_id_measure;
          }
        }
        if ((m_prob_cin[32 * b0 + t0] == 0.0) || (n_prob_cin[private_n] == 0.0)) {
          ++private_tmp_id_measure;
        } else {
          if (m_cin[32 * b0 + t0] == n_cin[private_n]) {
            ++private_tmp_id_measure;
          }
        }
        if (((private_weight >= 15.0) || (private_tmp_id_measure == 14)) && (private_weight > match_weight[32 * b0 + t0])) {
          match_id[32 * b0 + t0] = n_id[private_n];
          match_weight[32 * b0 + t0] = private_weight;
          id_measure[32 * b0 + t0] = private_tmp_id_measure;
        }
      }
      __pet_test_40[32 * b0 + t0] = ((match_weight[32 * b0 + t0] >= 15.0) || (id_measure[32 * b0 + t0] == 14));
    }
}
