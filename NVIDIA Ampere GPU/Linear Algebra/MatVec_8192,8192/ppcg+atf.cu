#include "matvec_row_major_n_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    __shared__ float shared_B[1604];
    float private_C[1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (76 * b0 + t0 <= 8191)
        private_C[0] = C[76 * b0 + t0];
      for (int c1 = 0; c1 <= 8191; c1 += 1604) {
        for (int c2 = t0; c2 <= ppcg_min(1603, -c1 + 8191); c2 += 76)
          shared_B[c2] = B[c1 + c2];
        __syncthreads();
        if (76 * b0 + t0 <= 8191)
          for (int c3 = 0; c3 <= ppcg_min(1603, -c1 + 8191); c3 += 1)
            private_C[0] += (A[(76 * b0 + t0) * 8192 + (c1 + c3)] * shared_B[c3]);
        __syncthreads();
      }
      if (76 * b0 + t0 <= 8191)
        C[76 * b0 + t0] = private_C[0];
    }
}
