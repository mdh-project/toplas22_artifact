#include "matmul_batched_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[1][2][64];
    __shared__ float shared_B[1][64][20];
    float private_C[1][2][1];

    {
      for (int c1 = 0; c1 <= 1; c1 += 1)
        for (int c2 = t2; c2 <= 63; c2 += 20)
          shared_A[0][c1][c2] = A[(b0 * 10 + (2 * b1 + c1)) * 64 + c2];
      __syncthreads();
      for (int c2 = 0; c2 <= 499; c2 += 20) {
        for (int c4 = 0; c4 <= 63; c4 += 1)
          shared_B[0][c4][t2] = B[(b0 * 64 + c4) * 500 + (t2 + c2)];
        __syncthreads();
        private_C[0][0][0] = C[(b0 * 10 + 2 * b1) * 500 + (t2 + c2)];
        private_C[0][1][0] = C[(b0 * 10 + (2 * b1 + 1)) * 500 + (t2 + c2)];
        for (int c4 = 0; c4 <= 63; c4 += 1) {
          private_C[0][0][0] += (shared_A[0][0][c4] * shared_B[0][c4][t2]);
          private_C[0][1][0] += (shared_A[0][1][c4] * shared_B[0][c4][t2]);
        }
        C[(b0 * 10 + 2 * b1) * 500 + (t2 + c2)] = private_C[0][0][0];
        C[(b0 * 10 + (2 * b1 + 1)) * 500 + (t2 + c2)] = private_C[0][1][0];
        __syncthreads();
      }
    }
}
