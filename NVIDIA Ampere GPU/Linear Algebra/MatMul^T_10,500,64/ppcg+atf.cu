#include "matmul_col_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[39][10];
    __shared__ float shared_B[10][39];
    float private_C[1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[(10 * b1 + t1) * 10 + t0];
      for (int c2 = 0; c2 <= 63; c2 += 39) {
        for (int c3 = t0; c3 <= ppcg_min(38, -c2 + 63); c3 += 10)
          shared_A[c3][t1] = A[(c2 + c3) * 10 + t1];
        for (int c4 = t1; c4 <= ppcg_min(38, -c2 + 63); c4 += 10)
          shared_B[t0][c4] = B[(10 * b1 + t0) * 64 + (c2 + c4)];
        __syncthreads();
        for (int c5 = 0; c5 <= ppcg_min(38, -c2 + 63); c5 += 1)
          private_C[0][0] += (shared_A[c5][t0] * shared_B[t1][c5]);
        __syncthreads();
      }
      C[(10 * b1 + t1) * 10 + t0] = private_C[0][0];
    }
}
