#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[2][1004];
    float private_C[2][6];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[2 * b0 * 1024 + t1];
      private_C[0][1] = C[2 * b0 * 1024 + (t1 + 185)];
      private_C[0][2] = C[2 * b0 * 1024 + (t1 + 370)];
      private_C[0][3] = C[2 * b0 * 1024 + (t1 + 555)];
      private_C[0][4] = C[2 * b0 * 1024 + (t1 + 740)];
      if (t1 <= 98)
        private_C[0][5] = C[2 * b0 * 1024 + (t1 + 925)];
      private_C[1][0] = C[(2 * b0 + 1) * 1024 + t1];
      private_C[1][1] = C[(2 * b0 + 1) * 1024 + (t1 + 185)];
      private_C[1][2] = C[(2 * b0 + 1) * 1024 + (t1 + 370)];
      private_C[1][3] = C[(2 * b0 + 1) * 1024 + (t1 + 555)];
      private_C[1][4] = C[(2 * b0 + 1) * 1024 + (t1 + 740)];
      if (t1 <= 98)
        private_C[1][5] = C[(2 * b0 + 1) * 1024 + (t1 + 925)];
      for (int c2 = 0; c2 <= 1023; c2 += 1004) {
        for (int c3 = 0; c3 <= 1; c3 += 1)
          for (int c4 = t1; c4 <= ppcg_min(1003, -c2 + 1023); c4 += 185)
            shared_A[c3][c4] = A[(2 * b0 + c3) * 1024 + (c2 + c4)];
        __syncthreads();
        for (int c3 = 0; c3 <= ppcg_min(1003, -c2 + 1023); c3 += 1) {
          private_C[0][0] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + t1]);
          private_C[0][1] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (t1 + 185)]);
          private_C[0][2] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (t1 + 370)]);
          private_C[0][3] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (t1 + 555)]);
          private_C[0][4] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (t1 + 740)]);
          if (t1 <= 98)
            private_C[0][5] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (t1 + 925)]);
          private_C[1][0] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + t1]);
          private_C[1][1] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (t1 + 185)]);
          private_C[1][2] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (t1 + 370)]);
          private_C[1][3] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (t1 + 555)]);
          private_C[1][4] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (t1 + 740)]);
          if (t1 <= 98)
            private_C[1][5] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (t1 + 925)]);
        }
        __syncthreads();
      }
      C[2 * b0 * 1024 + t1] = private_C[0][0];
      C[2 * b0 * 1024 + (t1 + 185)] = private_C[0][1];
      C[2 * b0 * 1024 + (t1 + 370)] = private_C[0][2];
      C[2 * b0 * 1024 + (t1 + 555)] = private_C[0][3];
      C[2 * b0 * 1024 + (t1 + 740)] = private_C[0][4];
      if (t1 <= 98)
        C[2 * b0 * 1024 + (t1 + 925)] = private_C[0][5];
      C[(2 * b0 + 1) * 1024 + t1] = private_C[1][0];
      C[(2 * b0 + 1) * 1024 + (t1 + 185)] = private_C[1][1];
      C[(2 * b0 + 1) * 1024 + (t1 + 370)] = private_C[1][2];
      C[(2 * b0 + 1) * 1024 + (t1 + 555)] = private_C[1][3];
      C[(2 * b0 + 1) * 1024 + (t1 + 740)] = private_C[1][4];
      if (t1 <= 98)
        C[(2 * b0 + 1) * 1024 + (t1 + 925)] = private_C[1][5];
    }
}
