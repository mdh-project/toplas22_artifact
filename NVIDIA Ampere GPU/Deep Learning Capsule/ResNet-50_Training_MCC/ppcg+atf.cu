#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2_asymmetrical_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_in[1][9][15][3][1][4];
    __shared__ float shared_out[1][2][5][18][1][2];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 111; c2 += 5)
      for (int c3 = 0; c3 <= 63; c3 += 18)
        for (int c4 = 0; c4 <= 3; c4 += 1) {
          if (t1 == 0 && t2 <= 3)
            for (int c6 = 0; c6 <= 8; c6 += 1)
              for (int c7 = 0; c7 <= ppcg_min(14, -2 * c2 + 229); c7 += 1)
                for (int c8 = 0; c8 <= 2; c8 += 1)
                  shared_in[0][c6][c7][c8][0][t2] = in[((((b0 * 230 + (4 * b1 + c6)) * 230 + (2 * c2 + c7)) * 3 + c8) * 4 + c4) * 4 + t2];
          __syncthreads();
          for (int c5 = 0; c5 <= 3; c5 += 2) {
            if (t1 == 0 && t2 <= 1)
              for (int c7 = 0; c7 <= 1; c7 += 1)
                for (int c8 = 0; c8 <= ppcg_min(4, -c2 + 111); c8 += 1)
                  for (int c9 = 0; c9 <= ppcg_min(17, -c3 + 63); c9 += 1)
                    shared_out[0][c7][c8][c9][0][t2] = out[((((b0 * 112 + (2 * b1 + c7)) * 112 + (c2 + c8)) * 64 + (c3 + c9)) * 4 + c4) * 4 + (t2 + c5)];
            __syncthreads();
            if (t2 + c2 <= 111)
              for (int c10 = 0; c10 <= ppcg_min(17, -c3 + 63); c10 += 1)
                for (int c12 = 0; c12 <= 1; c12 += 1)
                  for (int c13 = 0; c13 <= 2; c13 += 1)
                    for (int c14 = 0; c14 <= 6; c14 += 1)
                      for (int c15 = 0; c15 <= 6; c15 += 1)
                        for (int c16 = 0; c16 <= 3; c16 += 1)
                          shared_out[0][t1][t2][c10][0][c12] += (filter[(((((c3 + c10) * 7 + c14) * 7 + c15) * 3 + c13) * 4 + c16) * 4 + (c5 + c12)] * shared_in[0][2 * t1 + c14][2 * t2 + c15][c13][0][c16]);
            __syncthreads();
            if (t1 == 0 && t2 <= 1)
              for (int c7 = 0; c7 <= 1; c7 += 1)
                for (int c8 = 0; c8 <= ppcg_min(4, -c2 + 111); c8 += 1)
                  for (int c9 = 0; c9 <= ppcg_min(17, -c3 + 63); c9 += 1)
                    out[((((b0 * 112 + (2 * b1 + c7)) * 112 + (c2 + c8)) * 64 + (c3 + c9)) * 4 + c4) * 4 + (t2 + c5)] = shared_out[0][c7][c8][c9][0][t2];
          }
          __syncthreads();
        }
}
