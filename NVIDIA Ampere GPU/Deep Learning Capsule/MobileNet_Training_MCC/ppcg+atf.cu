#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_in[1][3][43][1][1][4];
    __shared__ float shared_out[1][1][21][18][1][4];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 111; c2 += 21)
      for (int c3 = 0; c3 <= 31; c3 += 18)
        for (int c4 = 0; c4 <= 3; c4 += 1) {
          if (t2 <= 3)
            for (int c7 = 0; c7 <= ppcg_min(20, -c2 + 111); c7 += 1)
              for (int c8 = 0; c8 <= ppcg_min(17, -c3 + 31); c8 += 1)
                shared_out[0][0][c7][c8][0][t2] = out[((((b0 * 112 + b1) * 112 + (c2 + c7)) * 32 + (c3 + c8)) * 4 + c4) * 4 + t2];
          for (int c6 = 0; c6 <= 2; c6 += 1) {
            if (t2 <= 3)
              for (int c8 = 0; c8 <= 2; c8 += 1)
                for (int c9 = 0; c9 <= ppcg_min(42, -2 * c2 + 224); c9 += 1)
                  shared_in[0][c8][c9][0][0][t2] = in[((((b0 * 225 + (2 * b1 + c8)) * 225 + (2 * c2 + c9)) * 3 + c6) * 4 + c4) * 4 + t2];
            __syncthreads();
            if (t2 + c2 <= 111)
              for (int c10 = 0; c10 <= ppcg_min(17, -c3 + 31); c10 += 1)
                for (int c12 = 0; c12 <= 3; c12 += 1)
                  for (int c14 = 0; c14 <= 2; c14 += 1)
                    for (int c15 = 0; c15 <= 2; c15 += 1)
                      for (int c16 = 0; c16 <= 3; c16 += 1)
                        shared_out[0][0][t2][c10][0][c12] += (filter[(((((c3 + c10) * 3 + c14) * 3 + c15) * 3 + c6) * 4 + c16) * 4 + c12] * shared_in[0][c14][2 * t2 + c15][0][0][c16]);
            __syncthreads();
          }
          if (t2 <= 3)
            for (int c7 = 0; c7 <= ppcg_min(20, -c2 + 111); c7 += 1)
              for (int c8 = 0; c8 <= ppcg_min(17, -c3 + 31); c8 += 1)
                out[((((b0 * 112 + b1) * 112 + (c2 + c7)) * 32 + (c3 + c8)) * 4 + c4) * 4 + t2] = shared_out[0][0][c7][c8][0][t2];
          __syncthreads();
        }
}
