// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul) {
  float matmul_local[1];
  __local float A_shared[32];
  __local float B_shared[256];
  matmul_local[(0)] = 0.000000e+00f;
  for (int k_outer_outer = 0; k_outer_outer < 64; ++k_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    vstore4(vload4(0, A + ((k_outer_outer * 32) + (((int)get_local_id(0)) * 4))), 0, A_shared + (((int)get_local_id(0)) * 4));
    vstore4(vload4(0, B + ((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 4000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 32));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 8000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 64));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 12000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 96));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 16000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 128));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 20000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 160));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 24000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 192));
    vstore4(vload4(0, B + (((((k_outer_outer * 32000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 28000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 224));
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int k_outer_inner = 0; k_outer_inner < 2; ++k_outer_inner) {
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[((k_outer_inner * 16))] * B_shared[(((k_outer_inner * 128) + ((int)get_local_id(0))))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 1))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 8))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 2))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 16))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 3))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 24))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 4))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 32))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 5))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 6))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 48))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 7))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 56))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 8))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 64))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 9))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 72))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 10))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 11))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 88))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 12))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 96))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 13))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 104))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 14))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 112))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 16) + 15))] * B_shared[((((k_outer_inner * 128) + ((int)get_local_id(0))) + 120))]));
    }
  }
  matmul[(((((int)get_group_id(0)) * 8) + ((int)get_local_id(0))))] = matmul_local[(0)];
}

