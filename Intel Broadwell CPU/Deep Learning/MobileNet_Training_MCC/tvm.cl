// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict data, __global float* restrict filter, __global float* restrict mcc_nhwc_krsc_npqk) {
  float mcc_nhwc_krsc_npqk_local[32];
  __local float data_shared[540];
  __local float filter_shared[864];
  mcc_nhwc_krsc_npqk_local[(0)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(16)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(1)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(17)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(2)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(18)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(3)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(19)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(4)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(20)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(5)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(21)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(6)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(22)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(7)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(23)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(8)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(24)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(9)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(25)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(10)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(26)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(11)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(27)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(12)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(28)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(13)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(29)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(14)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(30)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(15)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(31)] = 0.000000e+00f;
  data_shared[((((int)get_local_id(0)) * 15))] = data[(((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)))];
  data_shared[(((((int)get_local_id(0)) * 15) + 1))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 15) + 2))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 15) + 3))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 3))];
  data_shared[(((((int)get_local_id(0)) * 15) + 4))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 4))];
  data_shared[(((((int)get_local_id(0)) * 15) + 5))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 5))];
  data_shared[(((((int)get_local_id(0)) * 15) + 6))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 6))];
  data_shared[(((((int)get_local_id(0)) * 15) + 7))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 7))];
  data_shared[(((((int)get_local_id(0)) * 15) + 8))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 8))];
  data_shared[(((((int)get_local_id(0)) * 15) + 9))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 9))];
  data_shared[(((((int)get_local_id(0)) * 15) + 10))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 10))];
  data_shared[(((((int)get_local_id(0)) * 15) + 11))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 11))];
  data_shared[(((((int)get_local_id(0)) * 15) + 12))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 12))];
  data_shared[(((((int)get_local_id(0)) * 15) + 13))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 13))];
  data_shared[(((((int)get_local_id(0)) * 15) + 14))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + ((((int)get_local_id(0)) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) % 9) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 14))];
  if (((int)get_local_id(0)) < 4) {
    data_shared[(((((int)get_local_id(0)) * 15) + 480))] = data[(((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)))];
    data_shared[(((((int)get_local_id(0)) * 15) + 481))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 1))];
    data_shared[(((((int)get_local_id(0)) * 15) + 482))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 2))];
    data_shared[(((((int)get_local_id(0)) * 15) + 483))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 3))];
    data_shared[(((((int)get_local_id(0)) * 15) + 484))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 4))];
    data_shared[(((((int)get_local_id(0)) * 15) + 485))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 5))];
    data_shared[(((((int)get_local_id(0)) * 15) + 486))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 6))];
    data_shared[(((((int)get_local_id(0)) * 15) + 487))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 7))];
    data_shared[(((((int)get_local_id(0)) * 15) + 488))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 8))];
    data_shared[(((((int)get_local_id(0)) * 15) + 489))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 9))];
    data_shared[(((((int)get_local_id(0)) * 15) + 490))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 10))];
    data_shared[(((((int)get_local_id(0)) * 15) + 491))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 11))];
    data_shared[(((((int)get_local_id(0)) * 15) + 492))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 12))];
    data_shared[(((((int)get_local_id(0)) * 15) + 493))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 13))];
    data_shared[(((((int)get_local_id(0)) * 15) + 494))] = data[((((((((((int)get_group_id(0)) / 1568) * 607500) + (((((int)get_local_id(0)) + 32) / 9) * 151875)) + (((((int)get_group_id(0)) % 1568) / 56) * 5400)) + ((((int)get_local_id(0)) + 5) * 675)) + ((((int)get_group_id(0)) % 56) * 12)) + 14))];
  }
  vstore3(vload3(0, filter + (((int)get_local_id(0)) * 3)), 0, filter_shared + (((int)get_local_id(0)) * 3));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 96)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 96));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 192)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 192));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 288)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 288));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 384)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 384));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 480)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 480));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 576)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 576));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 672)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 672));
  vstore3(vload3(0, filter + ((((int)get_local_id(0)) * 3) + 768)), 0, filter_shared + ((((int)get_local_id(0)) * 3) + 768));
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int r_inner = 0; r_inner < 3; ++r_inner) {
    for (int s_inner = 0; s_inner < 3; ++s_inner) {
      for (int c_inner = 0; c_inner < 3; ++c_inner) {
        mcc_nhwc_krsc_npqk_local[(0)] = (mcc_nhwc_krsc_npqk_local[(0)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[((((r_inner * 9) + (s_inner * 3)) + c_inner))]));
        mcc_nhwc_krsc_npqk_local[(16)] = (mcc_nhwc_krsc_npqk_local[(16)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 432))]));
        mcc_nhwc_krsc_npqk_local[(1)] = (mcc_nhwc_krsc_npqk_local[(1)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 27))]));
        mcc_nhwc_krsc_npqk_local[(17)] = (mcc_nhwc_krsc_npqk_local[(17)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 459))]));
        mcc_nhwc_krsc_npqk_local[(2)] = (mcc_nhwc_krsc_npqk_local[(2)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 54))]));
        mcc_nhwc_krsc_npqk_local[(18)] = (mcc_nhwc_krsc_npqk_local[(18)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 486))]));
        mcc_nhwc_krsc_npqk_local[(3)] = (mcc_nhwc_krsc_npqk_local[(3)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 81))]));
        mcc_nhwc_krsc_npqk_local[(19)] = (mcc_nhwc_krsc_npqk_local[(19)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 513))]));
        mcc_nhwc_krsc_npqk_local[(4)] = (mcc_nhwc_krsc_npqk_local[(4)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 108))]));
        mcc_nhwc_krsc_npqk_local[(20)] = (mcc_nhwc_krsc_npqk_local[(20)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 540))]));
        mcc_nhwc_krsc_npqk_local[(5)] = (mcc_nhwc_krsc_npqk_local[(5)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 135))]));
        mcc_nhwc_krsc_npqk_local[(21)] = (mcc_nhwc_krsc_npqk_local[(21)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 567))]));
        mcc_nhwc_krsc_npqk_local[(6)] = (mcc_nhwc_krsc_npqk_local[(6)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 162))]));
        mcc_nhwc_krsc_npqk_local[(22)] = (mcc_nhwc_krsc_npqk_local[(22)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 594))]));
        mcc_nhwc_krsc_npqk_local[(7)] = (mcc_nhwc_krsc_npqk_local[(7)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 189))]));
        mcc_nhwc_krsc_npqk_local[(23)] = (mcc_nhwc_krsc_npqk_local[(23)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 621))]));
        mcc_nhwc_krsc_npqk_local[(8)] = (mcc_nhwc_krsc_npqk_local[(8)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 216))]));
        mcc_nhwc_krsc_npqk_local[(24)] = (mcc_nhwc_krsc_npqk_local[(24)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 648))]));
        mcc_nhwc_krsc_npqk_local[(9)] = (mcc_nhwc_krsc_npqk_local[(9)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 243))]));
        mcc_nhwc_krsc_npqk_local[(25)] = (mcc_nhwc_krsc_npqk_local[(25)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 675))]));
        mcc_nhwc_krsc_npqk_local[(10)] = (mcc_nhwc_krsc_npqk_local[(10)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 270))]));
        mcc_nhwc_krsc_npqk_local[(26)] = (mcc_nhwc_krsc_npqk_local[(26)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 702))]));
        mcc_nhwc_krsc_npqk_local[(11)] = (mcc_nhwc_krsc_npqk_local[(11)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 297))]));
        mcc_nhwc_krsc_npqk_local[(27)] = (mcc_nhwc_krsc_npqk_local[(27)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 729))]));
        mcc_nhwc_krsc_npqk_local[(12)] = (mcc_nhwc_krsc_npqk_local[(12)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 324))]));
        mcc_nhwc_krsc_npqk_local[(28)] = (mcc_nhwc_krsc_npqk_local[(28)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 756))]));
        mcc_nhwc_krsc_npqk_local[(13)] = (mcc_nhwc_krsc_npqk_local[(13)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 351))]));
        mcc_nhwc_krsc_npqk_local[(29)] = (mcc_nhwc_krsc_npqk_local[(29)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 783))]));
        mcc_nhwc_krsc_npqk_local[(14)] = (mcc_nhwc_krsc_npqk_local[(14)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 378))]));
        mcc_nhwc_krsc_npqk_local[(30)] = (mcc_nhwc_krsc_npqk_local[(30)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 810))]));
        mcc_nhwc_krsc_npqk_local[(15)] = (mcc_nhwc_krsc_npqk_local[(15)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 405))]));
        mcc_nhwc_krsc_npqk_local[(31)] = (mcc_nhwc_krsc_npqk_local[(31)] + (data_shared[((((((((((int)get_local_id(0)) >> 3) * 135) + (((((int)get_local_id(0)) & 7) >> 1) * 30)) + (r_inner * 15)) + ((((int)get_local_id(0)) & 1) * 6)) + (s_inner * 3)) + c_inner))] * filter_shared[(((((r_inner * 9) + (s_inner * 3)) + c_inner) + 837))]));
      }
    }
  }
  for (int k_inner = 0; k_inner < 16; ++k_inner) {
    mcc_nhwc_krsc_npqk[(((((((((((int)get_group_id(0)) / 1568) * 1605632) + ((((int)get_local_id(0)) >> 3) * 401408)) + (((((int)get_group_id(0)) % 1568) / 56) * 14336)) + (((((int)get_local_id(0)) & 7) >> 1) * 3584)) + ((((int)get_group_id(0)) % 56) * 64)) + ((((int)get_local_id(0)) & 1) * 32)) + k_inner))] = mcc_nhwc_krsc_npqk_local[(k_inner)];
    mcc_nhwc_krsc_npqk[((((((((((((int)get_group_id(0)) / 1568) * 1605632) + ((((int)get_local_id(0)) >> 3) * 401408)) + (((((int)get_group_id(0)) % 1568) / 56) * 14336)) + (((((int)get_local_id(0)) & 7) >> 1) * 3584)) + ((((int)get_group_id(0)) % 56) * 64)) + ((((int)get_local_id(0)) & 1) * 32)) + k_inner) + 16))] = mcc_nhwc_krsc_npqk_local[((k_inner + 16))];
  }
}

