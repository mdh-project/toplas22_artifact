__kernel void mcc_nhwc_krsc_npqk_stride_2_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[1 * 1]
                                   [1 * 1]
                                   [1 * 14]
                                   [1 * 1];
    __private float filter_prv[1]
                                      [1]
                                      [3]
                                      [1];
    const size_t wg_1 = get_group_id(2) / (56) % (16); {
    const size_t wg_2 = get_group_id(2) % (56); {
    {
    {
    {
    {
    {
    {
    const size_t wi_2 = get_local_id(2) % (2); {
    {
    const size_t wi_4 = get_local_id(0); {
    {
    {
    {
    {
    {
    for (size_t glb_3 = 0; glb_3 < 8; ++glb_3) {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 14; ++prv_3) {
    {
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 14 + prv_3][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}
    }
    {
    for (size_t glb_6 = 0; glb_6 < 3; ++glb_6) {
    {
        {
        {
        {
        {
        for (size_t lcl_5 = 0; lcl_5 < 3; ++lcl_5) {
        {
        {
            {
               
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    filter_prv[0][0][prv_7][0]
                                      =
                    filter_glb[(0 * 1 * 1 * 32 * 1 + 0 * 1 * 32 * 1 + 0 * 32 * 1 + wi_4 * 1 + 0) * (3) * (3) * (3) + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) * (3) + (0 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            for (size_t prv_3 = 0; prv_3 < 14; ++prv_3) {
            {
            {
            {
            for (size_t prv_7 = 0; prv_7 < 3; ++prv_7) {
            out_prv[0 * 1 + 0][0 * 1 + 0][0 * 14 + prv_3][0 * 1 + 0]
                +=
            images_glb[(0 * 16 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((2 * 112 - 1 + 3 - 1)) * ((2 * 112 - 1 + 3 - 1)) * (3) + ((0 * 56 * 1 * 2 * 1 + wg_2 * 1 * 2 * 1 + 0 * 2 * 1 + wi_2 * 1 + 0) * 2 + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 - 1 + 3 - 1)) * (3) + ((glb_3 * 1 * 1 * 1 * 14 + 0 * 1 * 1 * 14 + 0 * 1 * 14 + 0 * 14 + prv_3) * 2 + (0 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7)) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
            *
            filter_prv[0][0][prv_7][0]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 14; ++prv_3) {
    {
        out_glb[(0 * 16 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (112) * (32) + (0 * 56 * 1 * 2 * 1 + wg_2 * 1 * 2 * 1 + 0 * 2 * 1 + wi_2 * 1 + 0) * (112) * (32) + (glb_3 * 1 * 1 * 1 * 14 + 0 * 1 * 1 * 14 + 0 * 1 * 14 + 0 * 14 + prv_3) * (32) + (0 * 1 * 1 * 32 * 1 + 0 * 1 * 32 * 1 + 0 * 32 * 1 + wi_4 * 1 + 0) ]
                                                 =
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 14 + prv_3][0 * 1 + 0]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
