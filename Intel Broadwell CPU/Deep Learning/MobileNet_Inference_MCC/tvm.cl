// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict data, __global float* restrict filter, __global float* restrict mcc_nhwc_krsc_npqk) {
  float mcc_nhwc_krsc_npqk_local[64];
  __local float data_shared[3267];
  __local float filter_shared[864];
  mcc_nhwc_krsc_npqk_local[(0)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(32)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(1)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(33)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(2)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(34)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(3)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(35)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(4)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(36)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(5)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(37)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(6)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(38)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(7)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(39)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(8)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(40)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(9)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(41)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(10)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(42)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(11)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(43)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(12)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(44)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(13)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(45)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(14)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(46)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(15)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(47)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(16)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(48)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(17)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(49)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(18)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(50)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(19)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(51)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(20)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(52)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(21)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(53)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(22)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(54)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(23)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(55)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(24)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(56)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(25)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(57)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(26)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(58)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(27)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(59)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(28)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(60)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(29)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(61)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(30)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(62)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(31)] = 0.000000e+00f;
  mcc_nhwc_krsc_npqk_local[(63)] = 0.000000e+00f;
  data_shared[((((int)get_local_id(0)) * 9))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((int)get_local_id(0)) / 11) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((int)get_local_id(0)) % 11) * 9)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((int)get_local_id(0)) / 11) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((int)get_local_id(0)) % 11) * 9)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 2))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((int)get_local_id(0)) / 11) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((int)get_local_id(0)) % 11) * 9)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 9) + 3))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 1) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 1) % 33) * 3)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 4))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 1) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 1) % 33) * 3)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 5))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 1) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 1) % 33) * 3)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 9) + 6))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 2) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 2) % 33) * 3)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 7))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 2) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 2) % 33) * 3)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 8))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 2) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 2) % 33) * 3)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1152))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 384) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 21) % 33) * 3)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1153))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 384) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 21) % 33) * 3)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1154))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 384) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 21) % 33) * 3)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1155))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 385) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 22) % 33) * 3)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1156))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 385) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 22) % 33) * 3)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1157))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 385) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 22) % 33) * 3)) + 2))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1158))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 386) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 23) % 33) * 3)))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1159))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 386) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 23) % 33) * 3)) + 1))];
  data_shared[(((((int)get_local_id(0)) * 9) + 1160))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 386) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 23) % 33) * 3)) + 2))];
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2304))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 768) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 9) % 33) * 3)))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2305))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 768) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 9) % 33) * 3)) + 1))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2306))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 768) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 9) % 33) * 3)) + 2))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2307))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 769) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 10) % 33) * 3)))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2308))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 769) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 10) % 33) * 3)) + 1))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2309))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 769) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 10) % 33) * 3)) + 2))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2310))] = data[((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 770) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 11) % 33) * 3)))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2311))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 770) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 11) % 33) * 3)) + 1))];
  }
  if (((int)get_local_id(0)) < 107) {
    data_shared[(((((int)get_local_id(0)) * 9) + 2312))] = data[(((((((((int)get_group_id(0)) / 7) * 21600) + ((((((int)get_local_id(0)) * 3) + 770) / 33) * 675)) + ((((int)get_group_id(0)) % 7) * 96)) + ((((((int)get_local_id(0)) * 3) + 11) % 33) * 3)) + 2))];
  }
  filter_shared[((((int)get_local_id(0)) * 3))] = filter[((((int)get_local_id(0)) * 3))];
  filter_shared[(((((int)get_local_id(0)) * 3) + 1))] = filter[(((((int)get_local_id(0)) * 3) + 1))];
  filter_shared[(((((int)get_local_id(0)) * 3) + 2))] = filter[(((((int)get_local_id(0)) * 3) + 2))];
  filter_shared[(((((int)get_local_id(0)) * 3) + 384))] = filter[(((((int)get_local_id(0)) * 3) + 384))];
  filter_shared[(((((int)get_local_id(0)) * 3) + 385))] = filter[(((((int)get_local_id(0)) * 3) + 385))];
  filter_shared[(((((int)get_local_id(0)) * 3) + 386))] = filter[(((((int)get_local_id(0)) * 3) + 386))];
  if (((int)get_local_id(0)) < 32) {
    filter_shared[(((((int)get_local_id(0)) * 3) + 768))] = filter[(((((int)get_local_id(0)) * 3) + 768))];
    filter_shared[(((((int)get_local_id(0)) * 3) + 769))] = filter[(((((int)get_local_id(0)) * 3) + 769))];
    filter_shared[(((((int)get_local_id(0)) * 3) + 770))] = filter[(((((int)get_local_id(0)) * 3) + 770))];
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int r_outer_inner = 0; r_outer_inner < 3; ++r_outer_inner) {
    for (int s_outer_inner = 0; s_outer_inner < 3; ++s_outer_inner) {
      for (int c_outer_inner = 0; c_outer_inner < 3; ++c_outer_inner) {
        mcc_nhwc_krsc_npqk_local[(0)] = (mcc_nhwc_krsc_npqk_local[(0)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner))]));
        mcc_nhwc_krsc_npqk_local[(32)] = (mcc_nhwc_krsc_npqk_local[(32)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner))]));
        mcc_nhwc_krsc_npqk_local[(1)] = (mcc_nhwc_krsc_npqk_local[(1)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 27))]));
        mcc_nhwc_krsc_npqk_local[(33)] = (mcc_nhwc_krsc_npqk_local[(33)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 27))]));
        mcc_nhwc_krsc_npqk_local[(2)] = (mcc_nhwc_krsc_npqk_local[(2)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 54))]));
        mcc_nhwc_krsc_npqk_local[(34)] = (mcc_nhwc_krsc_npqk_local[(34)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 54))]));
        mcc_nhwc_krsc_npqk_local[(3)] = (mcc_nhwc_krsc_npqk_local[(3)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 81))]));
        mcc_nhwc_krsc_npqk_local[(35)] = (mcc_nhwc_krsc_npqk_local[(35)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 81))]));
        mcc_nhwc_krsc_npqk_local[(4)] = (mcc_nhwc_krsc_npqk_local[(4)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 108))]));
        mcc_nhwc_krsc_npqk_local[(36)] = (mcc_nhwc_krsc_npqk_local[(36)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 108))]));
        mcc_nhwc_krsc_npqk_local[(5)] = (mcc_nhwc_krsc_npqk_local[(5)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 135))]));
        mcc_nhwc_krsc_npqk_local[(37)] = (mcc_nhwc_krsc_npqk_local[(37)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 135))]));
        mcc_nhwc_krsc_npqk_local[(6)] = (mcc_nhwc_krsc_npqk_local[(6)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 162))]));
        mcc_nhwc_krsc_npqk_local[(38)] = (mcc_nhwc_krsc_npqk_local[(38)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 162))]));
        mcc_nhwc_krsc_npqk_local[(7)] = (mcc_nhwc_krsc_npqk_local[(7)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 189))]));
        mcc_nhwc_krsc_npqk_local[(39)] = (mcc_nhwc_krsc_npqk_local[(39)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 189))]));
        mcc_nhwc_krsc_npqk_local[(8)] = (mcc_nhwc_krsc_npqk_local[(8)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 216))]));
        mcc_nhwc_krsc_npqk_local[(40)] = (mcc_nhwc_krsc_npqk_local[(40)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 216))]));
        mcc_nhwc_krsc_npqk_local[(9)] = (mcc_nhwc_krsc_npqk_local[(9)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 243))]));
        mcc_nhwc_krsc_npqk_local[(41)] = (mcc_nhwc_krsc_npqk_local[(41)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 243))]));
        mcc_nhwc_krsc_npqk_local[(10)] = (mcc_nhwc_krsc_npqk_local[(10)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 270))]));
        mcc_nhwc_krsc_npqk_local[(42)] = (mcc_nhwc_krsc_npqk_local[(42)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 270))]));
        mcc_nhwc_krsc_npqk_local[(11)] = (mcc_nhwc_krsc_npqk_local[(11)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 297))]));
        mcc_nhwc_krsc_npqk_local[(43)] = (mcc_nhwc_krsc_npqk_local[(43)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 297))]));
        mcc_nhwc_krsc_npqk_local[(12)] = (mcc_nhwc_krsc_npqk_local[(12)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 324))]));
        mcc_nhwc_krsc_npqk_local[(44)] = (mcc_nhwc_krsc_npqk_local[(44)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 324))]));
        mcc_nhwc_krsc_npqk_local[(13)] = (mcc_nhwc_krsc_npqk_local[(13)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 351))]));
        mcc_nhwc_krsc_npqk_local[(45)] = (mcc_nhwc_krsc_npqk_local[(45)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 351))]));
        mcc_nhwc_krsc_npqk_local[(14)] = (mcc_nhwc_krsc_npqk_local[(14)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 378))]));
        mcc_nhwc_krsc_npqk_local[(46)] = (mcc_nhwc_krsc_npqk_local[(46)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 378))]));
        mcc_nhwc_krsc_npqk_local[(15)] = (mcc_nhwc_krsc_npqk_local[(15)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 405))]));
        mcc_nhwc_krsc_npqk_local[(47)] = (mcc_nhwc_krsc_npqk_local[(47)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 405))]));
        mcc_nhwc_krsc_npqk_local[(16)] = (mcc_nhwc_krsc_npqk_local[(16)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 432))]));
        mcc_nhwc_krsc_npqk_local[(48)] = (mcc_nhwc_krsc_npqk_local[(48)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 432))]));
        mcc_nhwc_krsc_npqk_local[(17)] = (mcc_nhwc_krsc_npqk_local[(17)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 459))]));
        mcc_nhwc_krsc_npqk_local[(49)] = (mcc_nhwc_krsc_npqk_local[(49)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 459))]));
        mcc_nhwc_krsc_npqk_local[(18)] = (mcc_nhwc_krsc_npqk_local[(18)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 486))]));
        mcc_nhwc_krsc_npqk_local[(50)] = (mcc_nhwc_krsc_npqk_local[(50)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 486))]));
        mcc_nhwc_krsc_npqk_local[(19)] = (mcc_nhwc_krsc_npqk_local[(19)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 513))]));
        mcc_nhwc_krsc_npqk_local[(51)] = (mcc_nhwc_krsc_npqk_local[(51)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 513))]));
        mcc_nhwc_krsc_npqk_local[(20)] = (mcc_nhwc_krsc_npqk_local[(20)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 540))]));
        mcc_nhwc_krsc_npqk_local[(52)] = (mcc_nhwc_krsc_npqk_local[(52)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 540))]));
        mcc_nhwc_krsc_npqk_local[(21)] = (mcc_nhwc_krsc_npqk_local[(21)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 567))]));
        mcc_nhwc_krsc_npqk_local[(53)] = (mcc_nhwc_krsc_npqk_local[(53)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 567))]));
        mcc_nhwc_krsc_npqk_local[(22)] = (mcc_nhwc_krsc_npqk_local[(22)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 594))]));
        mcc_nhwc_krsc_npqk_local[(54)] = (mcc_nhwc_krsc_npqk_local[(54)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 594))]));
        mcc_nhwc_krsc_npqk_local[(23)] = (mcc_nhwc_krsc_npqk_local[(23)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 621))]));
        mcc_nhwc_krsc_npqk_local[(55)] = (mcc_nhwc_krsc_npqk_local[(55)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 621))]));
        mcc_nhwc_krsc_npqk_local[(24)] = (mcc_nhwc_krsc_npqk_local[(24)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 648))]));
        mcc_nhwc_krsc_npqk_local[(56)] = (mcc_nhwc_krsc_npqk_local[(56)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 648))]));
        mcc_nhwc_krsc_npqk_local[(25)] = (mcc_nhwc_krsc_npqk_local[(25)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 675))]));
        mcc_nhwc_krsc_npqk_local[(57)] = (mcc_nhwc_krsc_npqk_local[(57)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 675))]));
        mcc_nhwc_krsc_npqk_local[(26)] = (mcc_nhwc_krsc_npqk_local[(26)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 702))]));
        mcc_nhwc_krsc_npqk_local[(58)] = (mcc_nhwc_krsc_npqk_local[(58)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 702))]));
        mcc_nhwc_krsc_npqk_local[(27)] = (mcc_nhwc_krsc_npqk_local[(27)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 729))]));
        mcc_nhwc_krsc_npqk_local[(59)] = (mcc_nhwc_krsc_npqk_local[(59)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 729))]));
        mcc_nhwc_krsc_npqk_local[(28)] = (mcc_nhwc_krsc_npqk_local[(28)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 756))]));
        mcc_nhwc_krsc_npqk_local[(60)] = (mcc_nhwc_krsc_npqk_local[(60)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 756))]));
        mcc_nhwc_krsc_npqk_local[(29)] = (mcc_nhwc_krsc_npqk_local[(29)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 783))]));
        mcc_nhwc_krsc_npqk_local[(61)] = (mcc_nhwc_krsc_npqk_local[(61)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 783))]));
        mcc_nhwc_krsc_npqk_local[(30)] = (mcc_nhwc_krsc_npqk_local[(30)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 810))]));
        mcc_nhwc_krsc_npqk_local[(62)] = (mcc_nhwc_krsc_npqk_local[(62)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 810))]));
        mcc_nhwc_krsc_npqk_local[(31)] = (mcc_nhwc_krsc_npqk_local[(31)] + (data_shared[(((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 837))]));
        mcc_nhwc_krsc_npqk_local[(63)] = (mcc_nhwc_krsc_npqk_local[(63)] + (data_shared[((((((((((int)get_local_id(0)) >> 4) * 198) + (r_outer_inner * 99)) + ((((int)get_local_id(0)) & 15) * 6)) + (s_outer_inner * 3)) + c_outer_inner) + 1584))] * filter_shared[(((((r_outer_inner * 9) + (s_outer_inner * 3)) + c_outer_inner) + 837))]));
      }
    }
  }
  for (int k_inner = 0; k_inner < 32; ++k_inner) {
    mcc_nhwc_krsc_npqk[(((((((((int)get_group_id(0)) / 7) * 57344) + ((((int)get_local_id(0)) >> 4) * 3584)) + ((((int)get_group_id(0)) % 7) * 512)) + ((((int)get_local_id(0)) & 15) * 32)) + k_inner))] = mcc_nhwc_krsc_npqk_local[(k_inner)];
    mcc_nhwc_krsc_npqk[((((((((((int)get_group_id(0)) / 7) * 57344) + ((((int)get_local_id(0)) >> 4) * 3584)) + ((((int)get_group_id(0)) % 7) * 512)) + ((((int)get_local_id(0)) & 15) * 32)) + k_inner) + 28672))] = mcc_nhwc_krsc_npqk_local[((k_inner + 32))];
  }
}

