#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 16
    #define N_VAL 4096
    #define K_VAL 25088

    static float A[M_VAL][K_VAL];
    static float B[K_VAL][N_VAL];
    static float C[M_VAL][N_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)C)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4, t5, t6;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((K_VAL >= 1) && (M_VAL >= 1) && (N_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,32);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4,t5,t6)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(N_VAL-1,32);t2++) {
      for (t3=0;t3<=floord(K_VAL-1,32);t3++) {
        for (t4=32*t1;t4<=(min(M_VAL-1,32*t1+31))-7;t4+=8) {
          for (t5=32*t3;t5<=(min(K_VAL-1,32*t3+31))-7;t5+=8) {
            lbv=32*t2;
            ubv=min(N_VAL-1,32*t2+31);
#pragma ivdep
#pragma vector always
            for (t6=lbv;t6<=ubv;t6++) {
              C[t4][t6] += A[t4][t5] * B[t5][t6];;
              C[(t4+1)][t6] += A[(t4+1)][t5] * B[t5][t6];;
              C[(t4+2)][t6] += A[(t4+2)][t5] * B[t5][t6];;
              C[(t4+3)][t6] += A[(t4+3)][t5] * B[t5][t6];;
              C[(t4+4)][t6] += A[(t4+4)][t5] * B[t5][t6];;
              C[(t4+5)][t6] += A[(t4+5)][t5] * B[t5][t6];;
              C[(t4+6)][t6] += A[(t4+6)][t5] * B[t5][t6];;
              C[(t4+7)][t6] += A[(t4+7)][t5] * B[t5][t6];;
              C[t4][t6] += A[t4][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+1)] * B[(t5+1)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+1)] * B[(t5+1)][t6];;
              C[t4][t6] += A[t4][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+2)] * B[(t5+2)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+2)] * B[(t5+2)][t6];;
              C[t4][t6] += A[t4][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+3)] * B[(t5+3)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+3)] * B[(t5+3)][t6];;
              C[t4][t6] += A[t4][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+4)] * B[(t5+4)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+4)] * B[(t5+4)][t6];;
              C[t4][t6] += A[t4][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+5)] * B[(t5+5)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+5)] * B[(t5+5)][t6];;
              C[t4][t6] += A[t4][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+6)] * B[(t5+6)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+6)] * B[(t5+6)][t6];;
              C[t4][t6] += A[t4][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+1)][t6] += A[(t4+1)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+2)][t6] += A[(t4+2)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+3)][t6] += A[(t4+3)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+4)][t6] += A[(t4+4)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+5)][t6] += A[(t4+5)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+6)][t6] += A[(t4+6)][(t5+7)] * B[(t5+7)][t6];;
              C[(t4+7)][t6] += A[(t4+7)][(t5+7)] * B[(t5+7)][t6];;
            }
          }
          for (;t5<=min(K_VAL-1,32*t3+31);t5++) {
            lbv=32*t2;
            ubv=min(N_VAL-1,32*t2+31);
#pragma ivdep
#pragma vector always
            for (t6=lbv;t6<=ubv;t6++) {
              C[t4][t6] += A[t4][t5] * B[t5][t6];;
              C[(t4+1)][t6] += A[(t4+1)][t5] * B[t5][t6];;
              C[(t4+2)][t6] += A[(t4+2)][t5] * B[t5][t6];;
              C[(t4+3)][t6] += A[(t4+3)][t5] * B[t5][t6];;
              C[(t4+4)][t6] += A[(t4+4)][t5] * B[t5][t6];;
              C[(t4+5)][t6] += A[(t4+5)][t5] * B[t5][t6];;
              C[(t4+6)][t6] += A[(t4+6)][t5] * B[t5][t6];;
              C[(t4+7)][t6] += A[(t4+7)][t5] * B[t5][t6];;
            }
          }
        }
        for (;t4<=min(M_VAL-1,32*t1+31);t4++) {
          for (t5=32*t3;t5<=min(K_VAL-1,32*t3+31);t5++) {
            lbv=32*t2;
            ubv=min(N_VAL-1,32*t2+31);
#pragma ivdep
#pragma vector always
            for (t6=lbv;t6<=ubv;t6++) {
              C[t4][t6] += A[t4][t5] * B[t5][t6];;
            }
          }
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float C M_VAL N_VAL
    {
        float C_gold[M_VAL][N_VAL];
        FILE *C_gold_file = fopen("../../../../../../gold/Pluto/12e075a/matmul_row_major_nn/16x4096x25088/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(C_gold_file, "%f", &(((float*)C_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*M_VAL*N_VAL) {
                printf("incorrect result buffer size for buffer C: expected %d, actual %d.", i, 1*M_VAL*N_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(C_gold_file);

        for (int i = 0; i < 1*M_VAL*N_VAL; ++i) {
            if (0.1 == 0) {
                if (((float*)C)[i] != ((float*)C_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)C)[i] - ((float*)C_gold)[i]) > 0.1) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
