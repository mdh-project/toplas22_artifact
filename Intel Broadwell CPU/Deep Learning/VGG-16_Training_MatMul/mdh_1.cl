__kernel void matmul_row_major_nn_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_l_1 = 0;
    const size_t i_wi_l_1 = get_local_id(0);
    const size_t i_wg_l_2 = get_group_id(1);
    const size_t i_wi_l_2 = get_local_id(1);
    const size_t i_wg_r_1 = get_group_id(2);
    const size_t i_wi_r_1 = 0;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __local float cb_l_a[(4)][(16)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __local float res_l_c[1][1][128][16];
  l_cb_offset_l_1 = i_wg_l_1 * 8;
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((16 / (1 * 8)) / (16 / 8)); ++l_step_l_1) {
    l_cb_offset_l_2 = i_wg_l_2 * 16;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((4096 / (16 * 16)) / (128 / 16)); ++l_step_l_2) {
      l_cb_offset_r_1 = i_wg_r_1 * 1;
      size_t l_step_r_1 = 0;
      {
        {
          if ((((get_local_id(2)) * (16) + (get_local_id(1))) * (8) + (get_local_id(0))) < (((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8))) * (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))) % (8 * 16 * 1)) {
            const size_t flat_index = (((get_local_id(2)) * (16) + (get_local_id(1))) * (8) + (get_local_id(0))) + ((((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8))) * (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))) / (8 * 16 * 1)) * (8 * 16 * 1);
            const size_t l_dim_0_index_l_1 = flat_index / ((((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))));
            const size_t l_dim_1_index_r_1 = flat_index % (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1)));
            cb_l_a[((l_dim_1_index_r_1))][((l_dim_0_index_l_1))] = a_buf[(((l_step_l_1 * (16 / 8) + (l_dim_0_index_l_1) / 8) * (1 * 8) + i_wg_l_1 * 8 + ((l_dim_0_index_l_1) % 8))) * (25088) + (((l_step_r_1 * (4 / 1) + (l_dim_1_index_r_1) / 1) * (64 * 1) + i_wg_r_1 * 1 + ((l_dim_1_index_r_1) % 1)))];
          }
            }
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((16 / 8) / (2)); ++p_step_l_1) {
        p_cb_offset_l_2 = i_wi_l_2 * 1;
        for (size_t p_step_l_2 = 0; p_step_l_2 < ((128 / 16) / (1)); ++p_step_l_2) {
          p_cb_offset_r_1 = i_wi_r_1 * 1;
          size_t p_step_r_1 = 0;
#pragma unroll
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (2); ++p_iteration_l_1) {
#pragma unroll
            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
              size_t p_iteration_r_1 = 0;
              res_l_c[(i_wi_r_1)][(0)][(p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)][(p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)] = cb_l_a[(((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)))][(((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_r_1)) * (4096) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)) / 16) * (16 * 16) + i_wi_l_2))];
#pragma unroll
              for (p_iteration_r_1 = 1; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                res_l_c[(i_wi_r_1)][(0)][(p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)][(p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)] += cb_l_a[(((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)))][(((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_r_1)) * (4096) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)) / 16) * (16 * 16) + i_wi_l_2))];
              }
            }
          }
          p_cb_offset_r_1 += 1 * (2);
          for (p_step_r_1 = 1; p_step_r_1 < ((4 / 1) / (2)); ++p_step_r_1) {
#pragma unroll
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (2); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                  res_l_c[(i_wi_r_1)][(0)][(p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)][(p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)] += cb_l_a[(((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)))][(((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_r_1)) * (4096) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)) / 16) * (16 * 16) + i_wi_l_2))];
                }
              }
            }
            p_cb_offset_r_1 += 1 * (2);
          }
          p_cb_offset_l_2 += 16 * (1);
        }
        p_cb_offset_l_1 += 8 * (2);
      }
      barrier(CLK_LOCAL_MEM_FENCE);
      l_cb_offset_r_1 += (64 * 1) * (4 / 1);
      for (l_step_r_1 = 1; l_step_r_1 < ((25088 / (64 * 1)) / (4 / 1)); ++l_step_r_1) {
        {
          {
            if ((((get_local_id(2)) * (16) + (get_local_id(1))) * (8) + (get_local_id(0))) < (((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8))) * (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))) % (8 * 16 * 1)) {
              const size_t flat_index = (((get_local_id(2)) * (16) + (get_local_id(1))) * (8) + (get_local_id(0))) + ((((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8))) * (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))) / (8 * 16 * 1)) * (8 * 16 * 1);
              const size_t l_dim_0_index_l_1 = flat_index / ((((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1))));
              const size_t l_dim_1_index_r_1 = flat_index % (((((4 / 1) / (2)) * (2) + ((1 * (4 / 1)) % (1 * (2)) / 1)) * 1 + ((1 * (4 / 1)) % (1 * (2)) % 1)));
              cb_l_a[((l_dim_1_index_r_1))][((l_dim_0_index_l_1))] = a_buf[(((l_step_l_1 * (16 / 8) + (l_dim_0_index_l_1) / 8) * (1 * 8) + i_wg_l_1 * 8 + ((l_dim_0_index_l_1) % 8))) * (25088) + (((l_step_r_1 * (4 / 1) + (l_dim_1_index_r_1) / 1) * (64 * 1) + i_wg_r_1 * 1 + ((l_dim_1_index_r_1) % 1)))];
            }
              }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        p_cb_offset_l_1 = i_wi_l_1 * 1;
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((16 / 8) / (2)); ++p_step_l_1) {
          p_cb_offset_l_2 = i_wi_l_2 * 1;
          for (size_t p_step_l_2 = 0; p_step_l_2 < ((128 / 16) / (1)); ++p_step_l_2) {
            p_cb_offset_r_1 = i_wi_r_1 * 1;
            size_t p_step_r_1 = 0;
#pragma unroll
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (2); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                  res_l_c[(i_wi_r_1)][(0)][(p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)][(p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)] += cb_l_a[(((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)))][(((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_r_1)) * (4096) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)) / 16) * (16 * 16) + i_wi_l_2))];
                }
              }
            }
            p_cb_offset_r_1 += 1 * (2);
            for (p_step_r_1 = 1; p_step_r_1 < ((4 / 1) / (2)); ++p_step_r_1) {
#pragma unroll
              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (2); ++p_iteration_l_1) {
                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                  for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                    res_l_c[(i_wi_r_1)][(0)][(p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)][(p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)] += cb_l_a[(((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)))][(((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 8 + 0)))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (64 * 1) + i_wi_r_1)) * (4096) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 16 + 0)) / 16) * (16 * 16) + i_wi_l_2))];
                  }
                }
              }
              p_cb_offset_r_1 += 1 * (2);
            }
            p_cb_offset_l_2 += 16 * (1);
          }
          p_cb_offset_l_1 += 8 * (2);
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        l_cb_offset_r_1 += (64 * 1) * (4 / 1);
      }
      {
        barrier(CLK_LOCAL_MEM_FENCE);
        for (size_t step = 0; step < ((((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8)) * ((((128 / 16) / (1)) * (1) + ((16 * (128 / 16)) % (16 * (1)) / 16)) * 16 + ((16 * (128 / 16)) % (16 * (1)) % 16)))) / (8 * 16 * 1); ++step) {
          const size_t flat_index = (((get_local_id(2)) * (16) + (get_local_id(1))) * (8) + (get_local_id(0))) + step * (8 * 16 * 1);
          const size_t l_index_l_2 = flat_index / (((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8)));
          const size_t l_index_l_1 = flat_index % ((((16 / 8) / (2)) * (2) + ((8 * (16 / 8)) % (8 * (2)) / 8)) * 8 + ((8 * (16 / 8)) % (8 * (2)) % 8));
          int_res_c[((i_wg_r_1) * (4096) + (((l_step_l_2 * (128 / 16) + (l_index_l_2) / 16) * (16 * 16) + i_wg_l_2 * 16 + ((l_index_l_2) % 16)))) * (16) + (((l_step_l_1 * (16 / 8) + (l_index_l_1) / 8) * (1 * 8) + i_wg_l_1 * 8 + ((l_index_l_1) % 8)))]
         =
          res_l_c[(0)][(0)][l_index_l_2][l_index_l_1];
        }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
      l_cb_offset_l_2 += (16 * 16) * (128 / 16);
    }
    l_cb_offset_l_1 += (1 * 8) * (16 / 8);
  }
}
