// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul) {
  float matmul_local[32];
  __local float A_shared[1024];
  __local float B_shared[4096];
  matmul_local[(0)] = 0.000000e+00f;
  matmul_local[(8)] = 0.000000e+00f;
  matmul_local[(16)] = 0.000000e+00f;
  matmul_local[(24)] = 0.000000e+00f;
  matmul_local[(1)] = 0.000000e+00f;
  matmul_local[(9)] = 0.000000e+00f;
  matmul_local[(17)] = 0.000000e+00f;
  matmul_local[(25)] = 0.000000e+00f;
  matmul_local[(2)] = 0.000000e+00f;
  matmul_local[(10)] = 0.000000e+00f;
  matmul_local[(18)] = 0.000000e+00f;
  matmul_local[(26)] = 0.000000e+00f;
  matmul_local[(3)] = 0.000000e+00f;
  matmul_local[(11)] = 0.000000e+00f;
  matmul_local[(19)] = 0.000000e+00f;
  matmul_local[(27)] = 0.000000e+00f;
  matmul_local[(4)] = 0.000000e+00f;
  matmul_local[(12)] = 0.000000e+00f;
  matmul_local[(20)] = 0.000000e+00f;
  matmul_local[(28)] = 0.000000e+00f;
  matmul_local[(5)] = 0.000000e+00f;
  matmul_local[(13)] = 0.000000e+00f;
  matmul_local[(21)] = 0.000000e+00f;
  matmul_local[(29)] = 0.000000e+00f;
  matmul_local[(6)] = 0.000000e+00f;
  matmul_local[(14)] = 0.000000e+00f;
  matmul_local[(22)] = 0.000000e+00f;
  matmul_local[(30)] = 0.000000e+00f;
  matmul_local[(7)] = 0.000000e+00f;
  matmul_local[(15)] = 0.000000e+00f;
  matmul_local[(23)] = 0.000000e+00f;
  matmul_local[(31)] = 0.000000e+00f;
  for (int k_outer_outer = 0; k_outer_outer < 392; ++k_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    vstore2(vload2(0, A + ((k_outer_outer * 64) + (((int)get_local_id(0)) * 2))), 0, A_shared + (((int)get_local_id(0)) * 2));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 25088)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 64));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 50176)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 128));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 75264)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 192));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 100352)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 256));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 125440)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 320));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 150528)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 384));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 175616)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 448));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 200704)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 512));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 225792)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 576));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 250880)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 640));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 275968)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 704));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 301056)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 768));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 326144)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 832));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 351232)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 896));
    vstore2(vload2(0, A + (((k_outer_outer * 64) + (((int)get_local_id(0)) * 2)) + 376320)), 0, A_shared + ((((int)get_local_id(0)) * 2) + 960));
    vstore4(vload4(0, B + ((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 8192)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 128));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 16384)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 256));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 24576)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 384));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 32768)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 512));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 40960)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 640));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 49152)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 768));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 57344)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 896));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 65536)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1024));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 73728)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1152));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 81920)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1280));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 90112)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1408));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 98304)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1536));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 106496)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1664));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 114688)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1792));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 122880)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1920));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 131072)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2048));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 139264)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2176));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 147456)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2304));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 155648)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2432));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 163840)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2560));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 172032)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2688));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 180224)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2816));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 188416)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2944));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 196608)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3072));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 204800)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3200));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 212992)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3328));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 221184)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3456));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 229376)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3584));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 237568)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3712));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 245760)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3840));
    vstore4(vload4(0, B + (((((k_outer_outer * 262144) + ((((int)get_local_id(0)) >> 4) * 4096)) + (((int)get_group_id(0)) * 64)) + ((((int)get_local_id(0)) & 15) * 4)) + 253952)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3968));
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int k_outer_inner = 0; k_outer_inner < 4; ++k_outer_inner) {
      for (int i_c_outer_inner = 0; i_c_outer_inner < 4; ++i_c_outer_inner) {
        for (int k_inner = 0; k_inner < 16; ++k_inner) {
          matmul_local[((i_c_outer_inner * 2))] = (matmul_local[((i_c_outer_inner * 2))] + (A_shared[((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner))] * B_shared[((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))))]));
          matmul_local[(((i_c_outer_inner * 2) + 8))] = (matmul_local[(((i_c_outer_inner * 2) + 8))] + (A_shared[((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner))] * B_shared[(((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
          matmul_local[(((i_c_outer_inner * 2) + 16))] = (matmul_local[(((i_c_outer_inner * 2) + 16))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 512))] * B_shared[((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))))]));
          matmul_local[(((i_c_outer_inner * 2) + 24))] = (matmul_local[(((i_c_outer_inner * 2) + 24))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 512))] * B_shared[(((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
          matmul_local[(((i_c_outer_inner * 2) + 1))] = (matmul_local[(((i_c_outer_inner * 2) + 1))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 64))] * B_shared[((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))))]));
          matmul_local[(((i_c_outer_inner * 2) + 9))] = (matmul_local[(((i_c_outer_inner * 2) + 9))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 64))] * B_shared[(((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
          matmul_local[(((i_c_outer_inner * 2) + 17))] = (matmul_local[(((i_c_outer_inner * 2) + 17))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 576))] * B_shared[((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))))]));
          matmul_local[(((i_c_outer_inner * 2) + 25))] = (matmul_local[(((i_c_outer_inner * 2) + 25))] + (A_shared[(((((i_c_outer_inner * 128) + (k_outer_inner * 16)) + k_inner) + 576))] * B_shared[(((((k_outer_inner * 1024) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
        }
      }
    }
  }
  for (int i_inner = 0; i_inner < 8; ++i_inner) {
    matmul[((((i_inner * 4096) + (((int)get_group_id(0)) * 64)) + ((int)get_local_id(0))))] = matmul_local[(i_inner)];
    matmul[(((((i_inner * 4096) + (((int)get_group_id(0)) * 64)) + ((int)get_local_id(0))) + 32))] = matmul_local[((i_inner + 8))];
    matmul[(((((i_inner * 4096) + (((int)get_group_id(0)) * 64)) + ((int)get_local_id(0))) + 32768))] = matmul_local[((i_inner + 16))];
    matmul[(((((i_inner * 4096) + (((int)get_group_id(0)) * 64)) + ((int)get_local_id(0))) + 32800))] = matmul_local[((i_inner + 24))];
  }
}

