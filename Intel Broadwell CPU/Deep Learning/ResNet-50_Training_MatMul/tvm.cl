// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul) {
  float matmul_local[8];
  __local float A_shared[1024];
  __local float B_shared[5120];
  matmul_local[(0)] = 0.000000e+00f;
  matmul_local[(4)] = 0.000000e+00f;
  matmul_local[(1)] = 0.000000e+00f;
  matmul_local[(5)] = 0.000000e+00f;
  matmul_local[(2)] = 0.000000e+00f;
  matmul_local[(6)] = 0.000000e+00f;
  matmul_local[(3)] = 0.000000e+00f;
  matmul_local[(7)] = 0.000000e+00f;
  for (int k_outer_outer = 0; k_outer_outer < 16; ++k_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    if (((int)get_local_id(0)) < 16) {
      A_shared[((((int)get_local_id(0)) * 64))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((int)get_local_id(0)) >> 1) * 2048)) + (k_outer_outer * 128)) + ((((int)get_local_id(0)) & 1) * 64)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 1))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 1) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 1) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 2))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 2) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 2) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 3))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 3) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 3) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 4))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 4) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 4) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 5))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 5) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 5) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 6))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 6) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 6) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 7))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 7) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 7) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 8))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 8) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 8) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 9))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 9) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 9) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 10))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 10) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 10) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 11))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 11) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 11) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 12))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 12) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 12) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 13))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 13) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 13) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 14))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 14) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 14) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 15))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 15) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 15) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 16))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 16) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 16) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 17))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 17) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 17) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 18))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 18) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 18) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 19))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 19) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 19) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 20))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 20) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 20) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 21))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 21) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 21) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 22))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 22) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 22) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 23))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 23) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 23) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 24))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 24) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 24) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 25))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 25) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 25) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 26))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 26) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 26) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 27))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 27) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 27) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 28))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 28) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 28) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 29))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 29) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 29) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 30))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 30) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 30) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 31))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 31) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 31) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 32))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 32) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 32) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 33))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 33) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 33) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 34))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 34) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 34) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 35))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 35) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 35) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 36))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 36) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 36) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 37))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 37) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 37) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 38))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 38) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 38) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 39))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 39) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 39) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 40))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 40) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 40) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 41))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 41) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 41) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 42))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 42) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 42) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 43))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 43) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 43) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 44))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 44) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 44) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 45))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 45) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 45) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 46))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 46) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 46) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 47))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 47) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 47) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 48))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 48) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 48) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 49))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 49) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 49) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 50))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 50) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 50) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 51))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 51) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 51) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 52))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 52) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 52) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 53))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 53) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 53) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 54))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 54) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 54) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 55))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 55) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 55) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 56))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 56) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 56) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 57))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 57) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 57) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 58))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 58) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 58) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 59))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 59) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 59) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 60))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 60) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 60) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 61))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 61) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 61) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 62))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 62) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 62) & 127)))];
    }
    if (((int)get_local_id(0)) < 16) {
      A_shared[(((((int)get_local_id(0)) * 64) + 63))] = A[((((((((int)get_group_id(0)) / 25) * 16384) + ((((((int)get_local_id(0)) * 64) + 63) >> 7) * 2048)) + (k_outer_outer * 128)) + (((((int)get_local_id(0)) * 64) + 63) & 127)))];
    }
    vstore4(vload4(0, B + ((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 4000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 160));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 8000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 320));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 12000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 480));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 16000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 640));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 20000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 800));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 24000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 960));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 28000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1120));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 32000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1280));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 36000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1440));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 40000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1600));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 44000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1760));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 48000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1920));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 52000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2080));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 56000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2240));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 60000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2400));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 64000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2560));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 68000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2720));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 72000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 2880));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 76000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3040));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 80000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3200));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 84000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3360));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 88000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3520));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 92000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3680));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 96000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 3840));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 100000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4000));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 104000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4160));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 108000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4320));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 112000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4480));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 116000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4640));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 120000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4800));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) / 10) * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((((int)get_local_id(0)) % 10) * 4)) + 124000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 4960));
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int k_outer_inner = 0; k_outer_inner < 16; ++k_outer_inner) {
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[((k_outer_inner * 8))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 512))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 128))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 640))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 1))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 513))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 129))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 641))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 2))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 514))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 130))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 642))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 3))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 515))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 131))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 643))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 4))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 516))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 132))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 644))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 5))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 517))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 133))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 645))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 6))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 518))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 134))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 646))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((k_outer_inner * 8) + 7))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((k_outer_inner * 8) + 519))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((k_outer_inner * 8) + 135))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((k_outer_inner * 8) + 647))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 256))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 768))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 384))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 896))] * B_shared[(((k_outer_inner * 320) + ((int)get_local_id(0))))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 257))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 769))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 385))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 897))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 40))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 258))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 770))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 386))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 898))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 80))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 259))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 771))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 387))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 899))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 120))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 260))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 772))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 388))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 900))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 160))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 261))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 773))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 389))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 901))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 200))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 262))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 774))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 390))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 902))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 240))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((k_outer_inner * 8) + 263))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((k_outer_inner * 8) + 775))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((k_outer_inner * 8) + 391))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((k_outer_inner * 8) + 903))] * B_shared[((((k_outer_inner * 320) + ((int)get_local_id(0))) + 280))]));
    }
  }
  for (int i_inner = 0; i_inner < 4; ++i_inner) {
    matmul[((((((((int)get_group_id(0)) / 25) * 8000) + (i_inner * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((int)get_local_id(0))))] = matmul_local[(i_inner)];
    matmul[(((((((((int)get_group_id(0)) / 25) * 8000) + (i_inner * 1000)) + ((((int)get_group_id(0)) % 25) * 40)) + ((int)get_local_id(0))) + 4000))] = matmul_local[((i_inner + 4))];
  }
}

