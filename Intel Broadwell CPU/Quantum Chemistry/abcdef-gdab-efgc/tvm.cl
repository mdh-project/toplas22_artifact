// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gdab_efgc) {
  float tc_abcdef_gdab_efgc_local[32];
  __local float matA_shared[4608];
  __local float matB_shared[768];
  for (int c_c_inner_init = 0; c_c_inner_init < 2; ++c_c_inner_init) {
    tc_abcdef_gdab_efgc_local[((c_c_inner_init * 16))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 1))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 2))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 3))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 4))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 5))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 6))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 7))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 8))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 9))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 10))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 11))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 12))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 13))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 14))] = 0.000000e+00f;
    tc_abcdef_gdab_efgc_local[(((c_c_inner_init * 16) + 15))] = 0.000000e+00f;
  }
  vstore4(vload4(0, matA + ((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4))), 0, matA_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, matA + (((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 24576)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 768));
  vstore4(vload4(0, matA + (((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 49152)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1536));
  vstore4(vload4(0, matA + (((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 73728)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 2304));
  vstore4(vload4(0, matA + (((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 98304)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 3072));
  vstore4(vload4(0, matA + (((((((((((int)get_local_id(0)) / 48) * 6144) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + (((((int)get_local_id(0)) % 48) / 12) * 384)) + ((((int)get_group_id(0)) / 1536) * 96)) + (((((int)get_local_id(0)) % 12) >> 1) * 16)) + (((((int)get_group_id(0)) % 1536) / 768) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 122880)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 3840));
  vstore2(vload2(0, matB + ((((((int)get_group_id(0)) % 24) * 6144) + (((int)get_local_id(0)) * 16)) + (((((int)get_group_id(0)) % 768) / 96) * 2))), 0, matB_shared + (((int)get_local_id(0)) * 2));
  vstore2(vload2(0, matB + (((((((int)get_group_id(0)) % 24) * 6144) + (((int)get_local_id(0)) * 16)) + (((((int)get_group_id(0)) % 768) / 96) * 2)) + 3072)), 0, matB_shared + ((((int)get_local_id(0)) * 2) + 384));
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int g_outer_inner = 0; g_outer_inner < 6; ++g_outer_inner) {
    for (int g_inner = 0; g_inner < 4; ++g_inner) {
      for (int c_c_inner = 0; c_c_inner < 2; ++c_c_inner) {
        tc_abcdef_gdab_efgc_local[((c_c_inner * 16))] = (tc_abcdef_gdab_efgc_local[((c_c_inner * 16))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 1))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 1))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 48))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 2))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 2))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 96))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 3))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 3))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 144))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 4))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 4))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 192))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 5))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 5))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 240))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 6))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 6))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 288))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 7))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 7))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 336))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 8))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 8))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 384))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 9))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 9))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 432))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 10))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 10))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 480))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 11))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 11))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 528))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 12))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 12))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 576))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 13))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 13))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 624))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 14))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 14))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 672))]));
        tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 15))] = (tc_abcdef_gdab_efgc_local[(((c_c_inner * 16) + 15))] + (matA_shared[(((((g_outer_inner * 768) + (g_inner * 192)) + ((((int)get_local_id(0)) & 3) * 48)) + (((int)get_local_id(0)) >> 2)))] * matB_shared[(((((g_outer_inner * 8) + (g_inner * 2)) + c_c_inner) + 720))]));
      }
    }
  }
  for (int c_inner = 0; c_inner < 2; ++c_inner) {
    for (int f_inner = 0; f_inner < 16; ++f_inner) {
      tc_abcdef_gdab_efgc[((((((((((((((int)get_group_id(0)) / 1536) * 9437184) + ((((int)get_local_id(0)) >> 5) * 1572864)) + (((((int)get_group_id(0)) % 1536) / 768) * 786432)) + (((((int)get_local_id(0)) & 31) >> 2) * 98304)) + (((((int)get_group_id(0)) % 768) / 96) * 12288)) + (c_inner * 6144)) + (((((int)get_group_id(0)) % 96) / 24) * 1536)) + ((((int)get_local_id(0)) & 3) * 384)) + ((((int)get_group_id(0)) % 24) * 16)) + f_inner))] = tc_abcdef_gdab_efgc_local[(((c_inner * 16) + f_inner))];
    }
  }
}

