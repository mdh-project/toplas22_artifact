__kernel void tc_abcdef_gdac_efgb_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 1]
                                 [1 * 1]
                                 [8 * 1]
                                 [1 * 1]
                                 [1 * 1]
                                 [1 * 1];
    __private float a_prv[3]
                                 [1]
                                 [1]
                                 [1];
    __private float b_prv[1]
                                 [1]
                                 [3]
                                 [1];
    const size_t wg_1 = get_group_id(2) / (16 * 2 * 1) % (4); {
    {
    const size_t wg_3 = get_group_id(2) / (16) % (2); {
    const size_t wg_4 = get_group_id(2) % (16); {
    const size_t wg_5 = get_group_id(1); {
    {
    {
    const size_t wi_1 = get_local_id(2) / (1 * 1 * 1) % (6); {
    {
    {
    {
    {
    const size_t wi_6 = get_local_id(0); {
    {
    {
    for (size_t glb_2 = 0; glb_2 < 16; ++glb_2) {
    {
    {
    {
    {
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
    {
    {
    {
    {
    {
    {
    {
    {
    {
        c_prv[0 * 1 + 0][0 * 1 + 0][lcl_3 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 8; ++glb_7) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
        {
        {
        {
        {
            {
               
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    a_prv[prv_7][0][0][0]
                                 =
                    a_glb[(glb_7 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7) * (16) * (24) * (16) + (0 * 16 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (24) * (16) + (0 * 4 * 1 * 6 * 1 + wg_1 * 1 * 6 * 1 + 0 * 6 * 1 + wi_1 * 1 + 0) * (16) + (0 * 2 * 8 * 1 * 1 + wg_3 * 8 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
               
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    b_prv[0][0][prv_7][0]
                                 =
                    b_glb[(0 * 24 * 1 * 1 * 1 + wg_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (0 * 1 * 1 * 16 * 1 + 0 * 1 * 16 * 1 + 0 * 16 * 1 + wi_6 * 1 + 0) * (24) * (16) + (glb_7 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7) * (16) + (glb_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            {
            {
            {
            for (size_t prv_7 = 0; prv_7 < 3; ++prv_7) {
            c_prv[0 * 1 + 0][0 * 1 + 0][lcl_3 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
                +=
            a_prv[prv_7][0][0][0]
            *
            b_prv[0][0][prv_7][0]
            ;
            }}}}}}}
        }}}}}}}
    }
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
    {
    {
    {
    {
    {
    {
    {
    {
    {
        c_glb[(0 * 4 * 1 * 6 * 1 + wg_1 * 1 * 6 * 1 + 0 * 6 * 1 + wi_1 * 1 + 0) * (16) * (16) * (16) * (24) * (16) + (glb_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (24) * (16) + (0 * 2 * 8 * 1 * 1 + wg_3 * 8 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (0 * 16 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (24) * (16) + (0 * 24 * 1 * 1 * 1 + wg_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (0 * 1 * 1 * 16 * 1 + 0 * 1 * 16 * 1 + 0 * 16 * 1 + wi_6 * 1 + 0) ]
                                               =
        c_prv[0 * 1 + 0][0 * 1 + 0][lcl_3 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
                           ;
    }}}}}}}}}}}}
    }
    }}}}}}
    }}}}}}}
    }}}}}}}
}
