// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gdac_efgb) {
  float tc_abcdef_gdac_efgb_local[256];
  __local float matA_shared[4608];
  __local float matB_shared[3072];
  tc_abcdef_gdac_efgb_local[(0)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(32)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(64)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(96)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(128)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(160)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(192)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(224)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(1)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(33)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(65)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(97)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(129)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(161)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(193)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(225)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(2)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(34)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(66)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(98)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(130)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(162)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(194)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(226)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(3)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(35)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(67)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(99)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(131)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(163)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(195)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(227)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(4)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(36)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(68)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(100)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(132)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(164)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(196)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(228)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(5)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(37)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(69)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(101)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(133)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(165)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(197)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(229)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(6)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(38)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(70)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(102)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(134)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(166)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(198)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(230)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(7)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(39)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(71)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(103)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(135)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(167)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(199)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(231)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(8)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(40)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(72)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(104)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(136)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(168)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(200)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(232)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(9)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(41)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(73)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(105)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(137)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(169)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(201)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(233)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(10)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(42)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(74)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(106)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(138)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(170)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(202)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(234)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(11)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(43)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(75)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(107)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(139)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(171)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(203)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(235)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(12)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(44)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(76)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(108)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(140)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(172)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(204)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(236)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(13)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(45)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(77)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(109)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(141)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(173)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(205)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(237)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(14)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(46)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(78)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(110)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(142)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(174)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(206)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(238)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(15)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(47)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(79)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(111)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(143)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(175)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(207)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(239)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(16)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(48)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(80)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(112)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(144)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(176)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(208)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(240)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(17)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(49)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(81)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(113)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(145)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(177)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(209)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(241)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(18)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(50)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(82)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(114)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(146)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(178)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(210)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(242)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(19)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(51)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(83)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(115)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(147)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(179)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(211)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(243)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(20)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(52)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(84)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(116)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(148)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(180)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(212)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(244)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(21)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(53)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(85)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(117)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(149)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(181)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(213)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(245)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(22)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(54)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(86)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(118)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(150)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(182)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(214)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(246)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(23)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(55)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(87)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(119)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(151)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(183)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(215)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(247)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(24)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(56)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(88)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(120)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(152)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(184)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(216)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(248)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(25)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(57)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(89)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(121)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(153)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(185)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(217)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(249)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(26)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(58)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(90)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(122)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(154)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(186)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(218)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(250)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(27)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(59)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(91)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(123)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(155)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(187)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(219)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(251)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(28)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(60)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(92)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(124)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(156)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(188)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(220)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(252)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(29)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(61)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(93)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(125)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(157)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(189)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(221)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(253)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(30)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(62)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(94)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(126)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(158)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(190)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(222)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(254)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(31)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(63)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(95)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(127)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(159)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(191)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(223)] = 0.000000e+00f;
  tc_abcdef_gdac_efgb_local[(255)] = 0.000000e+00f;
  vstore2(vload2(0, matA + ((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2))), 0, matA_shared + (((int)get_local_id(0)) * 2));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 6144)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 192));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 12288)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 384));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 18432)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 576));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 24576)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 768));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 30720)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 960));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 36864)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 1152));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 43008)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 1344));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 49152)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 1536));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 55296)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 1728));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 61440)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 1920));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 67584)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 2112));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 73728)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 2304));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 79872)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 2496));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 86016)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 2688));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 92160)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 2880));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 98304)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 3072));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 104448)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 3264));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 110592)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 3456));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 116736)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 3648));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 122880)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 3840));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 129024)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 4032));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 135168)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 4224));
  vstore2(vload2(0, matA + (((((((((int)get_local_id(0)) / 6) * 384) + ((((int)get_group_id(0)) / 192) * 48)) + (((((int)get_local_id(0)) % 6) >> 1) * 16)) + (((((int)get_group_id(0)) % 48) / 12) * 4)) + ((((int)get_local_id(0)) & 1) * 2)) + 141312)), 0, matA_shared + ((((int)get_local_id(0)) * 2) + 4416));
  if (((int)get_local_id(0)) < 64) {
    matB_shared[((((int)get_local_id(0)) * 48))] = matB[(((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 1))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 1))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 2))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 2))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 3))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 3))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 4))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 16))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 5))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 17))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 6))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 18))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 7))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 19))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 8))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 32))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 9))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 33))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 10))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 34))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 11))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 35))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 12))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 48))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 13))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 49))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 14))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 50))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 15))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 51))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 16))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 64))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 17))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 65))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 18))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 66))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 19))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 67))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 20))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 80))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 21))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 81))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 22))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 82))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 23))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 83))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 24))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 96))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 25))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 97))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 26))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 98))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 27))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 99))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 28))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 112))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 29))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 113))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 30))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 114))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 31))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 115))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 32))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 128))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 33))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 129))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 34))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 130))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 35))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 131))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 36))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 144))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 37))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 145))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 38))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 146))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 39))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 147))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 40))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 160))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 41))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 161))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 42))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 162))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 43))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 163))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 44))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 176))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 45))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 177))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 46))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 178))];
  }
  if (((int)get_local_id(0)) < 64) {
    matB_shared[(((((int)get_local_id(0)) * 48) + 47))] = matB[((((((((int)get_group_id(0)) % 12) * 12288) + (((int)get_local_id(0)) * 192)) + (((((int)get_group_id(0)) % 192) / 48) * 4)) + 179))];
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int e_c_outer_inner = 0; e_c_outer_inner < 2; ++e_c_outer_inner) {
    for (int f_c_outer_inner = 0; f_c_outer_inner < 16; ++f_c_outer_inner) {
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)))] * matB_shared[(((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 12))] * matB_shared[(((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 1))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 12))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 1))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 2))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 12))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 2))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 3))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 12))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 3))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 192))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 4))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 204))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 4))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 192))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 5))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 204))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 5))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 192))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 6))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 204))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 6))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 192))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 7))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 204))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 7))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 384))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 8))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 396))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 8))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 384))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 9))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 396))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 9))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 384))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 10))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 396))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 10))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 384))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 11))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 396))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 11))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 576))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 12))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 588))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 12))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 576))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 13))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 588))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 13))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 576))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 14))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 588))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 14))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 576))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 15))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 588))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 15))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 768))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 16))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 780))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 16))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 768))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 17))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 780))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 17))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 768))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 18))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 780))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 18))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 768))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 19))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 780))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 19))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 960))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 20))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 972))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 20))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 960))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 21))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 972))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 21))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 960))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 22))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 972))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 22))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 960))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 23))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 972))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 23))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1152))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 24))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1164))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 24))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1152))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 25))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1164))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 25))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1152))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 26))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1164))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 26))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1152))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 27))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1164))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 27))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1344))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 28))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1356))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 28))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1344))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 29))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1356))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 29))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1344))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 30))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1356))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 30))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1344))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 31))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1356))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 31))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1536))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 32))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1548))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 32))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1536))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 33))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1548))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 33))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1536))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 34))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1548))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 34))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1536))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 35))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1548))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 35))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1728))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 36))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1740))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 36))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1728))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 37))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1740))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 37))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1728))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 38))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1740))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 38))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1728))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 39))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1740))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 39))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1920))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 40))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1932))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 40))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1920))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 41))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1932))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 41))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1920))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 42))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1932))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 42))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1920))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 43))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 1932))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 43))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2112))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 44))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2124))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 44))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2112))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 45))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2124))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 45))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2112))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 46))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2124))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 46))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2112))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 47))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2124))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 47))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2304))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 48))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2316))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 48))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2304))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 49))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2316))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 49))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2304))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 50))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2316))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 50))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2304))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 51))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2316))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 51))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2496))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 52))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2508))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 52))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2496))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 53))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2508))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 53))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2496))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 54))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2508))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 54))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2496))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 55))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2508))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 55))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2688))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 56))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2700))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 56))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2688))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 57))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2700))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 57))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2688))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 58))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2700))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 58))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2688))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 59))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2700))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 59))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2880))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 60))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2892))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 60))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2880))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 61))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2892))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 61))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2880))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 62))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2892))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 62))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2880))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 63))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 2892))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 63))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3072))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 64))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3084))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 64))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3072))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 65))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3084))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 65))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3072))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 66))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3084))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 66))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3072))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 67))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3084))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 67))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3264))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 68))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3276))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 68))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3264))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 69))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3276))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 69))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3264))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 70))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3276))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 70))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3264))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 71))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3276))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 71))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3456))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 72))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3468))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 72))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3456))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 73))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3468))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 73))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3456))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 74))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3468))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 74))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3456))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 75))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3468))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 75))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3648))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 76))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3660))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 76))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3648))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 77))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3660))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 77))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3648))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 78))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3660))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 78))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3648))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 79))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3660))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 79))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3840))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 80))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3852))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 80))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3840))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 81))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3852))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 81))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3840))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 82))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3852))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 82))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3840))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 83))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 3852))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 83))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4032))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 84))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4044))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 84))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4032))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 85))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4044))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 85))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4032))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 86))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4044))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 86))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4032))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 87))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4044))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 87))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4224))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 88))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4236))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 88))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4224))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 89))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4236))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 89))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4224))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 90))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4236))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 90))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4224))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 91))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4236))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 91))]));
      tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] = (tc_abcdef_gdac_efgb_local[(((e_c_outer_inner * 16) + f_c_outer_inner))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4416))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 92))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 32))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4428))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 92))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 64))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4416))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 93))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 96))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4428))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 93))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 128))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4416))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 94))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 160))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4428))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 94))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 192))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4416))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 95))]));
      tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] = (tc_abcdef_gdac_efgb_local[((((e_c_outer_inner * 16) + f_c_outer_inner) + 224))] + (matA_shared[(((((((int)get_local_id(0)) & 7) * 24) + (((int)get_local_id(0)) >> 3)) + 4428))] * matB_shared[((((e_c_outer_inner * 1536) + (f_c_outer_inner * 96)) + 95))]));
    }
  }
  for (int b_inner = 0; b_inner < 4; ++b_inner) {
    for (int d_inner = 0; d_inner < 2; ++d_inner) {
      for (int e_inner = 0; e_inner < 2; ++e_inner) {
        for (int f_inner = 0; f_inner < 16; ++f_inner) {
          tc_abcdef_gdac_efgb[((((((((((((((int)get_group_id(0)) / 192) * 4718592) + ((((int)get_local_id(0)) >> 5) * 1572864)) + (((((int)get_group_id(0)) % 192) / 48) * 393216)) + (b_inner * 98304)) + (((((int)get_group_id(0)) % 48) / 12) * 24576)) + ((((int)get_local_id(0)) & 31) * 768)) + (d_inner * 384)) + ((((int)get_group_id(0)) % 12) * 32)) + (e_inner * 16)) + f_inner))] = tc_abcdef_gdac_efgb_local[(((((b_inner * 64) + (d_inner * 32)) + (e_inner * 16)) + f_inner))];
        }
      }
    }
  }
}

