__kernel void tc_abcdef_geab_dfgc_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[2 * 1]
                                 [2 * 2]
                                 [1 * 1]
                                 [1 * 1]
                                 [1 * 1]
                                 [1 * 1];
    __private float b_prv[1]
                                 [1]
                                 [1]
                                 [1];
    const size_t wg_1 = get_group_id(2) / (24 * 2 * 1) % (2); {
    {
    const size_t wg_3 = get_group_id(2) / (24) % (2); {
    const size_t wg_4 = get_group_id(2) % (24); {
    const size_t wg_5 = get_group_id(1); {
    {
    {
    const size_t wi_1 = get_local_id(2) / (1 * 2 * 1) % (3); {
    {
    const size_t wi_3 = get_local_id(2) / (1) % (2); {
    {
    {
    const size_t wi_6 = get_local_id(0); {
    {
    for (size_t glb_1 = 0; glb_1 < 2; ++glb_1) {
    for (size_t glb_2 = 0; glb_2 < 4; ++glb_2) {
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
    {
    {
    for (size_t glb_6 = 0; glb_6 < 2; ++glb_6) {
    {
    for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    {
    {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 2; ++prv_2) {
    {
    {
    {
    {
        c_prv[lcl_1 * 1 + 0][lcl_2 * 2 + prv_2][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 24; ++glb_7) {
        for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
        for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
        {
        {
        {
        {
        {
            {
               
               
               
               
                    b_prv[0][0][0][0]
                                 =
                    b_glb[(0 * 24 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (glb_6 * 1 * 1 * 8 * 1 + 0 * 1 * 8 * 1 + 0 * 8 * 1 + wi_6 * 1 + 0) * (24) * (16) + (glb_7 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (glb_3 * 2 * 1 * 2 * 1 + wg_3 * 1 * 2 * 1 + 0 * 2 * 1 + wi_3 * 1 + 0) ]
                        ;
            }
            {
            for (size_t prv_2 = 0; prv_2 < 2; ++prv_2) {
            {
            {
            {
            {
            {
            c_prv[lcl_1 * 1 + 0][lcl_2 * 2 + prv_2][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
                +=
            a_glb[(glb_7 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (0 * 16 * 1 * 1 * 1 + wg_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (24) * (16) + (glb_1 * 2 * 2 * 3 * 1 + wg_1 * 2 * 3 * 1 + lcl_1 * 3 * 1 + wi_1 * 1 + 0) * (16) + (glb_2 * 1 * 2 * 1 * 2 + 0 * 2 * 1 * 2 + lcl_2 * 1 * 2 + 0 * 2 + prv_2) ]
            *
            b_prv[0][0][0][0]
            ;
            }}}}}}}
        }}}}}}}
    }
    {
    for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    {
    {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 2; ++prv_2) {
    {
    {
    {
    {
        c_glb[(glb_1 * 2 * 2 * 3 * 1 + wg_1 * 2 * 3 * 1 + lcl_1 * 3 * 1 + wi_1 * 1 + 0) * (16) * (16) * (24) * (16) * (16) + (glb_2 * 1 * 2 * 1 * 2 + 0 * 2 * 1 * 2 + lcl_2 * 1 * 2 + 0 * 2 + prv_2) * (16) * (24) * (16) * (16) + (glb_3 * 2 * 1 * 2 * 1 + wg_3 * 1 * 2 * 1 + 0 * 2 * 1 + wi_3 * 1 + 0) * (24) * (16) * (16) + (0 * 24 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) + (0 * 16 * 1 * 1 * 1 + wg_5 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (glb_6 * 1 * 1 * 8 * 1 + 0 * 1 * 8 * 1 + 0 * 8 * 1 + wi_6 * 1 + 0) ]
                                               =
        c_prv[lcl_1 * 1 + 0][lcl_2 * 2 + prv_2][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0][0 * 1 + 0]
                           ;
    }}}}}}}}}}}}
    }
    }}}}}}
    }}}}}}}
    }}}}}}}
}
