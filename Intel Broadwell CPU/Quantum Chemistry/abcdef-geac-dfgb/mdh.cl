__kernel void tc_abcdef_geac_dfgb_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float b_prv[1]
                                 [1]
                                 [3]
                                 [1];
    const size_t wg_1 = get_group_id(2) / (24 * 1 * 8) % (24); {
    const size_t wg_2 = get_group_id(2) / (24 * 1) % (8); {
    {
    const size_t wg_4 = get_group_id(2) % (24); {
    const size_t wg_5 = get_group_id(1); {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_6 = get_local_id(0); {
    {
    {
    for (size_t glb_2 = 0; glb_2 < 2; ++glb_2) {
    for (size_t glb_3 = 0; glb_3 < 2; ++glb_3) {
    {
    {
    for (size_t glb_6 = 0; glb_6 < 2; ++glb_6) {
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
    {
    for (size_t lcl_5 = 0; lcl_5 < 8; ++lcl_5) {
    {
    {
    {
    {
    {
    {
    {
        c_glb[(0 * 24 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (24) * (16) * (16) + (glb_2 * 8 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) * (16) + (glb_3 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) * (24) * (16) * (16) + (0 * 24 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) + (0 * 2 * 8 * 1 * 1 + wg_5 * 8 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) * (16) + (glb_6 * 1 * 1 * 8 * 1 + 0 * 1 * 8 * 1 + 0 * 8 * 1 + wi_6 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 4; ++glb_7) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
        {
        for (size_t lcl_5 = 0; lcl_5 < 8; ++lcl_5) {
        {
        for (size_t lcl_7 = 0; lcl_7 < 2; ++lcl_7) {
            {
               
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    b_prv[0][0][prv_7][0]
                                 =
                    b_glb[(0 * 24 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (glb_6 * 1 * 1 * 8 * 1 + 0 * 1 * 8 * 1 + 0 * 8 * 1 + wi_6 * 1 + 0) * (24) * (16) + (glb_7 * 1 * 2 * 1 * 3 + 0 * 2 * 1 * 3 + lcl_7 * 1 * 3 + 0 * 3 + prv_7) * (16) + (glb_2 * 8 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            {
            {
            {
            for (size_t prv_7 = 0; prv_7 < 3; ++prv_7) {
            c_glb[(0 * 24 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (24) * (16) * (16) + (glb_2 * 8 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) * (16) + (glb_3 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) * (24) * (16) * (16) + (0 * 24 * 1 * 1 * 1 + wg_4 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) + (0 * 2 * 8 * 1 * 1 + wg_5 * 8 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) * (16) + (glb_6 * 1 * 1 * 8 * 1 + 0 * 1 * 8 * 1 + 0 * 8 * 1 + wi_6 * 1 + 0) ]
                +=
            a_glb[(glb_7 * 1 * 2 * 1 * 3 + 0 * 2 * 1 * 3 + lcl_7 * 1 * 3 + 0 * 3 + prv_7) * (16) * (24) * (16) + (0 * 2 * 8 * 1 * 1 + wg_5 * 8 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) * (24) * (16) + (0 * 24 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (glb_3 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) ]
            *
            b_prv[0][0][prv_7][0]
            ;
            }}}}}}}
        }}}}}}}
    }
    }}}}}}
    }}}}}}}
    }}}}}}}
}
