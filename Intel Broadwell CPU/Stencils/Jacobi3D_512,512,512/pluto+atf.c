#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 510
    #define N_VAL 510
    #define K_VAL 510

    static float in[M_VAL + 2][N_VAL + 2][K_VAL + 2];
    static float out[M_VAL][N_VAL][K_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < (M_VAL + 2) * (N_VAL + 2) * (K_VAL + 2); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL * K_VAL; ++i) ((float *)out)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4, t5, t6;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((K_VAL >= 1) && (M_VAL >= 1) && (N_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,16);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4,t5,t6)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(N_VAL-1,6);t2++) {
      for (t3=0;t3<=floord(K_VAL-1,427);t3++) {
        for (t4=16*t1;t4<=min(M_VAL-1,16*t1+15);t4++) {
          for (t5=6*t2;t5<=min(N_VAL-1,6*t2+5);t5++) {
            lbv=427*t3;
            ubv=min(K_VAL-1,427*t3+426);
#pragma ivdep
#pragma vector always
            for (t6=lbv;t6<=ubv;t6++) {
              out[t4][t5][t6] = 2.0f * in[t4 + 1][t5 + 1][t6 + 2] + 3.0f * in[t4 + 1][t5 + 1][t6 + 0] + 4.0f * in[t4 + 1][t5 + 2][t6 + 1] + 5.0f * in[t4 + 1][t5 + 0][t6 + 1] + 6.0f * in[t4 + 2][t5 + 1][t6 + 1] + 7.0f * in[t4 + 0][t5 + 1][t6 + 1] - 8.0f * in[t4 + 1][t5 + 1][t6 + 1] ;;
            }
          }
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // GEN: gold check float out M_VAL N_VAL K_VAL

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif

    out[0][0][0] += 1; // prevent dead code elimination
}
