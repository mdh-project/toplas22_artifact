#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 510
    #define N_VAL 510
    #define K_VAL 510

    static float in[M_VAL + 2][N_VAL + 2][K_VAL + 2];
    static float out[M_VAL][N_VAL][K_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < (M_VAL + 2) * (N_VAL + 2) * (K_VAL + 2); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL * K_VAL; ++i) ((float *)out)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4, t5, t6;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((K_VAL >= 1) && (M_VAL >= 1) && (N_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,32);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4,t5,t6)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(N_VAL-1,32);t2++) {
      for (t3=0;t3<=floord(K_VAL-1,32);t3++) {
        for (t4=32*t1;t4<=min(M_VAL-1,32*t1+31);t4++) {
          for (t5=32*t2;t5<=min(N_VAL-1,32*t2+31);t5++) {
            lbv=32*t3;
            ubv=min(K_VAL-1,32*t3+31);
#pragma ivdep
#pragma vector always
            for (t6=lbv;t6<=ubv;t6++) {
              out[t4][t5][t6] = 2.0f * in[t4 + 1][t5 + 1][t6 + 2] + 3.0f * in[t4 + 1][t5 + 1][t6 + 0] + 4.0f * in[t4 + 1][t5 + 2][t6 + 1] + 5.0f * in[t4 + 1][t5 + 0][t6 + 1] + 6.0f * in[t4 + 2][t5 + 1][t6 + 1] + 7.0f * in[t4 + 0][t5 + 1][t6 + 1] - 8.0f * in[t4 + 1][t5 + 1][t6 + 1] ;;
            }
          }
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float out M_VAL N_VAL K_VAL
    {
        float out_gold[M_VAL][N_VAL][K_VAL];
        FILE *out_gold_file = fopen("../../../../../../gold/Pluto/12e075a/jacobi/510x510x510/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(out_gold_file, "%f", &(((float*)out_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*M_VAL*N_VAL*K_VAL) {
                printf("incorrect result buffer size for buffer out: expected %d, actual %d.", i, 1*M_VAL*N_VAL*K_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(out_gold_file);

        for (int i = 0; i < 1*M_VAL*N_VAL*K_VAL; ++i) {
            if (0.1 == 0) {
                if (((float*)out)[i] != ((float*)out_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)out_gold)[i], ((float*)out)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)out)[i] - ((float*)out_gold)[i]) > 0.1) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)out_gold)[i], ((float*)out)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
