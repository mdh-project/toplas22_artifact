#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 220
    #define N_VAL 220

    static float in[M_VAL + 4][N_VAL + 4];
    static float out[M_VAL][N_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < (M_VAL + 4) * (N_VAL + 4); ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL * N_VAL; ++i) ((float *)out)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((M_VAL >= 1) && (N_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,20);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(N_VAL-1,93);t2++) {
      for (t3=20*t1;t3<=min(M_VAL-1,20*t1+19);t3++) {
        lbv=93*t2;
        ubv=min(N_VAL-1,93*t2+92);
#pragma ivdep
#pragma vector always
        for (t4=lbv;t4<=ubv;t4++) {
          out[t3][t4] = (2.0f * in[(2 + t3 - 2)][(2 + t4 - 2)] + 4.0f * in[(2 + t3 - 2)][(2 + t4 - 1)] + 5.0f * in[(2 + t3 - 2)][(2 + t4 - 0)] + 4.0f * in[(2 + t3 - 2)][(2 + t4 + 1)] + 2.0f * in[(2 + t3 - 2)][(2 + t4 + 2)] + 4.0f * in[(2 + t3 - 1)][(2 + t4 - 2)] + 9.0f * in[(2 + t3 - 1)][(2 + t4 - 1)] + 12.0f * in[(2 + t3 - 1)][(2 + t4 - 0)] + 9.0f * in[(2 + t3 - 1)][(2 + t4 + 1)] + 4.0f * in[(2 + t3 - 1)][(2 + t4 + 2)] + 5.0f * in[(2 + t3 - 0)][(2 + t4 - 2)] + 12.0f * in[(2 + t3 - 0)][(2 + t4 - 1)] + 15.0f * in[(2 + t3 - 0)][(2 + t4 - 0)] + 12.0f * in[(2 + t3 - 0)][(2 + t4 + 1)] + 5.0f * in[(2 + t3 - 0)][(2 + t4 + 2)] + 4.0f * in[(2 + t3 + 1)][(2 + t4 - 2)] + 9.0f * in[(2 + t3 + 1)][(2 + t4 - 1)] + 12.0f * in[(2 + t3 + 1)][(2 + t4 - 0)] + 9.0f * in[(2 + t3 + 1)][(2 + t4 + 1)] + 4.0f * in[(2 + t3 + 1)][(2 + t4 + 2)] + 2.0f * in[(2 + t3 + 2)][(2 + t4 - 2)] + 4.0f * in[(2 + t3 + 2)][(2 + t4 - 1)] + 5.0f * in[(2 + t3 + 2)][(2 + t4 - 0)] + 4.0f * in[(2 + t3 + 2)][(2 + t4 + 1)] + 2.0f * in[(2 + t3 + 2)][(2 + t4 + 2)]) / 159.0f;;
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // GEN: gold check float out M_VAL N_VAL

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif

    out[0][0] += 1; // prevent dead code elimination
}
