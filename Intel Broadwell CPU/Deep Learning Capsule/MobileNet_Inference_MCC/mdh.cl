__kernel void mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[(1 * 1) *
                                    (1 * 1) *
                                    (1 * 7) *
                                    (1 * 1) *
                                    (1 * 2) *
                                    (1 * 1)];
    __private float filter_prv[(1) *
                                       (1) *
                                       (1) *
                                       (1) *
                                       (1) *
                                       (1)];
    {
    const size_t wg_2 = get_group_id(0) / (1 * 1 * 4 * 16) % (112); {
    const size_t wg_3 = get_group_id(0) / (1 * 1 * 4) % (16); {
    const size_t wg_4 = get_group_id(0) / (1 * 1) % (4); {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_4 = get_local_id(0) / (2 * 1) % (8); {
    {
    const size_t wi_6 = get_local_id(0) % (2); {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t glb_5 = 0; glb_5 < 2; ++glb_5) {
    for (size_t glb_6 = 0; glb_6 < 2; ++glb_6) {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
    {
    for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
    {
        out_prv[(0 * 1 + 0) * (1 * 1) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 7 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 3; ++glb_7) {
    for (size_t glb_8 = 0; glb_8 < 3; ++glb_8) {
    for (size_t glb_9 = 0; glb_9 < 3; ++glb_9) {
    for (size_t glb_10 = 0; glb_10 < 4; ++glb_10) {
        {
        {
        {
        {
        {
        {
        {
        {
        {
        {
            {
               
               
               
               
               
               
                    filter_prv[(0) * (1) * (1) * (1) * (1) * (1) + (0) * (1) * (1) * (1) * (1) + (0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
                                       =
                    filter_glb[(wg_4 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) * (3) * (3) * (3) * (4) * (4) + (0 * 3 * 1 * 1 * 1 + glb_8 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) * (3) * (4) * (4) + (0 * 3 * 1 * 1 * 1 + glb_9 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) * (4) * (4) + (0 * 3 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (4) * (4) + (0 * 4 * 1 * 1 * 1 + glb_10 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (4) + (0 * 2 * 2 * 1 * 1 + glb_6 * 2 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            {
            {
            {
            for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
            {
            for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
            {
            out_prv[(0 * 1 + 0) * (1 * 1) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 7 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
                +=
            images_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((2 * 112 - 1 + 3 - 1)) * ((2 * 112 - 1 + 3 - 1)) * (3) * (4) * (4) + ((wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * 2 + (0 * 3 * 1 * 1 * 1 + glb_8 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 - 1 + 3 - 1)) * (3) * (4) * (4) + ((wg_3 * 1 * 1 * 1 * 7 + 0 * 1 * 1 * 7 + 0 * 1 * 7 + 0 * 7 + prv_3) * 2 + (0 * 3 * 1 * 1 * 1 + glb_9 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * (3) * (4) * (4) + (0 * 3 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (4) * (4) + (0 * 2 * 1 * 1 * 2 + glb_5 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_5) * (4) + (0 * 4 * 1 * 1 * 1 + glb_10 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
            *
            filter_prv[(0) * (1) * (1) * (1) * (1) * (1) + (0) * (1) * (1) * (1) * (1) + (0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
            ;
            }}}}}}}}}}
        }}}}}}}}}}
    }}}}
    {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 7; ++prv_3) {
    {
    for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
    {
        out_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (112) * (32) * (4) * (4) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (32) * (4) * (4) + (wg_3 * 1 * 1 * 1 * 7 + 0 * 1 * 1 * 7 + 0 * 1 * 7 + 0 * 7 + prv_3) * (32) * (4) * (4) + (wg_4 * 1 * 8 * 1 * 1 + 0 * 8 * 1 * 1 + wi_4 * 1 * 1 + 0 * 1 + 0) * (4) * (4) + (0 * 2 * 1 * 1 * 2 + glb_5 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_5) * (4) + (0 * 2 * 2 * 1 * 1 + glb_6 * 2 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) ]
                                                 =
        out_prv[(0 * 1 + 0) * (1 * 1) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 7) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 7 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
                             ;
    }}}}}}}}}}}}
    }
    }}}}}}
    }}}}}}}}}}
    }}}}}}}}}}
}
