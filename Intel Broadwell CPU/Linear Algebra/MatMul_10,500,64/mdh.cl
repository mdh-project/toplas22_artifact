__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[5 * 1]
                                 [2 * 1];
    __private float a_prv[1]
                                 [1];
    {
    const size_t wg_2 = get_group_id(0); {
    {
    {
    const size_t wi_2 = get_local_id(0); {
    {
    for (size_t glb_1 = 0; glb_1 < 2; ++glb_1) {
    {
    {
    for (size_t lcl_1 = 0; lcl_1 < 5; ++lcl_1) {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    {
        c_prv[lcl_1 * 1 + 0][lcl_2 * 1 + 0]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 16; ++glb_3) {
        for (size_t lcl_1 = 0; lcl_1 < 5; ++lcl_1) {
        for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
        for (size_t lcl_3 = 0; lcl_3 < 4; ++lcl_3) {
            {
               
               
                    a_prv[0][0]
                                 =
                    a_glb[(glb_1 * 1 * 5 * 1 * 1 + 0 * 5 * 1 * 1 + lcl_1 * 1 * 1 + 0 * 1 + 0) * (64) + (glb_3 * 1 * 4 * 1 * 1 + 0 * 4 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            c_prv[lcl_1 * 1 + 0][lcl_2 * 1 + 0]
                +=
            a_prv[0][0]
            *
            b_glb[(glb_3 * 1 * 4 * 1 * 1 + 0 * 4 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) * (500) + (0 * 25 * 2 * 10 * 1 + wg_2 * 2 * 10 * 1 + lcl_2 * 10 * 1 + wi_2 * 1 + 0) ]
            ;
            }}}
        }}}
    }
    {
    for (size_t lcl_1 = 0; lcl_1 < 5; ++lcl_1) {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    {
        c_glb[(glb_1 * 1 * 5 * 1 * 1 + 0 * 5 * 1 * 1 + lcl_1 * 1 * 1 + 0 * 1 + 0) * (500) + (0 * 25 * 2 * 10 * 1 + wg_2 * 2 * 10 * 1 + lcl_2 * 10 * 1 + wi_2 * 1 + 0) ]
                                               =
        c_prv[lcl_1 * 1 + 0][lcl_2 * 1 + 0]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
