__kernel void matvec_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    const size_t wg_1 = get_group_id(0) % (512); {
    {
    {
    {
    {
    {
    for (size_t lcl_1 = 0; lcl_1 < 4; ++lcl_1) {
    for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
        c_glb[(wg_1 * 1 * 1 * 4 * 2 + 0 * 1 * 4 * 2 + 0 * 4 * 2 + lcl_1 * 2 + prv_1)]
            = 0.0f;
    }}
    }
    for (size_t glb_2 = 0; glb_2 < 1024; ++glb_2) {
        for (size_t lcl_2 = 0; lcl_2 < 4; ++lcl_2) {
        for (size_t lcl_1 = 0; lcl_1 < 4; ++lcl_1) {
            {
            for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
            c_glb[(wg_1 * 1 * 1 * 4 * 2 + 0 * 1 * 4 * 2 + 0 * 4 * 2 + lcl_1 * 2 + prv_1)]
                +=
            a_glb[(wg_1 * 1 * 1 * 4 * 2 + 0 * 1 * 4 * 2 + 0 * 4 * 2 + lcl_1 * 2 + prv_1) * (4096) + (0 * 1024 * 1 * 4 * 1 + glb_2 * 1 * 4 * 1 + 0 * 4 * 1 + lcl_2 * 1 + 0) ]
            *
            b_glb[(0 * 1024 * 1 * 4 * 1 + glb_2 * 1 * 4 * 1 + 0 * 4 * 1 + lcl_2 * 1 + 0)]
            ;
            }}
        }}
    }
    }
    }}
    }}
}
