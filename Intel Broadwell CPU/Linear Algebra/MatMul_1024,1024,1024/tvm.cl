// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul) {
  float matmul_local[32];
  __local float A_shared[1024];
  __local float B_shared[4096];
  for (int i_c_outer_inner_init = 0; i_c_outer_inner_init < 4; ++i_c_outer_inner_init) {
    for (int i_c_inner_init = 0; i_c_inner_init < 2; ++i_c_inner_init) {
      matmul_local[(((i_c_outer_inner_init * 2) + i_c_inner_init))] = 0.000000e+00f;
      matmul_local[((((i_c_outer_inner_init * 2) + i_c_inner_init) + 8))] = 0.000000e+00f;
      matmul_local[((((i_c_outer_inner_init * 2) + i_c_inner_init) + 16))] = 0.000000e+00f;
      matmul_local[((((i_c_outer_inner_init * 2) + i_c_inner_init) + 24))] = 0.000000e+00f;
    }
  }
  for (int k_outer_outer = 0; k_outer_outer < 16; ++k_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int ax0_ax1_fused_outer_outer = 0; ax0_ax1_fused_outer_outer < 8; ++ax0_ax1_fused_outer_outer) {
      vstore4(vload4(0, A + ((((((((int)get_group_id(0)) >> 4) * 16384) + (ax0_ax1_fused_outer_outer * 2048)) + ((((int)get_local_id(0)) >> 4) * 1024)) + (k_outer_outer * 64)) + ((((int)get_local_id(0)) & 15) * 4))), 0, A_shared + ((ax0_ax1_fused_outer_outer * 128) + (((int)get_local_id(0)) * 4)));
    }
    for (int ax0_ax1_fused_outer_outer1 = 0; ax0_ax1_fused_outer_outer1 < 32; ++ax0_ax1_fused_outer_outer1) {
      vstore4(vload4(0, B + (((((k_outer_outer * 65536) + (ax0_ax1_fused_outer_outer1 * 2048)) + ((((int)get_local_id(0)) >> 4) * 1024)) + ((((int)get_group_id(0)) & 15) * 64)) + ((((int)get_local_id(0)) & 15) * 4))), 0, B_shared + ((ax0_ax1_fused_outer_outer1 * 128) + (((int)get_local_id(0)) * 4)));
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int k_outer_inner = 0; k_outer_inner < 2; ++k_outer_inner) {
      for (int i_c_outer_inner = 0; i_c_outer_inner < 4; ++i_c_outer_inner) {
        for (int k_inner = 0; k_inner < 32; ++k_inner) {
          for (int i_c_inner = 0; i_c_inner < 2; ++i_c_inner) {
            matmul_local[(((i_c_outer_inner * 2) + i_c_inner))] = (matmul_local[(((i_c_outer_inner * 2) + i_c_inner))] + (A_shared[(((((i_c_outer_inner * 128) + (i_c_inner * 64)) + (k_outer_inner * 32)) + k_inner))] * B_shared[((((k_outer_inner * 2048) + (k_inner * 64)) + ((int)get_local_id(0))))]));
            matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 8))] = (matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 8))] + (A_shared[(((((i_c_outer_inner * 128) + (i_c_inner * 64)) + (k_outer_inner * 32)) + k_inner))] * B_shared[(((((k_outer_inner * 2048) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
            matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 16))] = (matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 16))] + (A_shared[((((((i_c_outer_inner * 128) + (i_c_inner * 64)) + (k_outer_inner * 32)) + k_inner) + 512))] * B_shared[((((k_outer_inner * 2048) + (k_inner * 64)) + ((int)get_local_id(0))))]));
            matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 24))] = (matmul_local[((((i_c_outer_inner * 2) + i_c_inner) + 24))] + (A_shared[((((((i_c_outer_inner * 128) + (i_c_inner * 64)) + (k_outer_inner * 32)) + k_inner) + 512))] * B_shared[(((((k_outer_inner * 2048) + (k_inner * 64)) + ((int)get_local_id(0))) + 32))]));
          }
        }
      }
    }
  }
  for (int i_inner = 0; i_inner < 8; ++i_inner) {
    matmul[((((((((int)get_group_id(0)) >> 4) * 16384) + (i_inner * 1024)) + ((((int)get_group_id(0)) & 15) * 64)) + ((int)get_local_id(0))))] = matmul_local[(i_inner)];
    matmul[(((((((((int)get_group_id(0)) >> 4) * 16384) + (i_inner * 1024)) + ((((int)get_group_id(0)) & 15) * 64)) + ((int)get_local_id(0))) + 32))] = matmul_local[((i_inner + 8))];
    matmul[(((((((((int)get_group_id(0)) >> 4) * 16384) + (i_inner * 1024)) + ((((int)get_group_id(0)) & 15) * 64)) + ((int)get_local_id(0))) + 8192))] = matmul_local[((i_inner + 16))];
    matmul[(((((((((int)get_group_id(0)) >> 4) * 16384) + (i_inner * 1024)) + ((((int)get_group_id(0)) & 15) * 64)) + ((int)get_local_id(0))) + 8224))] = matmul_local[((i_inner + 24))];
  }
}

