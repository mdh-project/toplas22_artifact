__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 8]
                                 [2 * 1];
    __private float b_prv[1]
                                 [1];
    const size_t wg_1 = get_group_id(1); {
    const size_t wg_2 = get_group_id(0); {
    {
    {
    const size_t wi_2 = get_local_id(0); {
    {
    for (size_t glb_1 = 0; glb_1 < 2; ++glb_1) {
    for (size_t glb_2 = 0; glb_2 < 4; ++glb_2) {
    {
    {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_prv[0 * 8 + prv_1][lcl_2 * 1 + 0]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 32; ++glb_3) {
        {
        for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
        for (size_t lcl_3 = 0; lcl_3 < 32; ++lcl_3) {
            {
               
               
                    b_prv[0][0]
                                 =
                    b_glb[(glb_3 * 1 * 32 * 1 * 1 + 0 * 32 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) * (1024) + (glb_2 * 16 * 2 * 8 * 1 + wg_2 * 2 * 8 * 1 + lcl_2 * 8 * 1 + wi_2 * 1 + 0) ]
                        ;
            }
            for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
            {
            {
            c_prv[0 * 8 + prv_1][lcl_2 * 1 + 0]
                +=
            a_glb[(glb_1 * 64 * 1 * 1 * 8 + wg_1 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_1) * (1024) + (glb_3 * 1 * 32 * 1 * 1 + 0 * 32 * 1 * 1 + lcl_3 * 1 * 1 + 0 * 1 + 0) ]
            *
            b_prv[0][0]
            ;
            }}}
        }}}
    }
    {
    {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_glb[(glb_1 * 64 * 1 * 1 * 8 + wg_1 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_1) * (1024) + (glb_2 * 16 * 2 * 8 * 1 + wg_2 * 2 * 8 * 1 + lcl_2 * 8 * 1 + wi_2 * 1 + 0) ]
                                               =
        c_prv[0 * 8 + prv_1][lcl_2 * 1 + 0]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
