__kernel void matmul_col_major_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[(1 * 10) *
                                  (1 * 1)];
    __private float a_prv[(2) *
                                  (1)];
    __private float b_prv[(10) *
                                  (2)];
    {
    const size_t wg_2 = get_group_id(0) % (50); {
    {
    const size_t wi_1 = get_local_id(0) / (1) % (10); {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
        c_prv[(0 * 10 + prv_2) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
        for (size_t lcl_3 = 0; lcl_3 < 8; ++lcl_3) {
        {
        {
            {
               
                for (size_t prv_3 = 0; prv_3 < 2; ++prv_3)
                    a_prv[(prv_3) * (1) + (0) ]
                                 =
                    a_glb[(0 * 4 * 1 * 8 * 2 + glb_3 * 1 * 8 * 2 + 0 * 8 * 2 + lcl_3 * 2 + prv_3) * (10) + (0 * 1 * 10 * 1 * 1 + 0 * 10 * 1 * 1 + wi_1 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
                for (size_t prv_2 = 0; prv_2 < 10; ++prv_2)
                for (size_t prv_3 = 0; prv_3 < 2; ++prv_3)
                    b_prv[(prv_2) * (2) + (prv_3) ]
                                 =
                    b_glb[(wg_2 * 1 * 1 * 1 * 10 + 0 * 1 * 1 * 10 + 0 * 1 * 10 + 0 * 10 + prv_2) * (64) + (0 * 4 * 1 * 8 * 2 + glb_3 * 1 * 8 * 2 + 0 * 8 * 2 + lcl_3 * 2 + prv_3) ]
                        ;
            }
            for (size_t prv_3 = 0; prv_3 < 2; ++prv_3) {
            {
            for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
            c_prv[(0 * 10 + prv_2) * (1 * 1) + (0 * 1 + 0) ]
                +=
            a_prv[(prv_3) * (1) + (0) ]
            *
            b_prv[(prv_2) * (2) + (prv_3) ]
            ;
            }}}
        }}}
    }
    {
    {
    {
    {
    for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
        c_glb[(wg_2 * 1 * 1 * 1 * 10 + 0 * 1 * 1 * 10 + 0 * 1 * 10 + 0 * 10 + prv_2) * (10) + (0 * 1 * 10 * 1 * 1 + 0 * 10 * 1 * 1 + wi_1 * 1 * 1 + 0 * 1 + 0) ]
                                               =
        c_prv[(0 * 10 + prv_2) * (1 * 1) + (0 * 1 + 0) ]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
