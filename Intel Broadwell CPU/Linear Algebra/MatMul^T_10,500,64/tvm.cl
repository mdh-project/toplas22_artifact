// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul_col_major) {
  float matmul_col_major_local[10];
  __local float A_shared[640];
  __local float B_shared[640];
  matmul_col_major_local[(0)] = 0.000000e+00f;
  matmul_col_major_local[(1)] = 0.000000e+00f;
  matmul_col_major_local[(2)] = 0.000000e+00f;
  matmul_col_major_local[(3)] = 0.000000e+00f;
  matmul_col_major_local[(4)] = 0.000000e+00f;
  matmul_col_major_local[(5)] = 0.000000e+00f;
  matmul_col_major_local[(6)] = 0.000000e+00f;
  matmul_col_major_local[(7)] = 0.000000e+00f;
  matmul_col_major_local[(8)] = 0.000000e+00f;
  matmul_col_major_local[(9)] = 0.000000e+00f;
  vstore4(vload4(0, A + (((int)get_local_id(0)) * 4)), 0, A_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 40)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 40));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 80)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 80));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 120)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 120));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 160)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 160));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 200)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 200));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 240)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 240));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 280)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 280));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 320)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 320));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 360)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 360));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 400)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 400));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 440)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 440));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 480)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 480));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 520)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 520));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 560)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 560));
  vstore4(vload4(0, A + ((((int)get_local_id(0)) * 4) + 600)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 600));
  vstore4(vload4(0, B + ((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 40)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 40));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 80)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 80));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 120)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 120));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 160)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 160));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 200)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 200));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 240)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 240));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 280)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 280));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 320)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 320));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 360)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 360));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 400)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 400));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 440)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 440));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 480)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 480));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 520)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 520));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 560)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 560));
  vstore4(vload4(0, B + (((((int)get_group_id(0)) * 640) + (((int)get_local_id(0)) * 4)) + 600)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 600));
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int k_outer_inner = 0; k_outer_inner < 32; ++k_outer_inner) {
    for (int i_c_outer_inner = 0; i_c_outer_inner < 2; ++i_c_outer_inner) {
      matmul_col_major_local[((i_c_outer_inner * 5))] = (matmul_col_major_local[((i_c_outer_inner * 5))] + (A_shared[(((k_outer_inner * 20) + (i_c_outer_inner * 5)))] * B_shared[(((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 1))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 1))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 1))] * B_shared[(((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 2))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 2))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 2))] * B_shared[(((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 3))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 3))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 3))] * B_shared[(((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 4))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 4))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 4))] * B_shared[(((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)))]));
      matmul_col_major_local[((i_c_outer_inner * 5))] = (matmul_col_major_local[((i_c_outer_inner * 5))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 10))] * B_shared[((((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)) + 1))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 1))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 1))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 11))] * B_shared[((((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)) + 1))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 2))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 2))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 12))] * B_shared[((((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)) + 1))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 3))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 3))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 13))] * B_shared[((((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)) + 1))]));
      matmul_col_major_local[(((i_c_outer_inner * 5) + 4))] = (matmul_col_major_local[(((i_c_outer_inner * 5) + 4))] + (A_shared[((((k_outer_inner * 20) + (i_c_outer_inner * 5)) + 14))] * B_shared[((((((int)get_local_id(0)) * 64) + (k_outer_inner * 2)) + 1))]));
    }
  }
  for (int i_inner = 0; i_inner < 10; ++i_inner) {
    matmul_col_major[((((((int)get_group_id(0)) * 100) + (((int)get_local_id(0)) * 10)) + i_inner))] = matmul_col_major_local[(i_inner)];
  }
}

