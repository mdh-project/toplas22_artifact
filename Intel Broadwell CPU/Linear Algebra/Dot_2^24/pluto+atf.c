#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define N_VAL 16777216

    static float A[N_VAL];
    static float B[N_VAL];
    static float C[1];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < N_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < 1; ++i) ((float *)C)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if (N_VAL >= 1) {
  for (t1=0;t1<=N_VAL-1;t1++) {
    C[0] += A[t1] * B[t1];;
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // GEN: gold check float C 1

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
