__kernel void matvec_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    const size_t wg_1 = get_group_id(0) % (4096); {
    {
    {
    {
    {
    {
    for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
    {
        c_glb[(wg_1 * 1 * 1 * 2 * 1 + 0 * 1 * 2 * 1 + 0 * 2 * 1 + lcl_1 * 1 + 0)]
            = 0.0f;
    }}
    }
    for (size_t glb_2 = 0; glb_2 < 32; ++glb_2) {
        for (size_t lcl_2 = 0; lcl_2 < 16; ++lcl_2) {
        for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
            for (size_t prv_2 = 0; prv_2 < 16; ++prv_2) {
            {
            c_glb[(wg_1 * 1 * 1 * 2 * 1 + 0 * 1 * 2 * 1 + 0 * 2 * 1 + lcl_1 * 1 + 0)]
                +=
            a_glb[(wg_1 * 1 * 1 * 2 * 1 + 0 * 1 * 2 * 1 + 0 * 2 * 1 + lcl_1 * 1 + 0) * (8192) + (0 * 32 * 1 * 16 * 16 + glb_2 * 1 * 16 * 16 + 0 * 16 * 16 + lcl_2 * 16 + prv_2) ]
            *
            b_glb[(0 * 32 * 1 * 16 * 16 + glb_2 * 1 * 16 * 16 + 0 * 16 * 16 + lcl_2 * 16 + prv_2)]
            ;
            }}
        }}
    }
    }
    }}
    }}
}
