#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 8192
    #define K_VAL 8192

    static float A[M_VAL][K_VAL];
    static float B[K_VAL];
    static float C[M_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL; ++i) ((float *)C)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((K_VAL >= 1) && (M_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,4);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(K_VAL-1,1556);t2++) {
      for (t3=4*t1;t3<=(min(M_VAL-1,4*t1+3))-7;t3+=8) {
        for (t4=1556*t2;t4<=min(K_VAL-1,1556*t2+1555);t4++) {
          C[t3] += A[t3][t4] * B[t4];;
          C[(t3+1)] += A[(t3+1)][t4] * B[t4];;
          C[(t3+2)] += A[(t3+2)][t4] * B[t4];;
          C[(t3+3)] += A[(t3+3)][t4] * B[t4];;
          C[(t3+4)] += A[(t3+4)][t4] * B[t4];;
          C[(t3+5)] += A[(t3+5)][t4] * B[t4];;
          C[(t3+6)] += A[(t3+6)][t4] * B[t4];;
          C[(t3+7)] += A[(t3+7)][t4] * B[t4];;
        }
      }
      for (;t3<=min(M_VAL-1,4*t1+3);t3++) {
        for (t4=1556*t2;t4<=min(K_VAL-1,1556*t2+1555);t4++) {
          C[t3] += A[t3][t4] * B[t4];;
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float C M_VAL
    {
        float C_gold[M_VAL];
        FILE *C_gold_file = fopen("/home/r/r_schu41/mdh-project/benchmarks/2022-toplas/Pluto/matvec_row_major_n/8192x8192/build_broadwell/../gold/Pluto/12e075a/matvec_row_major_n/8192x8192/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(C_gold_file, "%f", &(((float*)C_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*M_VAL) {
                printf("incorrect result buffer size for buffer C: expected %d, actual %d.", i, 1*M_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(C_gold_file);

        for (int i = 0; i < 1*M_VAL; ++i) {
            if (0.100000 == 0) {
                if (((float*)C)[i] != ((float*)C_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)C)[i] - ((float*)C_gold)[i]) > 0.100000) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
