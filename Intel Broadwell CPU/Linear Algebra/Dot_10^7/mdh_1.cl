__kernel void dot_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_r_1 = get_group_id(0);
    const size_t i_wi_r_1 = get_local_id(0);
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __private float res_p_c[1];
  __local float c_l_reduction_mem[200];
  l_cb_offset_r_1 = i_wg_r_1 * 200;
  size_t l_step_r_1 = 0;
  p_cb_offset_r_1 = i_wi_r_1 * 1;
  size_t p_step_r_1 = 0;
  size_t p_iteration_r_1 = 0;
  res_p_c[(0)] = a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 200 + 0)) / 200) * (400 * 200) + i_wi_r_1))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 200 + 0)) / 200) * (400 * 200) + i_wi_r_1))];
  p_cb_offset_r_1 += 200 * (1);
  l_cb_offset_r_1 += (400 * 200) * (200 / 200);
  for (l_step_r_1 = 1; l_step_r_1 < ((10000000 / (400 * 200)) / (200 / 200)); ++l_step_r_1) {
    p_cb_offset_r_1 = i_wi_r_1 * 1;
    size_t p_step_r_1 = 0;
#pragma unroll
    for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
      res_p_c[(0)] += a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 200 + 0)) / 200) * (400 * 200) + i_wi_r_1))] * b_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 200 + 0)) / 200) * (400 * 200) + i_wi_r_1))];
    }
    p_cb_offset_r_1 += 200 * (1);
    l_cb_offset_r_1 += (400 * 200) * (200 / 200);
  }
  {
    barrier(CLK_LOCAL_MEM_FENCE);
    {
        {
          c_l_reduction_mem[i_wi_r_1] = res_p_c[(0)];
          }
        res_p_c[(0)] = 0;
      }
    barrier(CLK_LOCAL_MEM_FENCE);
    {
      size_t stride = 1 << (int) floor(log2((float) (200)));
        if (i_wi_r_1 < stride && i_wi_r_1 + stride < (200)) {
          c_l_reduction_mem[i_wi_r_1] += c_l_reduction_mem[((i_wi_r_1) + stride)];
        }
      stride /= 2;
      barrier(CLK_LOCAL_MEM_FENCE);
      for (; stride > 0; stride /= 2) {
          if (i_wi_r_1 < stride) {
            c_l_reduction_mem[i_wi_r_1] += c_l_reduction_mem[((i_wi_r_1) + stride)];
          }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (i_wi_r_1 == 0) {
      int_res_c[i_wg_r_1] = c_l_reduction_mem[i_wi_r_1];
    }
    barrier(CLK_GLOBAL_MEM_FENCE);
  }
}
