// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul_batched) {
  float matmul_batched_local[10];
  __local float A_shared[640];
  __local float B_shared[1280];
  matmul_batched_local[(0)] = 0.000000e+00f;
  matmul_batched_local[(1)] = 0.000000e+00f;
  matmul_batched_local[(2)] = 0.000000e+00f;
  matmul_batched_local[(3)] = 0.000000e+00f;
  matmul_batched_local[(4)] = 0.000000e+00f;
  matmul_batched_local[(5)] = 0.000000e+00f;
  matmul_batched_local[(6)] = 0.000000e+00f;
  matmul_batched_local[(7)] = 0.000000e+00f;
  matmul_batched_local[(8)] = 0.000000e+00f;
  matmul_batched_local[(9)] = 0.000000e+00f;
  vstore4(vload4(0, A + (((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4))), 0, A_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 80)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 80));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 160)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 160));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 240)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 240));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 320)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 320));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 400)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 400));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 480)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 480));
  vstore4(vload4(0, A + ((((((int)get_group_id(0)) / 25) * 640) + (((int)get_local_id(0)) * 4)) + 560)), 0, A_shared + ((((int)get_local_id(0)) * 4) + 560));
  vstore4(vload4(0, B + (((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 2000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 80));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 4000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 160));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 6000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 240));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 8000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 320));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 10000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 400));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 12000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 480));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 14000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 560));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 16000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 640));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 18000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 720));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 20000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 800));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 22000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 880));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 24000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 960));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 26000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1040));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 28000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1120));
  vstore4(vload4(0, B + ((((((((int)get_group_id(0)) / 25) * 32000) + ((((int)get_local_id(0)) / 5) * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) % 5) * 4)) + 30000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 1200));
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int k_outer_inner = 0; k_outer_inner < 64; ++k_outer_inner) {
    matmul_batched_local[(0)] = (matmul_batched_local[(0)] + (A_shared[((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)))]));
    matmul_batched_local[(1)] = (matmul_batched_local[(1)] + (A_shared[((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 1))]));
    matmul_batched_local[(2)] = (matmul_batched_local[(2)] + (A_shared[((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 2))]));
    matmul_batched_local[(3)] = (matmul_batched_local[(3)] + (A_shared[((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 3))]));
    matmul_batched_local[(4)] = (matmul_batched_local[(4)] + (A_shared[((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 4))]));
    matmul_batched_local[(5)] = (matmul_batched_local[(5)] + (A_shared[(((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner) + 64))] * B_shared[(((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)))]));
    matmul_batched_local[(6)] = (matmul_batched_local[(6)] + (A_shared[(((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner) + 64))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 1))]));
    matmul_batched_local[(7)] = (matmul_batched_local[(7)] + (A_shared[(((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner) + 64))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 2))]));
    matmul_batched_local[(8)] = (matmul_batched_local[(8)] + (A_shared[(((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner) + 64))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 3))]));
    matmul_batched_local[(9)] = (matmul_batched_local[(9)] + (A_shared[(((((((int)get_local_id(0)) >> 2) * 128) + k_outer_inner) + 64))] * B_shared[((((k_outer_inner * 20) + ((((int)get_local_id(0)) & 3) * 5)) + 4))]));
  }
  for (int i_inner = 0; i_inner < 2; ++i_inner) {
    for (int j_inner = 0; j_inner < 5; ++j_inner) {
      matmul_batched[((((((((((int)get_group_id(0)) / 25) * 5000) + ((((int)get_local_id(0)) >> 2) * 1000)) + (i_inner * 500)) + ((((int)get_group_id(0)) % 25) * 20)) + ((((int)get_local_id(0)) & 3) * 5)) + j_inner))] = matmul_batched_local[(((i_inner * 5) + j_inner))];
    }
  }
}

