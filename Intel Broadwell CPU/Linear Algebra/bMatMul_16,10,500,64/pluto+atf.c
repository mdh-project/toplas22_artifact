#include <omp.h>
#include <math.h>
#define ceild(n,d)  (((n)<0) ? -((-(n))/(d)) : ((n)+(d)-1)/(d))
#define floord(n,d) (((n)<0) ? -((-(n)+(d)-1)/(d)) : (n)/(d))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define B_VAL 16
    #define M_VAL 10
    #define N_VAL 500
    #define K_VAL 64

    static float A[B_VAL][M_VAL][K_VAL];
    static float B[B_VAL][K_VAL][N_VAL];
    static float C[B_VAL][M_VAL][N_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < B_VAL * M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < B_VAL * K_VAL * N_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < B_VAL * M_VAL * N_VAL; ++i) ((float *)C)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
  int t1, t2, t3, t4, t5, t6, t7, t8;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
if ((B_VAL >= 1) && (K_VAL >= 1) && (M_VAL >= 1) && (N_VAL >= 1)) {
  lbp=0;
  ubp=B_VAL-1;
#pragma omp parallel for private(lbv,ubv,t2,t3,t4,t5,t6,t7,t8)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(M_VAL-1,9);t2++) {
      for (t3=0;t3<=floord(N_VAL-1,260);t3++) {
        for (t4=0;t4<=floord(K_VAL-1,40);t4++) {
          for (t6=9*t2;t6<=(min(M_VAL-1,9*t2+8))-7;t6+=8) {
            for (t7=40*t4;t7<=(min(K_VAL-1,40*t4+39))-7;t7+=8) {
              lbv=260*t3;
              ubv=min(N_VAL-1,260*t3+259);
#pragma ivdep
#pragma vector always
              for (t8=lbv;t8<=ubv;t8++) {
                C[t1][t6][t8] += A[t1][t6][t7] * B[t1][t7][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][t7] * B[t1][t7][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][t7] * B[t1][t7][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][t7] * B[t1][t7][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][t7] * B[t1][t7][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][t7] * B[t1][t7][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][t7] * B[t1][t7][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][t7] * B[t1][t7][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+1)] * B[t1][(t7+1)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+2)] * B[t1][(t7+2)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+3)] * B[t1][(t7+3)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+4)] * B[t1][(t7+4)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+5)] * B[t1][(t7+5)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+6)] * B[t1][(t7+6)][t8];;
                C[t1][t6][t8] += A[t1][t6][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][(t7+7)] * B[t1][(t7+7)][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][(t7+7)] * B[t1][(t7+7)][t8];;
              }
            }
            for (;t7<=min(K_VAL-1,40*t4+39);t7++) {
              lbv=260*t3;
              ubv=min(N_VAL-1,260*t3+259);
#pragma ivdep
#pragma vector always
              for (t8=lbv;t8<=ubv;t8++) {
                C[t1][t6][t8] += A[t1][t6][t7] * B[t1][t7][t8];;
                C[t1][(t6+1)][t8] += A[t1][(t6+1)][t7] * B[t1][t7][t8];;
                C[t1][(t6+2)][t8] += A[t1][(t6+2)][t7] * B[t1][t7][t8];;
                C[t1][(t6+3)][t8] += A[t1][(t6+3)][t7] * B[t1][t7][t8];;
                C[t1][(t6+4)][t8] += A[t1][(t6+4)][t7] * B[t1][t7][t8];;
                C[t1][(t6+5)][t8] += A[t1][(t6+5)][t7] * B[t1][t7][t8];;
                C[t1][(t6+6)][t8] += A[t1][(t6+6)][t7] * B[t1][t7][t8];;
                C[t1][(t6+7)][t8] += A[t1][(t6+7)][t7] * B[t1][t7][t8];;
              }
            }
          }
          for (;t6<=min(M_VAL-1,9*t2+8);t6++) {
            for (t7=40*t4;t7<=min(K_VAL-1,40*t4+39);t7++) {
              lbv=260*t3;
              ubv=min(N_VAL-1,260*t3+259);
#pragma ivdep
#pragma vector always
              for (t8=lbv;t8<=ubv;t8++) {
                C[t1][t6][t8] += A[t1][t6][t7] * B[t1][t7][t8];;
              }
            }
          }
        }
      }
    }
  }
}
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float C B_VAL M_VAL N_VAL
    {
        float C_gold[B_VAL][M_VAL][N_VAL];
        FILE *C_gold_file = fopen("/home/r/r_schu41/mdh-project/benchmarks/2022-toplas/Pluto/matmul_batched_row_major_nn/16x10x500x64/build_broadwell/../gold/Pluto/12e075a/matmul_batched_row_major_nn/16x10x500x64/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(C_gold_file, "%f", &(((float*)C_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*B_VAL*M_VAL*N_VAL) {
                printf("incorrect result buffer size for buffer C: expected %d, actual %d.", i, 1*B_VAL*M_VAL*N_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(C_gold_file);

        for (int i = 0; i < 1*B_VAL*M_VAL*N_VAL; ++i) {
            if (0.100000 == 0) {
                if (((float*)C)[i] != ((float*)C_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)C)[i] - ((float*)C_gold)[i]) > 0.100000) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
