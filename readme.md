# Systematic (De-)Composition of Data-Parallel Computations via Multi-Dimensional Homomorphisms

This is a preliminary version of our artifact implementation which contains the implementations introduced and discussed in the paper _Systematic (De-)Composition of Data-Parallel Computations via Multi-Dimensional Homomorphisms_ under review at [ACM Transactions on Programming Languages and Systems (TOPLAS)](https://dl.acm.org/journal/toplas).

All experiments in our paper haven been conducted on two GPUs and two CPUs:
   - NVIDIA Ampere GPU A100-PCIE-40GB 
   - NVIDIA Volta GPU V100-SXM2-16GB 
   - Intel Xeon Broadwell CPU E5-2683 v4 @ 2.10GHz
   - Intel Xeon Skylake CPU Gold-6140 @ 2.30GHz

In case of any problems, please feel free to open an issue to get in touch with the authors.

**If our paper gets accepted for publication, we will provide a fully functional artifact that contains scripts for conveniently reproducing and plotting all experiments presented in the paper (including experimental results of our competitors: TVM, PPCG, Pluto, NVIDIA cuBLAS, NVIDIA cuDNN, Intel oneMKL, Intel oneDNN, and EKR).**
