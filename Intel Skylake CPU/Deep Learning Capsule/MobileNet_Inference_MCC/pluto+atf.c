#include <omp.h>
#include <math.h>
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define N_VAL 1
    #define P_VAL 112
    #define Q_VAL 112
    #define K_VAL 32
    #define CM_VAL 4
    #define CN_VAL 4
    #define C_VAL 3
    #define R_VAL 3
    #define S_VAL 3
    #define CK_VAL 4

    static float in[N_VAL][2 * P_VAL - 1 + R_VAL - 1][2 * Q_VAL - 1 + S_VAL - 1][C_VAL][CM_VAL][CK_VAL];
    static float filter[K_VAL][R_VAL][S_VAL][C_VAL][CK_VAL][CN_VAL];
    static float out[N_VAL][P_VAL][Q_VAL][K_VAL][CM_VAL][CN_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < N_VAL * (2 * P_VAL - 1 + R_VAL - 1) * (2 * Q_VAL - 1 + S_VAL - 1) * C_VAL * CM_VAL * CK_VAL; ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * R_VAL * S_VAL * C_VAL * CK_VAL * CN_VAL; ++i) ((float *)filter)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL * P_VAL * Q_VAL * K_VAL * CM_VAL * CN_VAL; ++i) ((float *)out)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
/* Copyright (C) 1991-2012 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* We do support the IEC 559 math functionality, real and complex.  */
/* wchar_t uses ISO/IEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0.  */
/* We do not support C11 <threads.h>.  */
  int t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15, t16, t17;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
/* Start of CLooG code */
if ((CK_VAL >= 1) && (CM_VAL >= 1) && (CN_VAL >= 1) && (C_VAL >= 1) && (K_VAL >= 1) && (N_VAL >= 1) && (P_VAL >= 1) && (Q_VAL >= 1) && (R_VAL >= 1) && (S_VAL >= 1)) {
  lbp=0;
  ubp=N_VAL-1;
#pragma omp parallel for private(lbv,ubv,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,t17)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=P_VAL-1;t2++) {
      for (t3=0;t3<=floord(Q_VAL-1,67);t3++) {
        for (t4=0;t4<=floord(K_VAL-1,20);t4++) {
          for (t5=0;t5<=CM_VAL-1;t5++) {
            for (t6=0;t6<=floord(CN_VAL-1,4);t6++) {
              for (t7=0;t7<=C_VAL-1;t7++) {
                for (t10=67*t3;t10<=min(Q_VAL-1,67*t3+66);t10++) {
                  for (t11=20*t4;t11<=min(K_VAL-1,20*t4+19);t11++) {
                    for (t13=4*t6;t13<=min(CN_VAL-1,4*t6+3);t13++) {
                      for (t15=0;t15<=R_VAL-1;t15++) {
                        for (t16=0;t16<=S_VAL-1;t16++) {
                          for (t17=0;t17<=CK_VAL-1;t17++) {
                            out[t1][t2][t10][t11][t5][t13] += filter[t11][t15][t16][t7][t17][t13] * in[t1][2 * t2 + t15][2 * t10 + t16][t7][t5][t17];;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
/* End of CLooG code */
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float out N_VAL P_VAL Q_VAL K_VAL CM_VAL CN_VAL
    {
        float out_gold[N_VAL][P_VAL][Q_VAL][K_VAL][CM_VAL][CN_VAL];
        FILE *out_gold_file = fopen("/home/r/r_schu41/mdh-project/benchmarks/2022-toplas/Pluto/mcc_capsule/nhwccmck_krscckcn_npqkcmcn_stride_2/1x112x112x32x4x4x3x3x3x4/build_normal/../gold/Pluto/12e075a/mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2/1x112x112x32x4x4x3x3x3x4/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(out_gold_file, "%f", &(((float*)out_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*N_VAL*P_VAL*Q_VAL*K_VAL*CM_VAL*CN_VAL) {
                printf("incorrect result buffer size for buffer out: expected %d, actual %d.", i, 1*N_VAL*P_VAL*Q_VAL*K_VAL*CM_VAL*CN_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(out_gold_file);

        for (int i = 0; i < 1*N_VAL*P_VAL*Q_VAL*K_VAL*CM_VAL*CN_VAL; ++i) {
            if (0.100000 == 0) {
                if (((float*)out)[i] != ((float*)out_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)out_gold)[i], ((float*)out)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)out)[i] - ((float*)out_gold)[i]) > 0.100000) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)out_gold)[i], ((float*)out)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
