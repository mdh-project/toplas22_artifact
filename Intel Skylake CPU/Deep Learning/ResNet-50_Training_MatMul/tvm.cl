// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul) {
  float matmul_local[8];
  __local float A_shared[2048];
  __local float B_shared[1024];
  matmul_local[(0)] = 0.000000e+00f;
  matmul_local[(1)] = 0.000000e+00f;
  matmul_local[(2)] = 0.000000e+00f;
  matmul_local[(3)] = 0.000000e+00f;
  matmul_local[(4)] = 0.000000e+00f;
  matmul_local[(5)] = 0.000000e+00f;
  matmul_local[(6)] = 0.000000e+00f;
  matmul_local[(7)] = 0.000000e+00f;
  for (int k_outer_outer = 0; k_outer_outer < 16; ++k_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int ax0_ax1_fused_outer_outer = 0; ax0_ax1_fused_outer_outer < 32; ++ax0_ax1_fused_outer_outer) {
      vstore4(vload4(0, A + ((((((ax0_ax1_fused_outer_outer * 64) + (((int)get_local_id(0)) * 4)) >> 7) * 2048) + (k_outer_outer * 128)) + (((ax0_ax1_fused_outer_outer * 64) + (((int)get_local_id(0)) * 4)) & 127))), 0, A_shared + ((ax0_ax1_fused_outer_outer * 64) + (((int)get_local_id(0)) * 4)));
    }
    vstore4(vload4(0, B + ((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4))), 0, B_shared + (((int)get_local_id(0)) * 4));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 8000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 64));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 16000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 128));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 24000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 192));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 32000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 256));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 40000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 320));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 48000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 384));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 56000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 448));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 64000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 512));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 72000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 576));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 80000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 640));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 88000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 704));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 96000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 768));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 104000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 832));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 112000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 896));
    vstore4(vload4(0, B + (((((k_outer_outer * 128000) + ((((int)get_local_id(0)) >> 1) * 1000)) + (((int)get_group_id(0)) * 8)) + ((((int)get_local_id(0)) & 1) * 4)) + 120000)), 0, B_shared + ((((int)get_local_id(0)) * 4) + 960));
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int k_outer_inner = 0; k_outer_inner < 128; ++k_outer_inner) {
      matmul_local[(0)] = (matmul_local[(0)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[((k_outer_inner * 8))]));
      matmul_local[(1)] = (matmul_local[(1)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 1))]));
      matmul_local[(2)] = (matmul_local[(2)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 2))]));
      matmul_local[(3)] = (matmul_local[(3)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 3))]));
      matmul_local[(4)] = (matmul_local[(4)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 4))]));
      matmul_local[(5)] = (matmul_local[(5)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 5))]));
      matmul_local[(6)] = (matmul_local[(6)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 6))]));
      matmul_local[(7)] = (matmul_local[(7)] + (A_shared[(((((int)get_local_id(0)) * 128) + k_outer_inner))] * B_shared[(((k_outer_inner * 8) + 7))]));
    }
  }
  matmul[(((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)))] = matmul_local[(0)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 1))] = matmul_local[(1)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 2))] = matmul_local[(2)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 3))] = matmul_local[(3)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 4))] = matmul_local[(4)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 5))] = matmul_local[(5)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 6))] = matmul_local[(6)];
  matmul[((((((int)get_local_id(0)) * 1000) + (((int)get_group_id(0)) * 8)) + 7))] = matmul_local[(7)];
}

