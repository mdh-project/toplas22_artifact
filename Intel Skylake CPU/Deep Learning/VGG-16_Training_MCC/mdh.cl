__kernel void mcc_nhwc_krsc_npqk_stride_1_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[1 * 1]
                                   [1 * 1]
                                   [1 * 8]
                                   [1 * 1];
    __private float images_prv[1]
                                      [(1) * 1 - (1 - 1) + (1) - 1]
                                      [(8) * 1 - (1 - 1) + (3) - 1]
                                      [1];
    __private float filter_prv[1]
                                      [1]
                                      [3]
                                      [1];
    const size_t wg_1 = get_group_id(2) / (224) % (16); {
    const size_t wg_2 = get_group_id(2) % (224); {
    const size_t wg_3 = get_group_id(1); {
    const size_t wg_4 = get_group_id(0); {
    {
    {
    {
    {
    {
    {
    const size_t wi_4 = get_local_id(0); {
    {
    {
    {
    {
    {
    for (size_t glb_3 = 0; glb_3 < 7; ++glb_3) {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 8; ++prv_3) {
    {
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 8 + prv_3][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}
    }
    {
    for (size_t glb_6 = 0; glb_6 < 3; ++glb_6) {
    {
        {
        {
        {
        {
        for (size_t lcl_5 = 0; lcl_5 < 3; ++lcl_5) {
        {
        {
            {
               
               
                for (size_t prv_3 = 0; prv_3 < 8; ++prv_3)
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    images_prv[0][(0) * 1 + (0)][(prv_3) * 1 + (prv_7)][0]
                                      =
                    images_glb[(0 * 16 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((1 * 224 + 3 - 1)) * ((1 * 224 + 3 - 1)) * (3) + ((0 * 224 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * 1 + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((1 * 224 + 3 - 1)) * (3) + ((glb_3 * 4 * 1 * 1 * 8 + wg_3 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_3) * 1 + (0 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7)) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
               
               
               
                for (size_t prv_7 = 0; prv_7 < 3; ++prv_7)
                    filter_prv[0][0][prv_7][0]
                                      =
                    filter_glb[(0 * 4 * 1 * 16 * 1 + wg_4 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) * (3) * (3) * (3) + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) * (3) + (0 * 1 * 1 * 1 * 3 + 0 * 1 * 1 * 3 + 0 * 1 * 3 + 0 * 3 + prv_7) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            for (size_t prv_3 = 0; prv_3 < 8; ++prv_3) {
            {
            {
            {
            for (size_t prv_7 = 0; prv_7 < 3; ++prv_7) {
            out_prv[0 * 1 + 0][0 * 1 + 0][0 * 8 + prv_3][0 * 1 + 0]
                +=
            images_prv[0][(0) * 1 + (0)][(prv_3) * 1 + (prv_7)][0]
            *
            filter_prv[0][0][prv_7][0]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 8; ++prv_3) {
    {
        out_glb[(0 * 16 * 1 * 1 * 1 + wg_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (224) * (224) * (64) + (0 * 224 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (224) * (64) + (glb_3 * 4 * 1 * 1 * 8 + wg_3 * 1 * 1 * 8 + 0 * 1 * 8 + 0 * 8 + prv_3) * (64) + (0 * 4 * 1 * 16 * 1 + wg_4 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) ]
                                                 =
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 8 + prv_3][0 * 1 + 0]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
