__kernel void matmul_row_major_nn_static_2(__global float const * const restrict int_res_c_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict c_raw) {
    const size_t i_wg_l_1 = 0;
    const size_t i_wi_l_1 = 0;
    const size_t i_wg_l_2 = get_group_id(0);
    const size_t i_wi_l_2 = get_local_id(0);
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = 0;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_r_1;
    __global float const * const restrict int_res_c_buf = int_res_c_buf_raw;
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict c = c_raw;
  __private float res_p_c[((16 / 1) / (16)) + (((1 * (16 / 1)) % (1 * (16)) / 1) > 0) + (((1 * (16 / 1)) % (1 * (16)) % 1) > 0)][16][1][((1024 / 1024) / (1)) + (((1024 * (1024 / 1024)) % (1024 * (1)) / 1024) > 0) + (((1024 * (1024 / 1024)) % (1024 * (1)) % 1024) > 0)][1];
  l_cb_offset_l_1 = i_wg_l_1 * 1;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((16 / (1 * 1)) / (16 / 1)); ++l_step_l_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    l_cb_offset_l_2 = i_wg_l_2 * 1024;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((4096 / (4 * 1024)) / (1024 / 1024)); ++l_step_l_2) {
      barrier(CLK_LOCAL_MEM_FENCE);
      l_cb_offset_r_1 = i_wg_r_1 * 1;
      size_t l_step_r_1 = 0;
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((16 / 1) / (16)); ++p_step_l_1) {
        p_cb_offset_l_2 = i_wi_l_2 * 1;
        for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1024) / (1)); ++p_step_l_2) {
          p_cb_offset_r_1 = i_wi_r_1 * 1;
          size_t p_step_r_1 = 0;
#pragma unroll
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (16); ++p_iteration_l_1) {
#pragma unroll
            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
              size_t p_iteration_r_1 = 0;
              res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)] = int_res_c_buf[(((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))) * (16) * (4096) + (((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * 1 + i_wi_r_1))) * (4096) + (((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2)))];
#pragma unroll
              for (p_iteration_r_1 = 1; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)] += int_res_c_buf[(((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))) * (16) * (4096) + (((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * 1 + i_wi_r_1))) * (4096) + (((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2)))];
              }
            }
          }
          p_cb_offset_r_1 += 1 * (2);
          for (p_step_r_1 = 1; p_step_r_1 < ((4 / 1) / (2)); ++p_step_r_1) {
#pragma unroll
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (16); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                  res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)] += int_res_c_buf[(((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))) * (16) * (4096) + (((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * 1 + i_wi_r_1))) * (4096) + (((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2)))];
                }
              }
            }
            p_cb_offset_r_1 += 1 * (2);
          }
          p_cb_offset_l_2 += 1024 * (1);
        }
        p_cb_offset_l_1 += 1 * (16);
      }
      l_cb_offset_r_1 += 1 * (4 / 1);
      for (l_step_r_1 = 1; l_step_r_1 < ((16 / 1) / (4 / 1)); ++l_step_r_1) {
        p_cb_offset_l_1 = i_wi_l_1 * 1;
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((16 / 1) / (16)); ++p_step_l_1) {
          p_cb_offset_l_2 = i_wi_l_2 * 1;
          for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1024) / (1)); ++p_step_l_2) {
            p_cb_offset_r_1 = i_wi_r_1 * 1;
            size_t p_step_r_1 = 0;
#pragma unroll
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (16); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                  res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)] += int_res_c_buf[(((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))) * (16) * (4096) + (((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * 1 + i_wi_r_1))) * (4096) + (((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2)))];
                }
              }
            }
            p_cb_offset_r_1 += 1 * (2);
            for (p_step_r_1 = 1; p_step_r_1 < ((4 / 1) / (2)); ++p_step_r_1) {
#pragma unroll
              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (16); ++p_iteration_l_1) {
                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                  for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
                    res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)] += int_res_c_buf[(((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))) * (16) * (4096) + (((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * 1 + i_wi_r_1))) * (4096) + (((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2)))];
                  }
                }
              }
              p_cb_offset_r_1 += 1 * (2);
            }
            p_cb_offset_l_2 += 1024 * (1);
          }
          p_cb_offset_l_1 += 1 * (16);
        }
        l_cb_offset_r_1 += 1 * (4 / 1);
      }
      {
        if (i_wi_r_1 == 0)
        {
          p_cb_offset_l_1 = i_wi_l_1 * 1;
          for (size_t p_step_l_1 = 0; p_step_l_1 < ((16 / 1) / (16)); ++p_step_l_1) {
            p_cb_offset_l_2 = i_wi_l_2 * 1;
            for (size_t p_step_l_2 = 0; p_step_l_2 < ((1024 / 1024) / (1)); ++p_step_l_2) {
              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (16); ++p_iteration_l_1) {
                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                  c[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1)) * (((4096 - 1)) - (0) + 1) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 1024 + 0)) / 1024) * (4 * 1024) + i_wi_l_2))] = res_p_c[p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_2][(p_iteration_l_2)];
                }
              }
              p_cb_offset_l_2 += 1024 * (1);
            }
            p_cb_offset_l_1 += 1 * (16);
          }
        }
      }
      l_cb_offset_l_2 += (4 * 1024) * (1024 / 1024);
    }
    l_cb_offset_l_1 += (1 * 1) * (16 / 1);
  }
}
