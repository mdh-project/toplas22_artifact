__kernel void mcc_nhwc_krsc_npqk_stride_2_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[1 * 1]
                                   [1 * 1]
                                   [1 * 28]
                                   [1 * 1];
    __private float images_prv[1]
                                      [(1) * 2 - (2 - 1) + (1) - 1]
                                      [(28) * 2 - (2 - 1) + (1) - 1]
                                      [1];
    {
    const size_t wg_2 = get_group_id(2) % (112); {
    {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_4 = get_local_id(0); {
    {
    {
    {
    {
    {
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
    for (size_t glb_4 = 0; glb_4 < 2; ++glb_4) {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 28; ++prv_3) {
    {
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 28 + prv_3][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}
    }
    for (size_t glb_5 = 0; glb_5 < 3; ++glb_5) {
    for (size_t glb_6 = 0; glb_6 < 3; ++glb_6) {
    {
        {
        {
        {
        {
        {
        {
        for (size_t lcl_7 = 0; lcl_7 < 3; ++lcl_7) {
            {
               
               
                for (size_t prv_3 = 0; prv_3 < 28; ++prv_3)
               
               
               
                    images_prv[0][(0) * 2 + (0)][(prv_3) * 2 + (0)][0]
                                      =
                    images_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * ((2 * 112 - 1 + 3 - 1)) * ((2 * 112 - 1 + 3 - 1)) * (3) + ((0 * 112 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * 2 + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 - 1 + 3 - 1)) * (3) + ((glb_3 * 1 * 1 * 1 * 28 + 0 * 1 * 1 * 28 + 0 * 1 * 28 + 0 * 28 + prv_3) * 2 + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_7 * 1 * 1 + 0 * 1 + 0)) * (3) + (glb_5 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            for (size_t prv_3 = 0; prv_3 < 28; ++prv_3) {
            {
            {
            {
            {
            out_prv[0 * 1 + 0][0 * 1 + 0][0 * 28 + prv_3][0 * 1 + 0]
                +=
            images_prv[0][(0) * 2 + (0)][(prv_3) * 2 + (0)][0]
            *
            filter_glb[(glb_4 * 1 * 1 * 16 * 1 + 0 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) * (3) * (3) * (3) + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_7 * 1 * 1 + 0 * 1 + 0) * (3) + (glb_5 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 28; ++prv_3) {
    {
        out_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (112) * (32) + (0 * 112 * 1 * 1 * 1 + wg_2 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (112) * (32) + (glb_3 * 1 * 1 * 1 * 28 + 0 * 1 * 1 * 28 + 0 * 1 * 28 + 0 * 28 + prv_3) * (32) + (glb_4 * 1 * 1 * 16 * 1 + 0 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) ]
                                                 =
        out_prv[0 * 1 + 0][0 * 1 + 0][0 * 28 + prv_3][0 * 1 + 0]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
