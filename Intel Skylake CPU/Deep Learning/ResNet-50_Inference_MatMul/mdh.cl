__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 1]
                                 [2 * 4];
    __local float a_lcl[1 * 1 * 1]
                                 [64 * 1 * 2];
    __local float b_lcl[64 * 1 * 2]
                                 [2 * 1 * 4];
    {
    const size_t wg_2 = get_group_id(0); {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    for (size_t prv_2 = 0; prv_2 < 4; ++prv_2) {
        c_prv[0 * 1 + 0][lcl_2 * 4 + prv_2]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 16; ++glb_3) {
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((1 * 1 * 1 ) * (64 * 1 * 2 )) / 4) / (1 * 1 * 1); ++step)
            {
                ((__local float4*)a_lcl)[((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) / (((64 * 1 * 2 ) / 4)) % ((1 * 1 * 1 ))) * (((64 * 1 * 2 ) / 4)) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) % (((64 * 1 * 2 ) / 4))) ]
                                                                                                               =
                ((__global float4*)a_glb)[((0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 ) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) / (((64 * 1 * 2 ) / 4)) % ((1 * 1 * 1 )))) * ((2048 / 4)) + (((glb_3 * 1 * 64 * 1 * 2 + 0 * 64 * 1 * 2 ) / 4) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) % (((64 * 1 * 2 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((64 * 1 * 2 ) * (2 * 1 * 4 )) / 4) / (1 * 1 * 1); ++step)
            {
                ((__local float4*)b_lcl)[((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) / (((2 * 1 * 4 ) / 4)) % ((64 * 1 * 2 ))) * (((2 * 1 * 4 ) / 4)) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) % (((2 * 1 * 4 ) / 4))) ]
                                                                                                               =
                ((__global float4*)b_glb)[((glb_3 * 1 * 64 * 1 * 2 + 0 * 64 * 1 * 2 ) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) / (((2 * 1 * 4 ) / 4)) % ((64 * 1 * 2 )))) * ((1000 / 4)) + (((0 * 125 * 2 * 1 * 4 + wg_2 * 2 * 1 * 4 ) / 4) + ((step * (1 * 1 * 1) + (0 * 1 * 1 + 0 * 1 + 0 )) % (((2 * 1 * 4 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        {
        for (size_t lcl_3 = 0; lcl_3 < 64; ++lcl_3) {
            {
            #pragma unroll
            for (size_t prv_3 = 0; prv_3 < 2; ++prv_3) {
            #pragma unroll
            for (size_t prv_2 = 0; prv_2 < 4; ++prv_2) {
            #pragma unroll
            for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
            c_prv[0 * 1 + 0][lcl_2 * 4 + prv_2]
                +=
            a_lcl[0 * 1 * 1 + 0 * 1 + 0][lcl_3 * 1 * 2 + 0 * 2 + prv_3]
            *
            b_lcl[lcl_3 * 1 * 2 + 0 * 2 + prv_3][lcl_2 * 1 * 4 + 0 * 4 + prv_2]
            ;
            }}}
        }}}
    }
    {
    {
    for (size_t lcl_2 = 0; lcl_2 < 2; ++lcl_2) {
    {
    for (size_t prv_2 = 0; prv_2 < 4; ++prv_2) {
        c_glb[(0 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (1000) + (0 * 125 * 2 * 1 * 4 + wg_2 * 2 * 1 * 4 + lcl_2 * 1 * 4 + 0 * 4 + prv_2) ]
                                               =
        c_prv[0 * 1 + 0][lcl_2 * 4 + prv_2]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
