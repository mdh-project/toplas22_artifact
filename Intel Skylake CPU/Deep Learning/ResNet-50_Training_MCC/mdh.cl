__kernel void mcc_nhwc_krsc_npqk_stride_2_asymmetrical_static_1(
        __global float const * const __restrict__ images_glb,
        __global float const * const __restrict__ filter_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ out_glb) {
    __private float out_prv[1 * 2]
                                   [1 * 14]
                                   [1 * 1]
                                   [1 * 1];
    __private float images_prv[2]
                                      [(14) * 2 - (2 - 1) + (1) - 1]
                                      [(1) * 2 - (2 - 1) + (1) - 1]
                                      [1];
    {
    const size_t wg_2 = get_group_id(2) % (2); {
    const size_t wg_3 = get_group_id(1); {
    const size_t wg_4 = get_group_id(0); {
    {
    {
    {
    {
    const size_t wi_2 = get_local_id(2) % (2); {
    {
    const size_t wi_4 = get_local_id(0); {
    {
    {
    {
    for (size_t glb_1 = 0; glb_1 < 8; ++glb_1) {
    for (size_t glb_2 = 0; glb_2 < 2; ++glb_2) {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
    for (size_t prv_2 = 0; prv_2 < 14; ++prv_2) {
    {
    {
        out_prv[0 * 2 + prv_1][0 * 14 + prv_2][0 * 1 + 0][0 * 1 + 0]
            = 0.0f;
    }}}}}}}}
    }
    {
    for (size_t glb_6 = 0; glb_6 < 7; ++glb_6) {
    for (size_t glb_7 = 0; glb_7 < 7; ++glb_7) {
        {
        {
        {
        {
        for (size_t lcl_5 = 0; lcl_5 < 3; ++lcl_5) {
        {
        {
            {
                for (size_t prv_1 = 0; prv_1 < 2; ++prv_1)
                for (size_t prv_2 = 0; prv_2 < 14; ++prv_2)
               
               
               
               
                    images_prv[prv_1][(prv_2) * 2 + (0)][(0) * 2 + (0)][0]
                                      =
                    images_glb[(glb_1 * 1 * 1 * 1 * 2 + 0 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_1) * ((2 * 112 + 7 - 1)) * ((2 * 112 + 7 - 1)) * (3) + ((glb_2 * 2 * 1 * 2 * 14 + wg_2 * 1 * 2 * 14 + 0 * 2 * 14 + wi_2 * 14 + prv_2) * 2 + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * ((2 * 112 + 7 - 1)) * (3) + ((0 * 112 * 1 * 1 * 1 + wg_3 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * 2 + (glb_7 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0)) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
            for (size_t prv_2 = 0; prv_2 < 14; ++prv_2) {
            {
            {
            {
            {
            {
            out_prv[0 * 2 + prv_1][0 * 14 + prv_2][0 * 1 + 0][0 * 1 + 0]
                +=
            images_prv[prv_1][(prv_2) * 2 + (0)][(0) * 2 + (0)][0]
            *
            filter_glb[(0 * 4 * 1 * 16 * 1 + wg_4 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) * (7) * (7) * (3) + (glb_6 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (7) * (3) + (glb_7 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (3) + (0 * 1 * 3 * 1 * 1 + 0 * 3 * 1 * 1 + lcl_5 * 1 * 1 + 0 * 1 + 0) ]
            ;
            }}}}}}}
        }}}}}}}
    }}}
    {
    {
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
    for (size_t prv_2 = 0; prv_2 < 14; ++prv_2) {
    {
    {
        out_glb[(glb_1 * 1 * 1 * 1 * 2 + 0 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_1) * (112) * (112) * (64) + (glb_2 * 2 * 1 * 2 * 14 + wg_2 * 1 * 2 * 14 + 0 * 2 * 14 + wi_2 * 14 + prv_2) * (112) * (64) + (0 * 112 * 1 * 1 * 1 + wg_3 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (64) + (0 * 4 * 1 * 16 * 1 + wg_4 * 1 * 16 * 1 + 0 * 16 * 1 + wi_4 * 1 + 0) ]
                                                 =
        out_prv[0 * 2 + prv_1][0 * 14 + prv_2][0 * 1 + 0][0 * 1 + 0]
                             ;
    }}}}}}}}
    }
    }}}}
    }}}}}}}
    }}}}}}}
}
