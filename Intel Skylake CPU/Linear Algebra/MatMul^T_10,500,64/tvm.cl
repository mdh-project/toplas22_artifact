// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict A, __global float* restrict B, __global float* restrict matmul_col_major) {
  float matmul_col_major_local[20];
  __local float A_shared[640];
  __local float B_shared[1280];
  for (int j_c_inner_init = 0; j_c_inner_init < 4; ++j_c_inner_init) {
    matmul_col_major_local[(j_c_inner_init)] = 0.000000e+00f;
    matmul_col_major_local[((j_c_inner_init + 4))] = 0.000000e+00f;
    matmul_col_major_local[((j_c_inner_init + 8))] = 0.000000e+00f;
    matmul_col_major_local[((j_c_inner_init + 12))] = 0.000000e+00f;
    matmul_col_major_local[((j_c_inner_init + 16))] = 0.000000e+00f;
  }
  for (int ax0_ax1_fused_outer_outer = 0; ax0_ax1_fused_outer_outer < 16; ++ax0_ax1_fused_outer_outer) {
    vstore4(vload4(0, A + ((ax0_ax1_fused_outer_outer * 40) + (((int)get_local_id(0)) * 4))), 0, A_shared + ((ax0_ax1_fused_outer_outer * 40) + (((int)get_local_id(0)) * 4)));
  }
  for (int ax0_ax1_fused_outer_outer1 = 0; ax0_ax1_fused_outer_outer1 < 32; ++ax0_ax1_fused_outer_outer1) {
    vstore4(vload4(0, B + (((((int)get_group_id(0)) * 1280) + (ax0_ax1_fused_outer_outer1 * 40)) + (((int)get_local_id(0)) * 4))), 0, B_shared + ((ax0_ax1_fused_outer_outer1 * 40) + (((int)get_local_id(0)) * 4)));
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int k_outer_inner = 0; k_outer_inner < 16; ++k_outer_inner) {
    for (int k_inner = 0; k_inner < 4; ++k_inner) {
      for (int j_c_inner = 0; j_c_inner < 4; ++j_c_inner) {
        matmul_col_major_local[(j_c_inner)] = (matmul_col_major_local[(j_c_inner)] + (A_shared[((((k_outer_inner * 40) + (k_inner * 10)) + (((int)get_local_id(0)) & 1)))] * B_shared[((((((((int)get_local_id(0)) >> 1) * 256) + (j_c_inner * 64)) + (k_outer_inner * 4)) + k_inner))]));
        matmul_col_major_local[((j_c_inner + 4))] = (matmul_col_major_local[((j_c_inner + 4))] + (A_shared[(((((k_outer_inner * 40) + (k_inner * 10)) + (((int)get_local_id(0)) & 1)) + 2))] * B_shared[((((((((int)get_local_id(0)) >> 1) * 256) + (j_c_inner * 64)) + (k_outer_inner * 4)) + k_inner))]));
        matmul_col_major_local[((j_c_inner + 8))] = (matmul_col_major_local[((j_c_inner + 8))] + (A_shared[(((((k_outer_inner * 40) + (k_inner * 10)) + (((int)get_local_id(0)) & 1)) + 4))] * B_shared[((((((((int)get_local_id(0)) >> 1) * 256) + (j_c_inner * 64)) + (k_outer_inner * 4)) + k_inner))]));
        matmul_col_major_local[((j_c_inner + 12))] = (matmul_col_major_local[((j_c_inner + 12))] + (A_shared[(((((k_outer_inner * 40) + (k_inner * 10)) + (((int)get_local_id(0)) & 1)) + 6))] * B_shared[((((((((int)get_local_id(0)) >> 1) * 256) + (j_c_inner * 64)) + (k_outer_inner * 4)) + k_inner))]));
        matmul_col_major_local[((j_c_inner + 16))] = (matmul_col_major_local[((j_c_inner + 16))] + (A_shared[(((((k_outer_inner * 40) + (k_inner * 10)) + (((int)get_local_id(0)) & 1)) + 8))] * B_shared[((((((((int)get_local_id(0)) >> 1) * 256) + (j_c_inner * 64)) + (k_outer_inner * 4)) + k_inner))]));
      }
    }
  }
  for (int j_inner = 0; j_inner < 4; ++j_inner) {
    matmul_col_major[(((((((int)get_group_id(0)) * 200) + ((((int)get_local_id(0)) >> 1) * 40)) + (j_inner * 10)) + (((int)get_local_id(0)) & 1)))] = matmul_col_major_local[(j_inner)];
    matmul_col_major[((((((((int)get_group_id(0)) * 200) + ((((int)get_local_id(0)) >> 1) * 40)) + (j_inner * 10)) + (((int)get_local_id(0)) & 1)) + 2))] = matmul_col_major_local[((j_inner + 4))];
    matmul_col_major[((((((((int)get_group_id(0)) * 200) + ((((int)get_local_id(0)) >> 1) * 40)) + (j_inner * 10)) + (((int)get_local_id(0)) & 1)) + 4))] = matmul_col_major_local[((j_inner + 8))];
    matmul_col_major[((((((((int)get_group_id(0)) * 200) + ((((int)get_local_id(0)) >> 1) * 40)) + (j_inner * 10)) + (((int)get_local_id(0)) & 1)) + 6))] = matmul_col_major_local[((j_inner + 12))];
    matmul_col_major[((((((((int)get_group_id(0)) * 200) + ((((int)get_local_id(0)) >> 1) * 40)) + (j_inner * 10)) + (((int)get_local_id(0)) & 1)) + 8))] = matmul_col_major_local[((j_inner + 16))];
  }
}

