__kernel void matmul_col_major_nn_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_l_1 = 0;
    const size_t i_wi_l_1 = 0;
    const size_t i_wg_l_2 = get_group_id(2);
    const size_t i_wi_l_2 = get_local_id(2);
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = 0;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __private float cb_p_a[(10)][(1)];
  __private float cb_p_b[(1)][(1)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __private float res_p_c[((10 / 10) / (1)) + (((10 * (10 / 10)) % (10 * (1)) / 10) > 0) + (((10 * (10 / 10)) % (10 * (1)) % 10) > 0)][1][((10 / 1) / (10)) + (((1 * (10 / 1)) % (1 * (10)) / 1) > 0) + (((1 * (10 / 1)) % (1 * (10)) % 1) > 0)][10][1];
  l_cb_offset_l_1 = i_wg_l_1 * 1;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((10 / (1 * 1)) / (10 / 1)); ++l_step_l_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    l_cb_offset_l_2 = i_wg_l_2 * 10;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((500 / (50 * 10)) / (10 / 10)); ++l_step_l_2) {
      barrier(CLK_LOCAL_MEM_FENCE);
      l_cb_offset_r_1 = i_wg_r_1 * 1;
      size_t l_step_r_1 = 0;
      {
        size_t p_step_l_1 = 0;
        size_t p_step_r_1 = 0;
        {
          for (size_t step = 0; step < ((1)) * ((10)) / (1); ++step) {
            const size_t flat_index = (0) + step * (1);
            const size_t p_dim_0_index_r_1 = flat_index / (((10)));
            const size_t p_dim_1_index_l_1 = flat_index % ((10));
            cb_p_a[((p_dim_1_index_l_1))][((p_dim_0_index_r_1))] = a_buf[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 1))) * (10) + (((l_step_l_1 * (10 / 1) + (((p_step_l_1 * (10) + (p_dim_1_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_1_index_l_1) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (10) + (p_dim_1_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_1_index_l_1) % 1))) % 1)))];
          }
            }
      }
      {
        size_t p_step_l_2 = 0;
        size_t p_step_r_1 = 0;
        {
          for (size_t step = 0; step < ((1)) * ((1)) / (1); ++step) {
            const size_t flat_index = (0) + step * (1);
            const size_t p_dim_0_index_l_2 = flat_index / (((1)));
            const size_t p_dim_1_index_r_1 = flat_index % ((1));
            cb_p_b[((p_dim_0_index_l_2))][((p_dim_1_index_r_1))] = b_buf[(((l_step_l_2 * (10 / 10) + (((p_step_l_2 * (1) + (p_dim_0_index_l_2) / 1) * 10 + i_wi_l_2 * 1 + ((p_dim_0_index_l_2) % 1))) / 10) * (50 * 10) + i_wg_l_2 * 10 + ((((p_step_l_2 * (1) + (p_dim_0_index_l_2) / 1) * 10 + i_wi_l_2 * 1 + ((p_dim_0_index_l_2) % 1))) % 10))) * (64) + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) % 1)))];
          }
            }
      }
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((10 / 1) / (10)); ++p_step_l_1) {
        p_cb_offset_l_2 = i_wi_l_2 * 1;
        for (size_t p_step_l_2 = 0; p_step_l_2 < ((10 / 10) / (1)); ++p_step_l_2) {
          p_cb_offset_r_1 = i_wi_r_1 * 1;
          size_t p_step_r_1 = 0;
#pragma unroll
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (10); ++p_iteration_l_1) {
#pragma unroll
            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
              size_t p_iteration_r_1 = 0;
              res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_1][(p_iteration_l_1)][(0)] = cb_p_a[(((p_iteration_l_1)))][(((p_iteration_r_1)))] * cb_p_b[(((p_iteration_l_2)))][(((p_iteration_r_1)))];
            }
          }
          p_cb_offset_r_1 += 1 * (1);
          p_cb_offset_l_2 += 10 * (1);
        }
        p_cb_offset_l_1 += 1 * (10);
      }
      l_cb_offset_r_1 += (1 * 1) * (1 / 1);
      for (l_step_r_1 = 1; l_step_r_1 < ((64 / (1 * 1)) / (1 / 1)); ++l_step_r_1) {
        {
          size_t p_step_l_1 = 0;
          size_t p_step_r_1 = 0;
          {
            for (size_t step = 0; step < ((1)) * ((10)) / (1); ++step) {
              const size_t flat_index = (0) + step * (1);
              const size_t p_dim_0_index_r_1 = flat_index / (((10)));
              const size_t p_dim_1_index_l_1 = flat_index % ((10));
              cb_p_a[((p_dim_1_index_l_1))][((p_dim_0_index_r_1))] = a_buf[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 1))) * (10) + (((l_step_l_1 * (10 / 1) + (((p_step_l_1 * (10) + (p_dim_1_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_1_index_l_1) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (10) + (p_dim_1_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_1_index_l_1) % 1))) % 1)))];
            }
              }
        }
        {
          size_t p_step_l_2 = 0;
          size_t p_step_r_1 = 0;
          {
            for (size_t step = 0; step < ((1)) * ((1)) / (1); ++step) {
              const size_t flat_index = (0) + step * (1);
              const size_t p_dim_0_index_l_2 = flat_index / (((1)));
              const size_t p_dim_1_index_r_1 = flat_index % ((1));
              cb_p_b[((p_dim_0_index_l_2))][((p_dim_1_index_r_1))] = b_buf[(((l_step_l_2 * (10 / 10) + (((p_step_l_2 * (1) + (p_dim_0_index_l_2) / 1) * 10 + i_wi_l_2 * 1 + ((p_dim_0_index_l_2) % 1))) / 10) * (50 * 10) + i_wg_l_2 * 10 + ((((p_step_l_2 * (1) + (p_dim_0_index_l_2) / 1) * 10 + i_wi_l_2 * 1 + ((p_dim_0_index_l_2) % 1))) % 10))) * (64) + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) % 1)))];
            }
              }
        }
        p_cb_offset_l_1 = i_wi_l_1 * 1;
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((10 / 1) / (10)); ++p_step_l_1) {
          p_cb_offset_l_2 = i_wi_l_2 * 1;
          for (size_t p_step_l_2 = 0; p_step_l_2 < ((10 / 10) / (1)); ++p_step_l_2) {
            p_cb_offset_r_1 = i_wi_r_1 * 1;
            size_t p_step_r_1 = 0;
#pragma unroll
            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (10); ++p_iteration_l_1) {
              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                  res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_1][(p_iteration_l_1)][(0)] += cb_p_a[(((p_iteration_l_1)))][(((p_iteration_r_1)))] * cb_p_b[(((p_iteration_l_2)))][(((p_iteration_r_1)))];
                }
              }
            }
            p_cb_offset_r_1 += 1 * (1);
            p_cb_offset_l_2 += 10 * (1);
          }
          p_cb_offset_l_1 += 1 * (10);
        }
        l_cb_offset_r_1 += (1 * 1) * (1 / 1);
      }
      {
        if (i_wi_r_1 == 0)
        {
          p_cb_offset_l_1 = i_wi_l_1 * 1;
          for (size_t p_step_l_1 = 0; p_step_l_1 < ((10 / 1) / (10)); ++p_step_l_1) {
            p_cb_offset_l_2 = i_wi_l_2 * 1;
            for (size_t p_step_l_2 = 0; p_step_l_2 < ((10 / 10) / (1)); ++p_step_l_2) {
              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (10); ++p_iteration_l_1) {
                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                  int_res_c[((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 10 + 0)) / 10) * (50 * 10) + i_wi_l_2)) * (((10 - 1)) - (0) + 1) + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_1))] = res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_1][(p_iteration_l_1)][(0)];
                }
              }
              p_cb_offset_l_2 += 10 * (1);
            }
            p_cb_offset_l_1 += 1 * (10);
          }
        }
      }
      l_cb_offset_l_2 += (50 * 10) * (10 / 10);
    }
    l_cb_offset_l_1 += (1 * 1) * (10 / 1);
  }
}
