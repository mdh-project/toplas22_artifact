__kernel void matvec_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[2 * 4];
    __local float a_lcl[2 * 1 * 4]
                                 [8 * 1 * 64];
    __local float b_lcl[8 * 1 * 64];
    const size_t wg_1 = get_group_id(0); {
    {
    {
    {
    {
    {
    for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
    for (size_t prv_1 = 0; prv_1 < 4; ++prv_1) {
        c_prv[lcl_1 * 4 + prv_1]
            = 0.0f;
    }}
    }
    for (size_t glb_2 = 0; glb_2 < 16; ++glb_2) {
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((2 * 1 * 4 ) * (8 * 1 * 64 )) / 4) / (1 * 1); ++step)
            {
                ((__local float4*)a_lcl)[((step * (1 * 1) + (0 * 1 + 0 )) / (((8 * 1 * 64 ) / 4)) % ((2 * 1 * 4 ))) * (((8 * 1 * 64 ) / 4)) + ((step * (1 * 1) + (0 * 1 + 0 )) % (((8 * 1 * 64 ) / 4))) ]
                                                                                                               =
                ((__global float4*)a_glb)[((0 * 1024 * 2 * 1 * 4 + wg_1 * 2 * 1 * 4 ) + ((step * (1 * 1) + (0 * 1 + 0 )) / (((8 * 1 * 64 ) / 4)) % ((2 * 1 * 4 )))) * ((8192 / 4)) + (((glb_2 * 1 * 8 * 1 * 64 + 0 * 8 * 1 * 64 ) / 4) + ((step * (1 * 1) + (0 * 1 + 0 )) % (((8 * 1 * 64 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((8 * 1 * 64 )) / 4) / (1 * 1); ++step)
            {
                ((__local float4*)b_lcl)[(step * (1 * 1) + (0 * 1 + 0 )) % (((8 * 1 * 64 ) / 4))]
                                                                                                               =
                ((__global float4*)b_glb)[((glb_2 * 1 * 8 * 1 * 64 + 0 * 8 * 1 * 64 ) / 4) + ((step * (1 * 1) + (0 * 1 + 0 )) % (((8 * 1 * 64 ) / 4)))]
                                                                                                                                                            ;
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
        for (size_t lcl_2 = 0; lcl_2 < 8; ++lcl_2) {
            for (size_t prv_1 = 0; prv_1 < 4; ++prv_1) {
            for (size_t prv_2 = 0; prv_2 < 64; ++prv_2) {
            c_prv[lcl_1 * 4 + prv_1]
                +=
            a_lcl[lcl_1 * 1 * 4 + 0 * 4 + prv_1][lcl_2 * 1 * 64 + 0 * 64 + prv_2]
            *
            b_lcl[lcl_2 * 1 * 64 + 0 * 64 + prv_2]
            ;
            }}
        }}
    }
    {
    for (size_t lcl_1 = 0; lcl_1 < 2; ++lcl_1) {
    for (size_t prv_1 = 0; prv_1 < 4; ++prv_1) {
        c_glb[0 * 1024 * 2 * 1 * 4 + wg_1 * 2 * 1 * 4 + lcl_1 * 1 * 4 + 0 * 4 + prv_1] =
        c_prv[lcl_1 * 4 + prv_1];
    }}
    }
    }
    }}
    }}
}
