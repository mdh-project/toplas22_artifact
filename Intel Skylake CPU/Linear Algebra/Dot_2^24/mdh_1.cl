__kernel void dot_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_r_1 = get_group_id(0);
    const size_t i_wi_r_1 = get_local_id(0);
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __private float cb_p_b[(1)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __private float res_p_c[1];
  __local float c_l_reduction_mem[512];
  l_cb_offset_r_1 = i_wg_r_1 * 512;
  size_t l_step_r_1 = 0;
  p_cb_offset_r_1 = i_wi_r_1 * 1;
  size_t p_step_r_1 = 0;
  {
    {
      for (size_t step = 0; step < ((1)) / (1); ++step) {
        const size_t flat_index = (0) + step * (1);
        const size_t p_dim_0_index_r_1 = flat_index;
        cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (1024 / 512) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 512) * (2048 * 512) + i_wg_r_1 * 512 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 512)))];
      }
        }
  }
  size_t p_iteration_r_1 = 0;
  res_p_c[(0)] = a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 512 + 0)) / 512) * (2048 * 512) + i_wi_r_1))] * cb_p_b[(((p_iteration_r_1)))];
  p_cb_offset_r_1 += 512 * (1);
  for (p_step_r_1 = 1; p_step_r_1 < ((1024 / 512) / (1)); ++p_step_r_1) {
    {
      {
        for (size_t step = 0; step < ((1)) / (1); ++step) {
          const size_t flat_index = (0) + step * (1);
          const size_t p_dim_0_index_r_1 = flat_index;
          cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (1024 / 512) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 512) * (2048 * 512) + i_wg_r_1 * 512 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 512)))];
        }
          }
    }
#pragma unroll
    for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
      res_p_c[(0)] += a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 512 + 0)) / 512) * (2048 * 512) + i_wi_r_1))] * cb_p_b[(((p_iteration_r_1)))];
    }
    p_cb_offset_r_1 += 512 * (1);
  }
  {
    {
        {
          c_l_reduction_mem[i_wi_r_1] = res_p_c[(0)];
          }
        res_p_c[(0)] = 0;
      }
    barrier(CLK_LOCAL_MEM_FENCE);
    {
      size_t stride = (512) / 2;
      for (; stride > 0; stride /= 2) {
          if (i_wi_r_1 < stride) {
            c_l_reduction_mem[i_wi_r_1] += c_l_reduction_mem[((i_wi_r_1) + stride)];
          }
          else if (i_wi_r_1 >= 2 * stride) {
          }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
      if (i_wi_r_1 == 1) {
      }
      barrier(CLK_LOCAL_MEM_FENCE);
    }
    if (i_wi_r_1 == 0) {
      res_p_c[(0)] = c_l_reduction_mem[i_wi_r_1];
    }
  }
  l_cb_offset_r_1 += (2048 * 512) * (1024 / 512);
  for (l_step_r_1 = 1; l_step_r_1 < ((16777216 / (2048 * 512)) / (1024 / 512)); ++l_step_r_1) {
    p_cb_offset_r_1 = i_wi_r_1 * 1;
    size_t p_step_r_1 = 0;
    {
      {
        for (size_t step = 0; step < ((1)) / (1); ++step) {
          const size_t flat_index = (0) + step * (1);
          const size_t p_dim_0_index_r_1 = flat_index;
          cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (1024 / 512) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 512) * (2048 * 512) + i_wg_r_1 * 512 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 512)))];
        }
          }
    }
#pragma unroll
    for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
      res_p_c[(0)] += a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 512 + 0)) / 512) * (2048 * 512) + i_wi_r_1))] * cb_p_b[(((p_iteration_r_1)))];
    }
    p_cb_offset_r_1 += 512 * (1);
    for (p_step_r_1 = 1; p_step_r_1 < ((1024 / 512) / (1)); ++p_step_r_1) {
      {
        {
          for (size_t step = 0; step < ((1)) / (1); ++step) {
            const size_t flat_index = (0) + step * (1);
            const size_t p_dim_0_index_r_1 = flat_index;
            cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (1024 / 512) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 512) * (2048 * 512) + i_wg_r_1 * 512 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 512 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 512)))];
          }
            }
      }
#pragma unroll
      for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
        res_p_c[(0)] += a_buf[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 512 + 0)) / 512) * (2048 * 512) + i_wi_r_1))] * cb_p_b[(((p_iteration_r_1)))];
      }
      p_cb_offset_r_1 += 512 * (1);
    }
    {
      {
          {
            c_l_reduction_mem[i_wi_r_1] = res_p_c[(0)];
            }
          res_p_c[(0)] = 0;
        }
      barrier(CLK_LOCAL_MEM_FENCE);
      {
        size_t stride = (512) / 2;
        for (; stride > 0; stride /= 2) {
            if (i_wi_r_1 < stride) {
              c_l_reduction_mem[i_wi_r_1] += c_l_reduction_mem[((i_wi_r_1) + stride)];
            }
            else if (i_wi_r_1 >= 2 * stride) {
            }
          barrier(CLK_LOCAL_MEM_FENCE);
        }
        if (i_wi_r_1 == 1) {
        }
        barrier(CLK_LOCAL_MEM_FENCE);
      }
      if (i_wi_r_1 == 0) {
        res_p_c[(0)] += c_l_reduction_mem[i_wi_r_1];
      }
    }
    l_cb_offset_r_1 += (2048 * 512) * (1024 / 512);
  }
  {
    if (i_wi_r_1 == 0)
    {
      int_res_c[i_wg_r_1] = res_p_c[(0)];
    }
  }
}
