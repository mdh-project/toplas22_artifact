__kernel void matvec_row_major_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_l_1 = get_group_id(1);
    const size_t i_wi_l_1 = 0;
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = get_local_id(0);
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __private float cb_p_a[(1)][(1)];
  __private float cb_p_b[(1)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __private float res_p_c[((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][1];
  l_cb_offset_l_1 = i_wg_l_1 * 1;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((4096 / (2048 * 1)) / (1 / 1)); ++l_step_l_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    l_cb_offset_r_1 = i_wg_r_1 * 16;
    size_t l_step_r_1 = 0;
    {
      size_t p_step_l_1 = 0;
      size_t p_step_r_1 = 0;
      {
        for (size_t step = 0; step < ((1)) * ((1)) / (1); ++step) {
          const size_t flat_index = (0) + step * (1);
          const size_t p_dim_0_index_l_1 = flat_index / (((1)));
          const size_t p_dim_1_index_r_1 = flat_index % ((1));
          cb_p_a[((p_dim_0_index_l_1))][((p_dim_1_index_r_1))] = a_buf[(((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (p_dim_0_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1) % 1))) / 1) * (2048 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (p_dim_0_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1) % 1))) % 1))) * (4096) + (((l_step_r_1 * (16 / 16) + (((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) / 16) * (1 * 16) + i_wg_r_1 * 16 + ((((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) % 16)))];
        }
          }
    }
    {
      size_t p_step_r_1 = 0;
      {
        for (size_t step = 0; step < ((1)) / (1); ++step) {
          const size_t flat_index = (0) + step * (1);
          const size_t p_dim_0_index_r_1 = flat_index;
          cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (16 / 16) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 16) * (1 * 16) + i_wg_r_1 * 16 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 16)))];
        }
          }
    }
    p_cb_offset_l_1 = i_wi_l_1 * 1;
    for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
      p_cb_offset_r_1 = i_wi_r_1 * 1;
      size_t p_step_r_1 = 0;
#pragma unroll
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        res_p_c[p_step_l_1][(p_iteration_l_1)][(0)] = cb_p_a[(((p_iteration_l_1)))][(((p_iteration_r_1)))] * cb_p_b[(((p_iteration_r_1)))];
      }
      p_cb_offset_r_1 += 16 * (1);
      p_cb_offset_l_1 += 1 * (1);
    }
    l_cb_offset_r_1 += (1 * 16) * (16 / 16);
    for (l_step_r_1 = 1; l_step_r_1 < ((4096 / (1 * 16)) / (16 / 16)); ++l_step_r_1) {
      {
        size_t p_step_l_1 = 0;
        size_t p_step_r_1 = 0;
        {
          for (size_t step = 0; step < ((1)) * ((1)) / (1); ++step) {
            const size_t flat_index = (0) + step * (1);
            const size_t p_dim_0_index_l_1 = flat_index / (((1)));
            const size_t p_dim_1_index_r_1 = flat_index % ((1));
            cb_p_a[((p_dim_0_index_l_1))][((p_dim_1_index_r_1))] = a_buf[(((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (p_dim_0_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1) % 1))) / 1) * (2048 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (p_dim_0_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_dim_0_index_l_1) % 1))) % 1))) * (4096) + (((l_step_r_1 * (16 / 16) + (((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) / 16) * (1 * 16) + i_wg_r_1 * 16 + ((((p_step_r_1 * (1) + (p_dim_1_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_1_index_r_1) % 1))) % 16)))];
          }
            }
      }
      {
        size_t p_step_r_1 = 0;
        {
          for (size_t step = 0; step < ((1)) / (1); ++step) {
            const size_t flat_index = (0) + step * (1);
            const size_t p_dim_0_index_r_1 = flat_index;
            cb_p_b[((p_dim_0_index_r_1))] = b_buf[(((l_step_r_1 * (16 / 16) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 16) * (1 * 16) + i_wg_r_1 * 16 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 16 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 16)))];
          }
            }
      }
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
        p_cb_offset_r_1 = i_wi_r_1 * 1;
        size_t p_step_r_1 = 0;
#pragma unroll
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
            res_p_c[p_step_l_1][(p_iteration_l_1)][(0)] += cb_p_a[(((p_iteration_l_1)))][(((p_iteration_r_1)))] * cb_p_b[(((p_iteration_r_1)))];
          }
        }
        p_cb_offset_r_1 += 16 * (1);
        p_cb_offset_l_1 += 1 * (1);
      }
      l_cb_offset_r_1 += (1 * 16) * (16 / 16);
    }
    {
      {
        p_cb_offset_l_1 = i_wi_l_1 * 1;
        for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
            res_g_c[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (2048 * 1) + i_wi_l_1)) * ((1 * 16)) + ((((i_wg_r_1)) * 16 + ((i_wi_r_1))))] = res_p_c[p_step_l_1][(p_iteration_l_1)][(0)];
          }
          p_cb_offset_l_1 += 1 * (1);
        }
      }
    }
    l_cb_offset_l_1 += (2048 * 1) * (1 / 1);
  }
  {
    barrier(CLK_GLOBAL_MEM_FENCE);
    l_cb_offset_l_1 = i_wg_l_1 * 1;
    for (size_t l_step_l_1 = 0; l_step_l_1 < ((4096 / (2048 * 1)) / (1 / 1)); ++l_step_l_1) {
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
          {
            size_t stride = (16) / 2;
            for (; stride > 0; stride /= 2) {
                if (i_wi_r_1 < stride) {
                  res_g_c[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (2048 * 1) + i_wi_l_1)) * ((1 * 16)) + ((((i_wg_r_1)) * 16 + ((i_wi_r_1))))] += res_g_c[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 1 + 0)) / 1) * (2048 * 1) + i_wi_l_1)) * ((1 * 16)) + ((((i_wg_r_1)) * 16 + ((i_wi_r_1 + stride))))];
                }
              barrier(CLK_GLOBAL_MEM_FENCE);
            }
            barrier(CLK_GLOBAL_MEM_FENCE);
          }
        }
        p_cb_offset_l_1 += 1 * (1);
      }
      l_cb_offset_l_1 += (2048 * 1) * (1 / 1);
    }
  }
  {
    l_cb_offset_l_1 = i_wg_l_1 * 1;
    for (size_t l_step_l_1 = 0; l_step_l_1 < ((4096 / (2048 * 1)) / (1 / 1)); ++l_step_l_1) {
      if (((get_local_id(1)) * (16) + (get_local_id(0))) < ((((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)))) % (1 * 16)) {
        const size_t flat_index = ((get_local_id(1)) * (16) + (get_local_id(0))) + (((((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)))) / (1 * 16)) * (1 * 16);
        const size_t l_index_l_1 = flat_index;
        int_res_c[(((l_step_l_1 * (1 / 1) + (l_index_l_1) / 1) * (2048 * 1) + i_wg_l_1 * 1 + ((l_index_l_1) % 1)))]
       =
        res_g_c[(((l_step_l_1 * (1 / 1) + (l_index_l_1) / 1) * (2048 * 1) + i_wg_l_1 * 1 + ((l_index_l_1) % 1))) * ((1 * 16)) + ((((i_wg_r_1)) * 16 + ((0))))];
      }
      l_cb_offset_l_1 += (2048 * 1) * (1 / 1);
    }
    barrier(CLK_GLOBAL_MEM_FENCE);
  }
}
