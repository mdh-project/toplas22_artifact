#include <omp.h>
#include <math.h>
#define ceild(n,d)  ceil(((double)(n))/((double)(d)))
#define floord(n,d) floor(((double)(n))/((double)(d)))
#define max(x,y)    ((x) > (y)? (x) : (y))
#define min(x,y)    ((x) < (y)? (x) : (y))

#include <limits.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, const char **argv) {
    int evaluations = 1;
    if (argc >= 2)
        evaluations = atoi(argv[1]);
    #define M_VAL 4096
    #define K_VAL 4096

    static float A[M_VAL][K_VAL];
    static float B[K_VAL];
    static float C[M_VAL];

    // evaluation loop start
    long min_runtime = LONG_MAX;
    long runtime;
    struct timespec start, stop;
    for (int i = 0; i < evaluations; ++i) {

    for (int i = 0; i < M_VAL * K_VAL; ++i) ((float *)A)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL; ++i) ((float *)B)[i] = (i % 10) + 1;
    for (int i = 0; i < M_VAL; ++i) ((float *)C)[i] = 0;

    // kernel start
    if(clock_gettime(CLOCK_REALTIME, &start) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
/* Copyright (C) 1991-2012 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */
/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */
/* We do support the IEC 559 math functionality, real and complex.  */
/* wchar_t uses ISO/IEC 10646 (2nd ed., published 2011-03-15) /
   Unicode 6.0.  */
/* We do not support C11 <threads.h>.  */
  int t1, t2, t3, t4;
 int lb, ub, lbp, ubp, lb2, ub2;
 register int lbv, ubv;
/* Start of CLooG code */
if ((K_VAL >= 1) && (M_VAL >= 1)) {
  lbp=0;
  ubp=floord(M_VAL-1,108);
#pragma omp parallel for private(lbv,ubv,t2,t3,t4)
  for (t1=lbp;t1<=ubp;t1++) {
    for (t2=0;t2<=floord(K_VAL-1,1118);t2++) {
      for (t3=108*t1;t3<=min(M_VAL-1,108*t1+107);t3++) {
        for (t4=1118*t2;t4<=min(K_VAL-1,1118*t2+1117);t4++) {
          C[t3] += A[t3][t4] * B[t4];;
        }
      }
    }
  }
}
/* End of CLooG code */
    // kernel end
    if(clock_gettime(CLOCK_REALTIME, &stop) == -1) {
        perror("clock gettime");
        return EXIT_FAILURE;
    }
    runtime = ( stop.tv_sec - start.tv_sec ) * 1000000000L + ( stop.tv_nsec - start.tv_nsec );
    if (runtime < min_runtime)
        min_runtime = runtime;
    // evaluation loop end
    }
    printf("%ld", min_runtime);

    // gold check float C M_VAL
    {
        float C_gold[M_VAL];
        FILE *C_gold_file = fopen("/home/r/r_schu41/mdh-project/benchmarks/2022-toplas/Pluto/matvec_row_major_n/4096x4096/build_normal/../gold/Pluto/12e075a/matvec_row_major_n/4096x4096/gold.tsv", "r");
        {
            int i = 0;
            while (fscanf(C_gold_file, "%f", &(((float*)C_gold)[i])) == 1) {
                ++i;
            }
            if (i != 1*M_VAL) {
                printf("incorrect result buffer size for buffer C: expected %d, actual %d.", i, 1*M_VAL);
                exit(EXIT_FAILURE);
            }
        }
        fclose(C_gold_file);

        for (int i = 0; i < 1*M_VAL; ++i) {
            if (0.100000 == 0) {
                if (((float*)C)[i] != ((float*)C_gold)[i]) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            } else {
                if (fabs(((float*)C)[i] - ((float*)C_gold)[i]) > 0.100000) {
                    printf("incorrect result at index %d: expected %f, actual %f", i, ((float*)C_gold)[i], ((float*)C)[i]);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
}
