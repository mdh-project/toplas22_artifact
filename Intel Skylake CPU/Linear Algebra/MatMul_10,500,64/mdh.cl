__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 2]
                                 [1 * 10];
    __local float a_lcl[1 * 5 * 2]
                                 [64 * 1 * 1];
    __local float b_lcl[64 * 1 * 1]
                                 [1 * 2 * 10];
    {
    const size_t wg_2 = get_group_id(0); {
    {
    const size_t wi_1 = get_local_id(1); {
    const size_t wi_2 = get_local_id(0); {
    {
    {
    {
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
    for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
        c_prv[0 * 2 + prv_1][0 * 10 + prv_2]
            = 0.0f;
    }}}}
    }
    {
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((1 * 5 * 2 ) * (64 * 1 * 1 )) / 4) / (5 * 2 * 1); ++step)
            {
                ((__local float4*)a_lcl)[((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) / (((64 * 1 * 1 ) / 4)) % ((1 * 5 * 2 ))) * (((64 * 1 * 1 ) / 4)) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) % (((64 * 1 * 1 ) / 4))) ]
                                                                                                               =
                ((__global float4*)a_glb)[((0 * 1 * 1 * 5 * 2 + 0 * 1 * 5 * 2 ) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) / (((64 * 1 * 1 ) / 4)) % ((1 * 5 * 2 )))) * ((64 / 4)) + (((0 * 1 * 64 * 1 * 1 + 0 * 64 * 1 * 1 ) / 4) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) % (((64 * 1 * 1 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        {
            barrier(CLK_LOCAL_MEM_FENCE);
#pragma unroll
            for (size_t step = 0; step < (((64 * 1 * 1 ) * (1 * 2 * 10 )) / 4) / (5 * 2 * 1); ++step)
            {
                ((__local float4*)b_lcl)[((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) / (((1 * 2 * 10 ) / 4)) % ((64 * 1 * 1 ))) * (((1 * 2 * 10 ) / 4)) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) % (((1 * 2 * 10 ) / 4))) ]
                                                                                                               =
                ((__global float4*)b_glb)[((0 * 1 * 64 * 1 * 1 + 0 * 64 * 1 * 1 ) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) / (((1 * 2 * 10 ) / 4)) % ((64 * 1 * 1 )))) * ((500 / 4)) + (((0 * 25 * 1 * 2 * 10 + wg_2 * 1 * 2 * 10 ) / 4) + ((step * (5 * 2 * 1) + (wi_1 * 2 * 1 + wi_2 * 1 + 0 )) % (((1 * 2 * 10 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 64; ++lcl_3) {
            for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
            for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
            {
            c_prv[0 * 2 + prv_1][0 * 10 + prv_2]
                +=
            a_lcl[0 * 5 * 2 + wi_1 * 2 + prv_1][lcl_3 * 1 * 1 + 0 * 1 + 0]
            *
            b_lcl[lcl_3 * 1 * 1 + 0 * 1 + 0][0 * 2 * 10 + wi_2 * 10 + prv_2]
            ;
            }}}
        }}}
    }
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 2; ++prv_1) {
    for (size_t prv_2 = 0; prv_2 < 10; ++prv_2) {
        c_glb[(0 * 1 * 1 * 5 * 2 + 0 * 1 * 5 * 2 + 0 * 5 * 2 + wi_1 * 2 + prv_1) * (500) + (0 * 25 * 1 * 2 * 10 + wg_2 * 1 * 2 * 10 + 0 * 2 * 10 + wi_2 * 10 + prv_2) ]
                                               =
        c_prv[0 * 2 + prv_1][0 * 10 + prv_2]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
