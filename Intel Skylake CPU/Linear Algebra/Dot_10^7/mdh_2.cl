__kernel void dot_static_2(__global float const * const restrict int_res_c_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict c_raw) {
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = get_local_id(0);
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_r_1;
    __global float const * const restrict int_res_c_buf = int_res_c_buf_raw;
  __private float cb_p_int_res_c[(2)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict c = c_raw;
  __private float res_p_c[1];
  l_cb_offset_r_1 = i_wg_r_1 * 100;
  size_t l_step_r_1 = 0;
  {
    size_t p_step_r_1 = 0;
    {
      for (size_t step = 0; step < ((2)) / (1); ++step) {
        const size_t flat_index = (0) + step * (1);
        const size_t p_dim_0_index_r_1 = flat_index;
        cb_p_int_res_c[((p_dim_0_index_r_1))] = int_res_c_buf[((((l_step_r_1 * (200 / 100) + (((p_step_r_1 * (2) + (p_dim_0_index_r_1) / 1) * 100 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 100) * 100 + i_wg_r_1 * 100 + ((((p_step_r_1 * (2) + (p_dim_0_index_r_1) / 1) * 100 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 100))))];
      }
        }
  }
  p_cb_offset_r_1 = i_wi_r_1 * 1;
  size_t p_step_r_1 = 0;
  size_t p_iteration_r_1 = 0;
  res_p_c[(0)] = cb_p_int_res_c[(((p_iteration_r_1)))];
#pragma unroll
  for (p_iteration_r_1 = 1; p_iteration_r_1 < (2); ++p_iteration_r_1) {
    res_p_c[(0)] += cb_p_int_res_c[(((p_iteration_r_1)))];
  }
  p_cb_offset_r_1 += 100 * (2);
  l_cb_offset_r_1 += 100 * (200 / 100);
  for (l_step_r_1 = 1; l_step_r_1 < ((2000 / 100) / (200 / 100)); ++l_step_r_1) {
    {
      size_t p_step_r_1 = 0;
      {
        for (size_t step = 0; step < ((2)) / (1); ++step) {
          const size_t flat_index = (0) + step * (1);
          const size_t p_dim_0_index_r_1 = flat_index;
          cb_p_int_res_c[((p_dim_0_index_r_1))] = int_res_c_buf[((((l_step_r_1 * (200 / 100) + (((p_step_r_1 * (2) + (p_dim_0_index_r_1) / 1) * 100 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 100) * 100 + i_wg_r_1 * 100 + ((((p_step_r_1 * (2) + (p_dim_0_index_r_1) / 1) * 100 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 100))))];
        }
          }
    }
    p_cb_offset_r_1 = i_wi_r_1 * 1;
    size_t p_step_r_1 = 0;
#pragma unroll
    for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (2); ++p_iteration_r_1) {
      res_p_c[(0)] += cb_p_int_res_c[(((p_iteration_r_1)))];
    }
    p_cb_offset_r_1 += 100 * (2);
    l_cb_offset_r_1 += 100 * (200 / 100);
  }
  {
    {
      res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1)))] = res_p_c[(0)];
    }
  }
  {
    barrier(CLK_GLOBAL_MEM_FENCE);
    {
      size_t stride = 1 << (int) floor(log2((float) (100)));
        if (i_wi_r_1 < stride && i_wi_r_1 + stride < (100)) {
          res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1)))] += res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1 + stride)))];
        }
      stride /= 2;
      barrier(CLK_GLOBAL_MEM_FENCE);
      for (; stride > 0; stride /= 2) {
          if (i_wi_r_1 < stride) {
            res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1)))] += res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1 + stride)))];
          }
        barrier(CLK_GLOBAL_MEM_FENCE);
      }
      barrier(CLK_GLOBAL_MEM_FENCE);
    }
  }
  {
    if (i_wi_r_1 == 0)
    {
      c[(0)] = res_g_c[(((i_wg_r_1)) * 100 + ((i_wi_r_1)))];
    }
  }
}
