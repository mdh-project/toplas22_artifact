// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict x, __global float* restrict y, __global float* restrict dot) {
  float normal_reduce_temp0[1];
  __local float red_buf0[16];
  normal_reduce_temp0[(0)] = 0.000000e+00f;
  for (int i_outer = 0; i_outer < 625000; ++i_outer) {
    normal_reduce_temp0[(0)] = (normal_reduce_temp0[(0)] + (x[(((i_outer * 16) + ((int)get_local_id(0))))] * y[(((i_outer * 16) + ((int)get_local_id(0))))]));
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  ((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] = normal_reduce_temp0[(0)];
  barrier(CLK_LOCAL_MEM_FENCE);
  if (((int)get_local_id(0)) < 8) {
    ((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] = (((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] + ((volatile __local float*)red_buf0)[((((int)get_local_id(0)) + 8))]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  if (((int)get_local_id(0)) < 4) {
    ((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] = (((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] + ((volatile __local float*)red_buf0)[((((int)get_local_id(0)) + 4))]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  if (((int)get_local_id(0)) < 2) {
    ((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] = (((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] + ((volatile __local float*)red_buf0)[((((int)get_local_id(0)) + 2))]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  if (((int)get_local_id(0)) < 1) {
    ((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] = (((volatile __local float*)red_buf0)[(((int)get_local_id(0)))] + ((volatile __local float*)red_buf0)[((((int)get_local_id(0)) + 1))]);
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  dot[(0)] = ((volatile __local float*)red_buf0)[(0)];
}

