__kernel void matmul_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[1 * 8]
                                 [1 * 1];
    __local float b_lcl[128 * 1 * 2]
                                 [1 * 32 * 1];
    const size_t wg_1 = get_group_id(1); {
    const size_t wg_2 = get_group_id(0); {
    {
    const size_t wi_1 = get_local_id(1); {
    const size_t wi_2 = get_local_id(0); {
    {
    {
    {
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_prv[0 * 8 + prv_1][0 * 1 + 0]
            = 0.0f;
    }}}}
    }
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
        {
            barrier(CLK_LOCAL_MEM_FENCE);
            {
                ((__local float4*)b_lcl)[((0 * (64 * 32 * 1) + (wi_1 * 32 * 1 + wi_2 * 1 + 0 )) / (((1 * 32 * 1 ) / 4)) % ((128 * 1 * 2 ))) * (((1 * 32 * 1 ) / 4)) + ((0 * (64 * 32 * 1) + (wi_1 * 32 * 1 + wi_2 * 1 + 0 )) % (((1 * 32 * 1 ) / 4))) ]
                                                                                                               =
                ((__global float4*)b_glb)[((glb_3 * 1 * 128 * 1 * 2 + 0 * 128 * 1 * 2 ) + ((0 * (64 * 32 * 1) + (wi_1 * 32 * 1 + wi_2 * 1 + 0 )) / (((1 * 32 * 1 ) / 4)) % ((128 * 1 * 2 )))) * ((1024 / 4)) + (((0 * 32 * 1 * 32 * 1 + wg_2 * 1 * 32 * 1 ) / 4) + ((0 * (64 * 32 * 1) + (wi_1 * 32 * 1 + wi_2 * 1 + 0 )) % (((1 * 32 * 1 ) / 4)))) ]
                                                                                                                                                            ;
            }
        }
        barrier(CLK_LOCAL_MEM_FENCE);
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 128; ++lcl_3) {
            for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
            {
            for (size_t prv_3 = 0; prv_3 < 2; ++prv_3) {
            c_prv[0 * 8 + prv_1][0 * 1 + 0]
                +=
            a_glb[(0 * 2 * 1 * 64 * 8 + wg_1 * 1 * 64 * 8 + 0 * 64 * 8 + wi_1 * 8 + prv_1) * (1024) + (glb_3 * 1 * 128 * 1 * 2 + 0 * 128 * 1 * 2 + lcl_3 * 1 * 2 + 0 * 2 + prv_3) ]
            *
            b_lcl[lcl_3 * 1 * 2 + 0 * 2 + prv_3][0 * 32 * 1 + wi_2 * 1 + 0]
            ;
            }}}
        }}}
    }
    {
    {
    {
    for (size_t prv_1 = 0; prv_1 < 8; ++prv_1) {
    {
        c_glb[(0 * 2 * 1 * 64 * 8 + wg_1 * 1 * 64 * 8 + 0 * 64 * 8 + wi_1 * 8 + prv_1) * (1024) + (0 * 32 * 1 * 32 * 1 + wg_2 * 1 * 32 * 1 + 0 * 32 * 1 + wi_2 * 1 + 0) ]
                                               =
        c_prv[0 * 8 + prv_1][0 * 1 + 0]
                           ;
    }}}}
    }
    }}
    }}}
    }}}
}
