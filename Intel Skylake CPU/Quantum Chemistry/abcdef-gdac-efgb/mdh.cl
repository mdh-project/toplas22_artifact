inline float f(const float a_val, const float b_val) {
  return a_val * b_val;
}
__kernel void tc_abcdefg_gdac_efgb_static_1(__global float const * const restrict a, __global float const * const restrict b, __global float * const restrict res_g, __global float * const restrict int_res) {
  const size_t i_wg_l_1 = (get_group_id(2) % (12 * 24 * 2) / (24 * 2));
  const size_t i_wi_l_1 = (get_local_id(2) % (1 * 1 * 1) / (1 * 1));
  const size_t i_wg_l_2 = (get_group_id(2) % (2));
  const size_t i_wi_l_2 = (get_local_id(2) % (1));
  const size_t i_wg_l_3 = get_group_id(1);
  const size_t i_wi_l_3 = get_local_id(1);
  const size_t i_wg_l_4 = (get_group_id(2) % (2 * 12 * 24 * 2) / (12 * 24 * 2));
  const size_t i_wi_l_4 = (get_local_id(2) % (1 * 1 * 1 * 1) / (1 * 1 * 1));
  const size_t i_wg_l_5 = (get_group_id(2) % (24 * 2) / (2));
  const size_t i_wi_l_5 = (get_local_id(2) % (1 * 1) / (1));
  const size_t i_wg_l_6 = get_group_id(0);
  const size_t i_wi_l_6 = get_local_id(0);
  const size_t i_wg_r_1 = (get_group_id(2) / (2 * 12 * 24 * 2));
  const size_t i_wi_r_1 = (get_local_id(2) / (1 * 1 * 1 * 1));
  __private float cb_p_a[1][2][1][1];
  __private float cb_p_b[1][1][1][1];
  __private float res_p[((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((4 / 1) / (2)) + (((1 * (4 / 1)) % (1 * (2)) / 1) > 0) + (((1 * (4 / 1)) % (1 * (2)) % 1) > 0)][2][1][((8 / 2) / (1)) + (((2 * (8 / 2)) % (2 * (1)) / 2) > 0) + (((2 * (8 / 2)) % (2 * (1)) % 2) > 0)][1][((16 / 16) / (1)) + (((16 * (16 / 16)) % (16 * (1)) / 16) > 0) + (((16 * (16 / 16)) % (16 * (1)) % 16) > 0)][1];
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((24 / (12 * 1)) / (1 / 1)); ++l_step_l_1) {
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((16 / (2 * 1)) / (1 / 1)); ++l_step_l_2) {
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((16 / (1 * 2)) / (8 / 2)); ++l_step_l_3) {
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((16 / (2 * 1)) / (4 / 1)); ++l_step_l_4) {
          for (size_t l_step_l_5 = 0; l_step_l_5 < ((24 / (24 * 1)) / (1 / 1)); ++l_step_l_5) {
            for (size_t l_step_l_6 = 0; l_step_l_6 < ((16 / (1 * 16)) / (16 / 16)); ++l_step_l_6) {
              size_t l_step_r_1 = 0;
              for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
                for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                  for (size_t p_step_l_3 = 0; p_step_l_3 < ((8 / 2) / (1)); ++p_step_l_3) {
                    for (size_t p_step_l_4 = 0; p_step_l_4 < ((4 / 1) / (2)); ++p_step_l_4) {
                      for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                        for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                          size_t p_step_r_1 = 0;
                          for (size_t step = 0; step < ((1) * (2) * (1) * (1)) / (1); ++step) {
                            const size_t flat_index = (0) + step * (1);
                            const size_t p_index_r_1 = flat_index / ((2) * (1) * (1));
                            const size_t p_index_l_4 = (flat_index / ((1) * (1))) % (2);
                            const size_t p_index_l_1 = (flat_index / ((1))) % (1);
                            const size_t p_index_l_3 = flat_index % (1);
                            cb_p_a[(p_index_l_1)][(p_index_l_4)][(p_index_r_1)][(p_index_l_3)] = a[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) % 1))) * 16 * 24 * 16 + (((l_step_l_4 * (4 / 1) + (((p_step_l_4 * (2) + (p_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_index_l_4) % 1))) / 1) * (2 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (2) + (p_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_index_l_4) % 1))) % 1))) * 24 * 16 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (p_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_index_l_1) % 1))) / 1) * (12 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (p_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_index_l_1) % 1))) % 1))) * 16 + (((l_step_l_3 * (8 / 2) + (((p_step_l_3 * (1) + (p_index_l_3) / 1) * 2 + i_wi_l_3 * 1 + ((p_index_l_3) % 1))) / 2) * (1 * 2) + i_wg_l_3 * 2 + ((((p_step_l_3 * (1) + (p_index_l_3) / 1) * 2 + i_wi_l_3 * 1 + ((p_index_l_3) % 1))) % 2)))];
                          }
                          for (size_t step = 0; step < ((1) * (1) * (1) * (1)) / (1); ++step) {
                            const size_t flat_index = (0) + step * (1);
                            const size_t p_index_l_5 = flat_index / ((1) * (1) * (1));
                            const size_t p_index_l_6 = (flat_index / ((1) * (1))) % (1);
                            const size_t p_index_r_1 = (flat_index / ((1))) % (1);
                            const size_t p_index_l_2 = flat_index % (1);
                            cb_p_b[(p_index_l_2)][(p_index_l_5)][(p_index_r_1)][(p_index_l_6)] = b[(((l_step_l_5 * (1 / 1) + (((p_step_l_5 * (1) + (p_index_l_5) / 1) * 1 + i_wi_l_5 * 1 + ((p_index_l_5) % 1))) / 1) * (24 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (1) + (p_index_l_5) / 1) * 1 + i_wi_l_5 * 1 + ((p_index_l_5) % 1))) % 1))) * 16 * 24 * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (p_index_l_6) / 1) * 16 + i_wi_l_6 * 1 + ((p_index_l_6) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (p_index_l_6) / 1) * 16 + i_wi_l_6 * 1 + ((p_index_l_6) % 1))) % 16))) * 24 * 16 + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) % 1))) * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (p_index_l_2) / 1) * 1 + i_wi_l_2 * 1 + ((p_index_l_2) % 1))) / 1) * (2 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (p_index_l_2) / 1) * 1 + i_wi_l_2 * 1 + ((p_index_l_2) % 1))) % 1)))];
                          }
                          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                              for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4) {
                                  for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5) {
                                    for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                      size_t p_iteration_r_1 = 0;
                                      res_p[p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)] = f(
                                          cb_p_a[(((p_iteration_l_1) * 1 + 0))][(((p_iteration_l_4) * 1 + 0))][(((p_iteration_r_1) * 1 + 0))][(((p_iteration_l_3) * 1 + 0))],
                                          cb_p_b[(((p_iteration_l_2) * 1 + 0))][(((p_iteration_l_5) * 1 + 0))][(((p_iteration_r_1) * 1 + 0))][(((p_iteration_l_6) * 1 + 0))]
                                      );
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              for (l_step_r_1 = 1; l_step_r_1 < ((24 / (1 * 1)) / (1 / 1)); ++l_step_r_1) {
                for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
                  for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                    for (size_t p_step_l_3 = 0; p_step_l_3 < ((8 / 2) / (1)); ++p_step_l_3) {
                      for (size_t p_step_l_4 = 0; p_step_l_4 < ((4 / 1) / (2)); ++p_step_l_4) {
                        for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                          for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                            size_t p_step_r_1 = 0;
                            for (size_t step = 0; step < ((1) * (2) * (1) * (1)) / (1); ++step) {
                              const size_t flat_index = (0) + step * (1);
                              const size_t p_index_r_1 = flat_index / ((2) * (1) * (1));
                              const size_t p_index_l_4 = (flat_index / ((1) * (1))) % (2);
                              const size_t p_index_l_1 = (flat_index / ((1))) % (1);
                              const size_t p_index_l_3 = flat_index % (1);
                              cb_p_a[(p_index_l_1)][(p_index_l_4)][(p_index_r_1)][(p_index_l_3)] = a[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) % 1))) * 16 * 24 * 16 + (((l_step_l_4 * (4 / 1) + (((p_step_l_4 * (2) + (p_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_index_l_4) % 1))) / 1) * (2 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (2) + (p_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_index_l_4) % 1))) % 1))) * 24 * 16 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (p_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_index_l_1) % 1))) / 1) * (12 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (p_index_l_1) / 1) * 1 + i_wi_l_1 * 1 + ((p_index_l_1) % 1))) % 1))) * 16 + (((l_step_l_3 * (8 / 2) + (((p_step_l_3 * (1) + (p_index_l_3) / 1) * 2 + i_wi_l_3 * 1 + ((p_index_l_3) % 1))) / 2) * (1 * 2) + i_wg_l_3 * 2 + ((((p_step_l_3 * (1) + (p_index_l_3) / 1) * 2 + i_wi_l_3 * 1 + ((p_index_l_3) % 1))) % 2)))];
                            }
                            for (size_t step = 0; step < ((1) * (1) * (1) * (1)) / (1); ++step) {
                              const size_t flat_index = (0) + step * (1);
                              const size_t p_index_l_5 = flat_index / ((1) * (1) * (1));
                              const size_t p_index_l_6 = (flat_index / ((1) * (1))) % (1);
                              const size_t p_index_r_1 = (flat_index / ((1))) % (1);
                              const size_t p_index_l_2 = flat_index % (1);
                              cb_p_b[(p_index_l_2)][(p_index_l_5)][(p_index_r_1)][(p_index_l_6)] = b[(((l_step_l_5 * (1 / 1) + (((p_step_l_5 * (1) + (p_index_l_5) / 1) * 1 + i_wi_l_5 * 1 + ((p_index_l_5) % 1))) / 1) * (24 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (1) + (p_index_l_5) / 1) * 1 + i_wi_l_5 * 1 + ((p_index_l_5) % 1))) % 1))) * 16 * 24 * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (p_index_l_6) / 1) * 16 + i_wi_l_6 * 1 + ((p_index_l_6) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (p_index_l_6) / 1) * 16 + i_wi_l_6 * 1 + ((p_index_l_6) % 1))) % 16))) * 24 * 16 + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_index_r_1) % 1))) % 1))) * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (p_index_l_2) / 1) * 1 + i_wi_l_2 * 1 + ((p_index_l_2) % 1))) / 1) * (2 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (p_index_l_2) / 1) * 1 + i_wi_l_2 * 1 + ((p_index_l_2) % 1))) % 1)))];
                            }
                            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                                for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                  for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4) {
                                    for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5) {
                                      for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                        for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                                          res_p[p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)] += f(
                                              cb_p_a[(((p_iteration_l_1) * 1 + 0))][(((p_iteration_l_4) * 1 + 0))][(((p_iteration_r_1) * 1 + 0))][(((p_iteration_l_3) * 1 + 0))],
                                              cb_p_b[(((p_iteration_l_2) * 1 + 0))][(((p_iteration_l_5) * 1 + 0))][(((p_iteration_r_1) * 1 + 0))][(((p_iteration_l_6) * 1 + 0))]
                                          );
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              {
                if (i_wi_r_1 == 0) {
                  for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
                    for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                      for (size_t p_step_l_3 = 0; p_step_l_3 < ((8 / 2) / (1)); ++p_step_l_3) {
                        for (size_t p_step_l_4 = 0; p_step_l_4 < ((4 / 1) / (2)); ++p_step_l_4) {
                          for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                            for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1)
                                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2)
                                  for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3)
                                    for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4)
                                      for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5)
                                        for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6)
                                          int_res[(((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (12 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1))) * 16 * 16 * 16 * 24 * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (2 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 16 * 16 * 24 * 16 + (((l_step_l_3 * (8 / 2) + (((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 2 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 2) * (1 * 2) + i_wg_l_3 * 2 + ((((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 2 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 2))) * 16 * 24 * 16 + (((l_step_l_4 * (4 / 1) + (((p_step_l_4 * (2) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (2 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (2) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 24 * 16 + (((l_step_l_5 * (1 / 1) + (((p_step_l_5 * (1) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) / 1) * (24 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (1) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) % 1))) * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) % 16)))] = res_p[p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)];
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
