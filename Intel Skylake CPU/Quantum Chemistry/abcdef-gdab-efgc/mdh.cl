__kernel void tc_abcdefg_gdab_efgc_static_1(__global float const * const restrict a_buf_raw, __global float const * const restrict b_buf_raw, __global float * const restrict res_g_c_raw, __global float * const restrict int_res_c_raw) {
    const size_t i_wg_l_1 = (get_group_id(2) % (3 * 16 * 4) / (16 * 4));
    const size_t i_wi_l_1 = (get_local_id(2) % (4 * 1 * 2) / (1 * 2));
    const size_t i_wg_l_2 = (get_group_id(2) % (4));
    const size_t i_wi_l_2 = (get_local_id(2) % (2));
    const size_t i_wg_l_3 = (get_group_id(2) % (16 * 4) / (4));
    const size_t i_wi_l_3 = 0;
    const size_t i_wg_l_4 = (get_group_id(2) / (1 * 3 * 16 * 4));
    const size_t i_wi_l_4 = 0;
    const size_t i_wg_l_5 = 0;
    const size_t i_wi_l_5 = 0;
    const size_t i_wg_l_6 = 0;
    const size_t i_wi_l_6 = get_local_id(0);
    const size_t i_wg_r_1 = 0;
    const size_t i_wi_r_1 = 0;
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_l_2;
  size_t l_cb_offset_l_3;
  size_t l_cb_offset_l_4;
  size_t l_cb_offset_l_5;
  size_t l_cb_offset_l_6;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_l_2;
  size_t p_cb_offset_l_3;
  size_t p_cb_offset_l_4;
  size_t p_cb_offset_l_5;
  size_t p_cb_offset_l_6;
  size_t p_cb_offset_r_1;
    __global float const * const restrict a_buf = a_buf_raw;
    __global float const * const restrict b_buf = b_buf_raw;
  __private float cb_p_a[(1)][(1)][(1)][(2)];
  __global float * const restrict res_g_c = res_g_c_raw;
  __global float * const restrict int_res_c = int_res_c_raw;
  __private float res_p_c[((4 / 2) / (1)) + (((2 * (4 / 2)) % (2 * (1)) / 2) > 0) + (((2 * (4 / 2)) % (2 * (1)) % 2) > 0)][1][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((8 / 4) / (1)) + (((4 * (8 / 4)) % (4 * (1)) / 4) > 0) + (((4 * (8 / 4)) % (4 * (1)) % 4) > 0)][1][1][((2 / 1) / (2)) + (((1 * (2 / 1)) % (1 * (2)) / 1) > 0) + (((1 * (2 / 1)) % (1 * (2)) % 1) > 0)][2][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((16 / 16) / (1)) + (((16 * (16 / 16)) % (16 * (1)) / 16) > 0) + (((16 * (16 / 16)) % (16 * (1)) % 16) > 0)][1];
  l_cb_offset_l_1 = i_wg_l_1 * 4;
  barrier(CLK_LOCAL_MEM_FENCE);
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((24 / (3 * 4)) / (8 / 4)); ++l_step_l_1) {
    barrier(CLK_LOCAL_MEM_FENCE);
    l_cb_offset_l_2 = i_wg_l_2 * 2;
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((16 / (4 * 2)) / (4 / 2)); ++l_step_l_2) {
      barrier(CLK_LOCAL_MEM_FENCE);
      l_cb_offset_l_3 = i_wg_l_3 * 1;
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((16 / (16 * 1)) / (1 / 1)); ++l_step_l_3) {
        barrier(CLK_LOCAL_MEM_FENCE);
        l_cb_offset_l_4 = i_wg_l_4 * 1;
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((16 / (8 * 1)) / (2 / 1)); ++l_step_l_4) {
          barrier(CLK_LOCAL_MEM_FENCE);
          l_cb_offset_l_5 = i_wg_l_5 * 1;
          for (size_t l_step_l_5 = 0; l_step_l_5 < ((24 / (1 * 1)) / (1 / 1)); ++l_step_l_5) {
            barrier(CLK_LOCAL_MEM_FENCE);
            l_cb_offset_l_6 = i_wg_l_6 * 16;
            for (size_t l_step_l_6 = 0; l_step_l_6 < ((16 / (1 * 16)) / (16 / 16)); ++l_step_l_6) {
              barrier(CLK_LOCAL_MEM_FENCE);
              l_cb_offset_r_1 = i_wg_r_1 * 1;
              size_t l_step_r_1 = 0;
              p_cb_offset_l_1 = i_wi_l_1 * 1;
              for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 4) / (1)); ++p_step_l_1) {
                p_cb_offset_l_2 = i_wi_l_2 * 1;
                for (size_t p_step_l_2 = 0; p_step_l_2 < ((4 / 2) / (1)); ++p_step_l_2) {
                  {
                    size_t p_step_l_4 = 0;
                    size_t p_step_r_1 = 0;
                    {
                      for (size_t step = 0; step < ((1)) * ((2)) * ((1)) * ((1)) / (1); ++step) {
                        const size_t flat_index = (0) + step * (1);
                        const size_t p_dim_0_index_r_1 = flat_index / (((2)) * ((1)) * ((1)));
                        const size_t p_dim_1_index_l_4 = (flat_index / (((1)) * ((1)))) % ((2));
                        const size_t p_dim_2_index_l_1 = (flat_index / (((1)))) % ((1));
                        const size_t p_dim_3_index_l_2 = flat_index % ((1));
                        cb_p_a[((p_dim_3_index_l_2))][((p_dim_2_index_l_1))][((p_dim_0_index_r_1))][((p_dim_1_index_l_4))] = a_buf[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 1))) * (16) * (24) * (16) + (((l_step_l_4 * (2 / 1) + (((p_step_l_4 * (2) + (p_dim_1_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_1_index_l_4) % 1))) / 1) * (8 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (2) + (p_dim_1_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_1_index_l_4) % 1))) % 1))) * (24) * (16) + (((l_step_l_1 * (8 / 4) + (((p_step_l_1 * (1) + (p_dim_2_index_l_1) / 1) * 4 + i_wi_l_1 * 1 + ((p_dim_2_index_l_1) % 1))) / 4) * (3 * 4) + i_wg_l_1 * 4 + ((((p_step_l_1 * (1) + (p_dim_2_index_l_1) / 1) * 4 + i_wi_l_1 * 1 + ((p_dim_2_index_l_1) % 1))) % 4))) * (16) + (((l_step_l_2 * (4 / 2) + (((p_step_l_2 * (1) + (p_dim_3_index_l_2) / 1) * 2 + i_wi_l_2 * 1 + ((p_dim_3_index_l_2) % 1))) / 2) * (4 * 2) + i_wg_l_2 * 2 + ((((p_step_l_2 * (1) + (p_dim_3_index_l_2) / 1) * 2 + i_wi_l_2 * 1 + ((p_dim_3_index_l_2) % 1))) % 2)))];
                      }
                        }
                  }
                  p_cb_offset_l_3 = i_wi_l_3 * 1;
                  for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                    p_cb_offset_l_4 = i_wi_l_4 * 1;
                    for (size_t p_step_l_4 = 0; p_step_l_4 < ((2 / 1) / (2)); ++p_step_l_4) {
                      p_cb_offset_l_5 = i_wi_l_5 * 1;
                      for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                        p_cb_offset_l_6 = i_wi_l_6 * 1;
                        for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                          p_cb_offset_r_1 = i_wi_r_1 * 1;
                          size_t p_step_r_1 = 0;
#pragma unroll
                          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
#pragma unroll
                            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
#pragma unroll
                              for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
#pragma unroll
                                for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4) {
#pragma unroll
                                  for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5) {
#pragma unroll
                                    for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                      size_t p_iteration_r_1 = 0;
                                      res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_3][(p_iteration_l_3)][p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_4][(p_iteration_l_4)][p_step_l_5][(p_iteration_l_5)][p_step_l_6][(p_iteration_l_6)] = cb_p_a[(((p_iteration_l_2)))][(((p_iteration_l_1)))][(((p_iteration_r_1)))][(((p_iteration_l_4)))] * b_buf[((l_cb_offset_l_5 + (((p_cb_offset_l_5 + (((p_iteration_l_5)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_5)) * (16) * (24) * (16) + ((l_cb_offset_l_6 + (((p_cb_offset_l_6 + (((p_iteration_l_6)) / 1) * 16 + 0)) / 16) * (1 * 16) + i_wi_l_6)) * (24) * (16) + ((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_r_1)) * (16) + ((l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 1 + 0)) / 1) * (16 * 1) + i_wi_l_3))];
                                    }
                                  }
                                }
                              }
                            }
                          }
                          p_cb_offset_r_1 += 1 * (1);
                          p_cb_offset_l_6 += 16 * (1);
                        }
                        p_cb_offset_l_5 += 1 * (1);
                      }
                      p_cb_offset_l_4 += 1 * (2);
                    }
                    p_cb_offset_l_3 += 1 * (1);
                  }
                  p_cb_offset_l_2 += 2 * (1);
                }
                p_cb_offset_l_1 += 4 * (1);
              }
              l_cb_offset_r_1 += (1 * 1) * (1 / 1);
              for (l_step_r_1 = 1; l_step_r_1 < ((24 / (1 * 1)) / (1 / 1)); ++l_step_r_1) {
                p_cb_offset_l_1 = i_wi_l_1 * 1;
                for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 4) / (1)); ++p_step_l_1) {
                  p_cb_offset_l_2 = i_wi_l_2 * 1;
                  for (size_t p_step_l_2 = 0; p_step_l_2 < ((4 / 2) / (1)); ++p_step_l_2) {
                    {
                      size_t p_step_l_4 = 0;
                      size_t p_step_r_1 = 0;
                      {
                        for (size_t step = 0; step < ((1)) * ((2)) * ((1)) * ((1)) / (1); ++step) {
                          const size_t flat_index = (0) + step * (1);
                          const size_t p_dim_0_index_r_1 = flat_index / (((2)) * ((1)) * ((1)));
                          const size_t p_dim_1_index_l_4 = (flat_index / (((1)) * ((1)))) % ((2));
                          const size_t p_dim_2_index_l_1 = (flat_index / (((1)))) % ((1));
                          const size_t p_dim_3_index_l_2 = flat_index % ((1));
                          cb_p_a[((p_dim_3_index_l_2))][((p_dim_2_index_l_1))][((p_dim_0_index_r_1))][((p_dim_1_index_l_4))] = a_buf[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (p_dim_0_index_r_1) / 1) * 1 + i_wi_r_1 * 1 + ((p_dim_0_index_r_1) % 1))) % 1))) * (16) * (24) * (16) + (((l_step_l_4 * (2 / 1) + (((p_step_l_4 * (2) + (p_dim_1_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_1_index_l_4) % 1))) / 1) * (8 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (2) + (p_dim_1_index_l_4) / 1) * 1 + i_wi_l_4 * 1 + ((p_dim_1_index_l_4) % 1))) % 1))) * (24) * (16) + (((l_step_l_1 * (8 / 4) + (((p_step_l_1 * (1) + (p_dim_2_index_l_1) / 1) * 4 + i_wi_l_1 * 1 + ((p_dim_2_index_l_1) % 1))) / 4) * (3 * 4) + i_wg_l_1 * 4 + ((((p_step_l_1 * (1) + (p_dim_2_index_l_1) / 1) * 4 + i_wi_l_1 * 1 + ((p_dim_2_index_l_1) % 1))) % 4))) * (16) + (((l_step_l_2 * (4 / 2) + (((p_step_l_2 * (1) + (p_dim_3_index_l_2) / 1) * 2 + i_wi_l_2 * 1 + ((p_dim_3_index_l_2) % 1))) / 2) * (4 * 2) + i_wg_l_2 * 2 + ((((p_step_l_2 * (1) + (p_dim_3_index_l_2) / 1) * 2 + i_wi_l_2 * 1 + ((p_dim_3_index_l_2) % 1))) % 2)))];
                        }
                          }
                    }
                    p_cb_offset_l_3 = i_wi_l_3 * 1;
                    for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                      p_cb_offset_l_4 = i_wi_l_4 * 1;
                      for (size_t p_step_l_4 = 0; p_step_l_4 < ((2 / 1) / (2)); ++p_step_l_4) {
                        p_cb_offset_l_5 = i_wi_l_5 * 1;
                        for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                          p_cb_offset_l_6 = i_wi_l_6 * 1;
                          for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                            p_cb_offset_r_1 = i_wi_r_1 * 1;
                            size_t p_step_r_1 = 0;
#pragma unroll
                            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                                for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                  for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4) {
                                    for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5) {
                                      for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                        for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                                          res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_3][(p_iteration_l_3)][p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_4][(p_iteration_l_4)][p_step_l_5][(p_iteration_l_5)][p_step_l_6][(p_iteration_l_6)] += cb_p_a[(((p_iteration_l_2)))][(((p_iteration_l_1)))][(((p_iteration_r_1)))][(((p_iteration_l_4)))] * b_buf[((l_cb_offset_l_5 + (((p_cb_offset_l_5 + (((p_iteration_l_5)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_5)) * (16) * (24) * (16) + ((l_cb_offset_l_6 + (((p_cb_offset_l_6 + (((p_iteration_l_6)) / 1) * 16 + 0)) / 16) * (1 * 16) + i_wi_l_6)) * (24) * (16) + ((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_r_1)) * (16) + ((l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 1 + 0)) / 1) * (16 * 1) + i_wi_l_3))];
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            p_cb_offset_r_1 += 1 * (1);
                            p_cb_offset_l_6 += 16 * (1);
                          }
                          p_cb_offset_l_5 += 1 * (1);
                        }
                        p_cb_offset_l_4 += 1 * (2);
                      }
                      p_cb_offset_l_3 += 1 * (1);
                    }
                    p_cb_offset_l_2 += 2 * (1);
                  }
                  p_cb_offset_l_1 += 4 * (1);
                }
                l_cb_offset_r_1 += (1 * 1) * (1 / 1);
              }
              {
                {
                  p_cb_offset_l_1 = i_wi_l_1 * 1;
                  for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 4) / (1)); ++p_step_l_1) {
                    p_cb_offset_l_2 = i_wi_l_2 * 1;
                    for (size_t p_step_l_2 = 0; p_step_l_2 < ((4 / 2) / (1)); ++p_step_l_2) {
                      p_cb_offset_l_3 = i_wi_l_3 * 1;
                      for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                        p_cb_offset_l_4 = i_wi_l_4 * 1;
                        for (size_t p_step_l_4 = 0; p_step_l_4 < ((2 / 1) / (2)); ++p_step_l_4) {
                          p_cb_offset_l_5 = i_wi_l_5 * 1;
                          for (size_t p_step_l_5 = 0; p_step_l_5 < ((1 / 1) / (1)); ++p_step_l_5) {
                            p_cb_offset_l_6 = i_wi_l_6 * 1;
                            for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                                  for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                    for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (2); ++p_iteration_l_4) {
                                      for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (1); ++p_iteration_l_5) {
                                        for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                          int_res_c[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 4 + 0)) / 4) * (3 * 4) + i_wi_l_1)) * (((16 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) * (((24 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) + ((l_cb_offset_l_2 + (((p_cb_offset_l_2 + (((p_iteration_l_2)) / 1) * 2 + 0)) / 2) * (4 * 2) + i_wi_l_2)) * (((16 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) * (((24 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) + ((l_cb_offset_l_3 + (((p_cb_offset_l_3 + (((p_iteration_l_3)) / 1) * 1 + 0)) / 1) * (16 * 1) + i_wi_l_3)) * (((16 - 1)) - (0) + 1) * (((24 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) + ((l_cb_offset_l_4 + (((p_cb_offset_l_4 + (((p_iteration_l_4)) / 1) * 1 + 0)) / 1) * (8 * 1) + i_wi_l_4)) * (((24 - 1)) - (0) + 1) * (((16 - 1)) - (0) + 1) + ((l_cb_offset_l_5 + (((p_cb_offset_l_5 + (((p_iteration_l_5)) / 1) * 1 + 0)) / 1) * (1 * 1) + i_wi_l_5)) * (((16 - 1)) - (0) + 1) + ((l_cb_offset_l_6 + (((p_cb_offset_l_6 + (((p_iteration_l_6)) / 1) * 16 + 0)) / 16) * (1 * 16) + i_wi_l_6))] = res_p_c[p_step_l_2][(p_iteration_l_2)][p_step_l_3][(p_iteration_l_3)][p_step_l_1][(p_iteration_l_1)][(0)][p_step_l_4][(p_iteration_l_4)][p_step_l_5][(p_iteration_l_5)][p_step_l_6][(p_iteration_l_6)];
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                              p_cb_offset_l_6 += 16 * (1);
                            }
                            p_cb_offset_l_5 += 1 * (1);
                          }
                          p_cb_offset_l_4 += 1 * (2);
                        }
                        p_cb_offset_l_3 += 1 * (1);
                      }
                      p_cb_offset_l_2 += 2 * (1);
                    }
                    p_cb_offset_l_1 += 4 * (1);
                  }
                }
              }
              l_cb_offset_l_6 += (1 * 16) * (16 / 16);
            }
            l_cb_offset_l_5 += (1 * 1) * (1 / 1);
          }
          l_cb_offset_l_4 += (8 * 1) * (2 / 1);
        }
        l_cb_offset_l_3 += (16 * 1) * (1 / 1);
      }
      l_cb_offset_l_2 += (4 * 2) * (4 / 2);
    }
    l_cb_offset_l_1 += (3 * 4) * (8 / 4);
  }
}
