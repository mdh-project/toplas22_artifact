// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gdab_efgc) {
  float tc_abcdef_gdab_efgc_local[64];
  __local float matA_shared[1024];
  __local float matB_shared[3072];
  for (int a_c_inner_init = 0; a_c_inner_init < 2; ++a_c_inner_init) {
    for (int e_c_inner_init = 0; e_c_inner_init < 8; ++e_c_inner_init) {
      tc_abcdef_gdab_efgc_local[(((a_c_inner_init * 8) + e_c_inner_init))] = 0.000000e+00f;
      tc_abcdef_gdab_efgc_local[((((a_c_inner_init * 8) + e_c_inner_init) + 16))] = 0.000000e+00f;
      tc_abcdef_gdab_efgc_local[((((a_c_inner_init * 8) + e_c_inner_init) + 32))] = 0.000000e+00f;
      tc_abcdef_gdab_efgc_local[((((a_c_inner_init * 8) + e_c_inner_init) + 48))] = 0.000000e+00f;
    }
  }
  for (int g_outer_outer = 0; g_outer_outer < 3; ++g_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int ax0_ax1_fused_ax2_fused_ax3_fused_inner_s = 0; ax0_ax1_fused_ax2_fused_ax3_fused_inner_s < 2; ++ax0_ax1_fused_ax2_fused_ax3_fused_inner_s) {
      if (((((int)get_local_id(0)) * 2) + ax0_ax1_fused_ax2_fused_ax3_fused_inner_s) < 1024) {
        if (((int)get_local_id(0)) < 512) {
          matA_shared[(((((int)get_local_id(0)) * 2) + ax0_ax1_fused_ax2_fused_ax3_fused_inner_s))] = matA[(((((((g_outer_outer * 49152) + ((((((int)get_local_id(0)) * 2) + ax0_ax1_fused_ax2_fused_ax3_fused_inner_s) >> 3) * 384)) + ((((int)get_group_id(0)) >> 6) * 32)) + (((((((int)get_local_id(0)) * 2) + ax0_ax1_fused_ax2_fused_ax3_fused_inner_s) & 7) >> 2) * 16)) + (((((int)get_group_id(0)) & 63) >> 4) * 4)) + (((((int)get_local_id(0)) * 2) + ax0_ax1_fused_ax2_fused_ax3_fused_inner_s) & 3)))];
        }
      }
    }
    for (int ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer = 0; ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer < 4; ++ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer) {
      matB_shared[(((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 768) + ((int)get_local_id(0))))] = matB[((((((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 36864) + ((((int)get_local_id(0)) >> 3) * 384)) + (g_outer_outer * 128)) + ((((int)get_local_id(0)) & 7) * 16)) + (((int)get_group_id(0)) & 15)))];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int g_inner = 0; g_inner < 8; ++g_inner) {
      for (int a_c_inner = 0; a_c_inner < 2; ++a_c_inner) {
        for (int e_c_inner = 0; e_c_inner < 8; ++e_c_inner) {
          tc_abcdef_gdab_efgc_local[(((a_c_inner * 8) + e_c_inner))] = (tc_abcdef_gdab_efgc_local[(((a_c_inner * 8) + e_c_inner))] + (matA_shared[(((((g_inner * 128) + (((((int)get_local_id(0)) % 384) / 48) * 8)) + (a_c_inner * 4)) + (((int)get_local_id(0)) / 384)))] * matB_shared[(((((((((int)get_local_id(0)) % 48) >> 4) * 1024) + (e_c_inner * 128)) + ((((int)get_local_id(0)) & 15) * 8)) + g_inner))]));
          tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 16))] = (tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 16))] + (matA_shared[((((((g_inner * 128) + (((((int)get_local_id(0)) % 384) / 48) * 8)) + (a_c_inner * 4)) + (((int)get_local_id(0)) / 384)) + 64))] * matB_shared[(((((((((int)get_local_id(0)) % 48) >> 4) * 1024) + (e_c_inner * 128)) + ((((int)get_local_id(0)) & 15) * 8)) + g_inner))]));
          tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 32))] = (tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 32))] + (matA_shared[((((((g_inner * 128) + (((((int)get_local_id(0)) % 384) / 48) * 8)) + (a_c_inner * 4)) + (((int)get_local_id(0)) / 384)) + 2))] * matB_shared[(((((((((int)get_local_id(0)) % 48) >> 4) * 1024) + (e_c_inner * 128)) + ((((int)get_local_id(0)) & 15) * 8)) + g_inner))]));
          tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 48))] = (tc_abcdef_gdab_efgc_local[((((a_c_inner * 8) + e_c_inner) + 48))] + (matA_shared[((((((g_inner * 128) + (((((int)get_local_id(0)) % 384) / 48) * 8)) + (a_c_inner * 4)) + (((int)get_local_id(0)) / 384)) + 66))] * matB_shared[(((((((((int)get_local_id(0)) % 48) >> 4) * 1024) + (e_c_inner * 128)) + ((((int)get_local_id(0)) & 15) * 8)) + g_inner))]));
        }
      }
    }
  }
  for (int a_inner = 0; a_inner < 2; ++a_inner) {
    for (int e_inner = 0; e_inner < 8; ++e_inner) {
      tc_abcdef_gdab_efgc[((((((((((((int)get_group_id(0)) >> 6) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 63) >> 4) * 393216)) + ((((int)get_local_id(0)) / 384) * 98304)) + ((((int)get_group_id(0)) & 15) * 6144)) + (((((int)get_local_id(0)) % 384) >> 4) * 128)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)))] = tc_abcdef_gdab_efgc_local[(((a_inner * 8) + e_inner))];
      tc_abcdef_gdab_efgc[(((((((((((((int)get_group_id(0)) >> 6) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 63) >> 4) * 393216)) + ((((int)get_local_id(0)) / 384) * 98304)) + ((((int)get_group_id(0)) & 15) * 6144)) + (((((int)get_local_id(0)) % 384) >> 4) * 128)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)) + 3072))] = tc_abcdef_gdab_efgc_local[((((a_inner * 8) + e_inner) + 16))];
      tc_abcdef_gdab_efgc[(((((((((((((int)get_group_id(0)) >> 6) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 63) >> 4) * 393216)) + ((((int)get_local_id(0)) / 384) * 98304)) + ((((int)get_group_id(0)) & 15) * 6144)) + (((((int)get_local_id(0)) % 384) >> 4) * 128)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)) + 196608))] = tc_abcdef_gdab_efgc_local[((((a_inner * 8) + e_inner) + 32))];
      tc_abcdef_gdab_efgc[(((((((((((((int)get_group_id(0)) >> 6) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 63) >> 4) * 393216)) + ((((int)get_local_id(0)) / 384) * 98304)) + ((((int)get_group_id(0)) & 15) * 6144)) + (((((int)get_local_id(0)) % 384) >> 4) * 128)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)) + 199680))] = tc_abcdef_gdab_efgc_local[((((a_inner * 8) + e_inner) + 48))];
    }
  }
}

