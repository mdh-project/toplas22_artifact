// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_geab_dfgc) {
  float tc_abcdef_geab_dfgc_local[64];
  __local float matA_shared[1536];
  __local float matB_shared[6144];
  for (int d_c_outer_inner_init = 0; d_c_outer_inner_init < 8; ++d_c_outer_inner_init) {
    for (int e_c_outer_inner_init = 0; e_c_outer_inner_init < 4; ++e_c_outer_inner_init) {
      tc_abcdef_geab_dfgc_local[(((d_c_outer_inner_init * 4) + e_c_outer_inner_init))] = 0.000000e+00f;
      tc_abcdef_geab_dfgc_local[((((d_c_outer_inner_init * 4) + e_c_outer_inner_init) + 32))] = 0.000000e+00f;
    }
  }
  for (int ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer = 0; ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer < 2; ++ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer) {
    int3 _1 = (((((int3)((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 73728), (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 73728), (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 73728))) + (((((((int3)(4, 4, 4)) >= ((int3)(0, 0, 0))) && ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) >= ((int3)(0, 0, 0)))) || ((((int3)(4, 4, 4)) < ((int3)(0, 0, 0))) && ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) <= ((int3)(0, 0, 0))))) ? (((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) / ((int3)(4, 4, 4))) : ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) / ((int3)(4, 4, 4))) - ((int3)(1, 1, 1)))) * ((int3)(384, 384, 384)))) + ((int3)(((((int)get_group_id(0)) / 384) * 64), ((((int)get_group_id(0)) / 384) * 64), ((((int)get_group_id(0)) / 384) * 64)))) + (((((((int3)(4, 4, 4)) >= ((int3)(0, 0, 0))) && ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) >= ((int3)(0, 0, 0)))) || ((((int3)(4, 4, 4)) < ((int3)(0, 0, 0))) && ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) <= ((int3)(0, 0, 0))))) ? (((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) : ((((int3)(((((int)get_local_id(0)) * 3))+(1*0), ((((int)get_local_id(0)) * 3))+(1*1), ((((int)get_local_id(0)) * 3))+(1*2))) % ((int3)(4, 4, 4))) + ((int3)(4, 4, 4)))) * ((int3)(16, 16, 16)))) + ((int3)(((((int)get_group_id(0)) % 384) / 24), ((((int)get_group_id(0)) % 384) / 24), ((((int)get_group_id(0)) % 384) / 24)));
    vstore3(((float3)(matA[_1.s0],matA[_1.s1],matA[_1.s2])), 0, matA_shared + ((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 768) + (((int)get_local_id(0)) * 3)));
  }
  for (int ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 = 0; ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 < 12; ++ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1) {
    vstore2(vload2(0, matB + (((((((int)get_group_id(0)) % 3) * 49152) + (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 * 4096)) + (((int)get_local_id(0)) * 16)) + (((((int)get_group_id(0)) % 24) / 3) * 2))), 0, matB_shared + ((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 * 512) + (((int)get_local_id(0)) * 2)));
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int g_outer_inner = 0; g_outer_inner < 24; ++g_outer_inner) {
    for (int d_c_outer_inner = 0; d_c_outer_inner < 8; ++d_c_outer_inner) {
      for (int e_c_outer_inner = 0; e_c_outer_inner < 4; ++e_c_outer_inner) {
        tc_abcdef_geab_dfgc_local[(((d_c_outer_inner * 4) + e_c_outer_inner))] = (tc_abcdef_geab_dfgc_local[(((d_c_outer_inner * 4) + e_c_outer_inner))] + (matA_shared[(((((g_outer_inner * 64) + (((((int)get_local_id(0)) & 63) >> 4) * 16)) + (e_c_outer_inner * 4)) + (((int)get_local_id(0)) >> 6)))] * matB_shared[((((d_c_outer_inner * 768) + ((((int)get_local_id(0)) & 15) * 48)) + (g_outer_inner * 2)))]));
        tc_abcdef_geab_dfgc_local[((((d_c_outer_inner * 4) + e_c_outer_inner) + 32))] = (tc_abcdef_geab_dfgc_local[((((d_c_outer_inner * 4) + e_c_outer_inner) + 32))] + (matA_shared[(((((g_outer_inner * 64) + (((((int)get_local_id(0)) & 63) >> 4) * 16)) + (e_c_outer_inner * 4)) + (((int)get_local_id(0)) >> 6)))] * matB_shared[(((((d_c_outer_inner * 768) + ((((int)get_local_id(0)) & 15) * 48)) + (g_outer_inner * 2)) + 1))]));
      }
    }
  }
  for (int d_inner = 0; d_inner < 8; ++d_inner) {
    for (int e_inner = 0; e_inner < 4; ++e_inner) {
      tc_abcdef_geab_dfgc[((((((((((((int)get_group_id(0)) / 384) * 6291456) + ((((int)get_local_id(0)) >> 6) * 1572864)) + (((((int)get_group_id(0)) % 384) / 3) * 12288)) + ((((int)get_group_id(0)) % 3) * 2048)) + (d_inner * 256)) + (((((int)get_local_id(0)) & 63) >> 4) * 64)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)))] = tc_abcdef_geab_dfgc_local[(((d_inner * 4) + e_inner))];
      tc_abcdef_geab_dfgc[(((((((((((((int)get_group_id(0)) / 384) * 6291456) + ((((int)get_local_id(0)) >> 6) * 1572864)) + (((((int)get_group_id(0)) % 384) / 3) * 12288)) + ((((int)get_group_id(0)) % 3) * 2048)) + (d_inner * 256)) + (((((int)get_local_id(0)) & 63) >> 4) * 64)) + (e_inner * 16)) + (((int)get_local_id(0)) & 15)) + 6144))] = tc_abcdef_geab_dfgc_local[((((d_inner * 4) + e_inner) + 32))];
    }
  }
}

