__kernel void tc_abcdef_geac_dfgb_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[(1 * 1) *
                                  (1 * 1) *
                                  (2 * 4) *
                                  (1 * 1) *
                                  (1 * 2) *
                                  (1 * 1)];
    const size_t wg_1 = get_group_id(0) / (1 * 1 * 24 * 2 * 16) % (8); {
    const size_t wg_2 = get_group_id(0) / (1 * 1 * 24 * 2) % (16); {
    const size_t wg_3 = get_group_id(0) / (1 * 1 * 24) % (2); {
    const size_t wg_4 = get_group_id(0) / (1 * 1) % (24); {
    {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_6 = get_local_id(0) % (16); {
    {
    for (size_t glb_1 = 0; glb_1 < 3; ++glb_1) {
    {
    {
    {
    for (size_t glb_5 = 0; glb_5 < 8; ++glb_5) {
    {
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
    {
    for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
    {
        c_prv[(0 * 1 + 0) * (1 * 1) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (lcl_3 * 4 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 4; ++glb_7) {
        for (size_t lcl_7 = 0; lcl_7 < 3; ++lcl_7) {
        {
        {
        for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
        {
        {
        {
            for (size_t prv_7 = 0; prv_7 < 2; ++prv_7) {
            {
            {
            for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
            {
            for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
            {
            c_prv[(0 * 1 + 0) * (1 * 1) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (lcl_3 * 4 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
                +=
            a_glb[(0 * 4 * 1 * 3 * 2 + glb_7 * 1 * 3 * 2 + 0 * 3 * 2 + lcl_7 * 2 + prv_7) * (16) * (24) * (16) + (0 * 8 * 1 * 1 * 2 + glb_5 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_5) * (24) * (16) + (wg_1 * 3 * 1 * 1 * 1 + glb_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (wg_3 * 1 * 1 * 2 * 4 + 0 * 1 * 2 * 4 + 0 * 2 * 4 + lcl_3 * 4 + prv_3) ]
            *
            b_glb[(wg_4 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) + (0 * 1 * 16 * 1 * 1 + 0 * 16 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) * (24) * (16) + (0 * 4 * 1 * 3 * 2 + glb_7 * 1 * 3 * 2 + 0 * 3 * 2 + lcl_7 * 2 + prv_7) * (16) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
            ;
            }}}}}}}
        }}}}}}}
    }
    {
    {
    {
    for (size_t lcl_3 = 0; lcl_3 < 2; ++lcl_3) {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
    {
    for (size_t prv_5 = 0; prv_5 < 2; ++prv_5) {
    {
        c_glb[(wg_1 * 3 * 1 * 1 * 1 + glb_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (24) * (16) * (16) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (16) * (16) + (wg_3 * 1 * 1 * 2 * 4 + 0 * 1 * 2 * 4 + 0 * 2 * 4 + lcl_3 * 4 + prv_3) * (24) * (16) * (16) + (wg_4 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) + (0 * 8 * 1 * 1 * 2 + glb_5 * 1 * 1 * 2 + 0 * 1 * 2 + 0 * 2 + prv_5) * (16) + (0 * 1 * 16 * 1 * 1 + 0 * 16 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) ]
                                               =
        c_prv[(0 * 1 + 0) * (1 * 1) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (2 * 4) * (1 * 1) * (1 * 2) * (1 * 1) + (lcl_3 * 4 + prv_3) * (1 * 1) * (1 * 2) * (1 * 1) + (0 * 1 + 0) * (1 * 2) * (1 * 1) + (0 * 2 + prv_5) * (1 * 1) + (0 * 1 + 0) ]
                           ;
    }}}}}}}}}}}}
    }
    }}}}}}
    }}}}}}}
    }}}}}}}
}
