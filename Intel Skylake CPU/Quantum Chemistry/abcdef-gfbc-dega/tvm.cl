// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gfbc_dega) {
  float tc_abcdef_gfbc_dega_local[48];
  __local float matA_shared[3072];
  __local float matB_shared[4608];
  for (int a_c_inner_init = 0; a_c_inner_init < 2; ++a_c_inner_init) {
    for (int c_c_inner_init = 0; c_c_inner_init < 2; ++c_c_inner_init) {
      tc_abcdef_gfbc_dega_local[(((a_c_inner_init * 8) + (c_c_inner_init * 4)))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 16))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 32))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 1))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 17))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 33))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 2))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 18))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 34))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 3))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 19))] = 0.000000e+00f;
      tc_abcdef_gfbc_dega_local[((((a_c_inner_init * 8) + (c_c_inner_init * 4)) + 35))] = 0.000000e+00f;
    }
  }
  for (int g_outer_outer = 0; g_outer_outer < 2; ++g_outer_outer) {
    barrier(CLK_LOCAL_MEM_FENCE);
    if (((int)get_local_id(0)) < 768) {
      matA_shared[((((int)get_local_id(0)) * 4))] = matA[(((((g_outer_outer * 49152) + ((((int)get_local_id(0)) >> 2) * 256)) + (((((int)get_group_id(0)) & 31) >> 1) * 16)) + ((((int)get_local_id(0)) & 3) * 4)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matA_shared[(((((int)get_local_id(0)) * 4) + 1))] = matA[(((((g_outer_outer * 49152) + ((((((int)get_local_id(0)) * 4) + 1) >> 4) * 256)) + (((((int)get_group_id(0)) & 31) >> 1) * 16)) + (((((int)get_local_id(0)) * 4) + 1) & 15)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matA_shared[(((((int)get_local_id(0)) * 4) + 2))] = matA[(((((g_outer_outer * 49152) + ((((((int)get_local_id(0)) * 4) + 2) >> 4) * 256)) + (((((int)get_group_id(0)) & 31) >> 1) * 16)) + (((((int)get_local_id(0)) * 4) + 2) & 15)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matA_shared[(((((int)get_local_id(0)) * 4) + 3))] = matA[(((((g_outer_outer * 49152) + ((((((int)get_local_id(0)) * 4) + 3) >> 4) * 256)) + (((((int)get_group_id(0)) & 31) >> 1) * 16)) + (((((int)get_local_id(0)) * 4) + 3) & 15)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[((((int)get_local_id(0)) * 6))] = matB[(((((((((int)get_group_id(0)) & 1) * 110592) + ((((int)get_local_id(0)) >> 2) * 576)) + (g_outer_outer * 288)) + ((((int)get_local_id(0)) & 3) * 72)) + ((((int)get_group_id(0)) >> 5) * 2)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[(((((int)get_local_id(0)) * 6) + 1))] = matB[((((((((((int)get_group_id(0)) & 1) * 110592) + ((((int)get_local_id(0)) >> 2) * 576)) + (g_outer_outer * 288)) + ((((int)get_local_id(0)) & 3) * 72)) + ((((int)get_group_id(0)) >> 5) * 2)) + 1))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[(((((int)get_local_id(0)) * 6) + 2))] = matB[(((((((((int)get_group_id(0)) & 1) * 110592) + ((((((int)get_local_id(0)) * 3) + 1) / 12) * 576)) + (g_outer_outer * 288)) + ((((((int)get_local_id(0)) * 3) + 1) % 12) * 24)) + ((((int)get_group_id(0)) >> 5) * 2)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[(((((int)get_local_id(0)) * 6) + 3))] = matB[((((((((((int)get_group_id(0)) & 1) * 110592) + ((((((int)get_local_id(0)) * 3) + 1) / 12) * 576)) + (g_outer_outer * 288)) + ((((((int)get_local_id(0)) * 3) + 1) % 12) * 24)) + ((((int)get_group_id(0)) >> 5) * 2)) + 1))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[(((((int)get_local_id(0)) * 6) + 4))] = matB[(((((((((int)get_group_id(0)) & 1) * 110592) + ((((((int)get_local_id(0)) * 3) + 2) / 12) * 576)) + (g_outer_outer * 288)) + ((((((int)get_local_id(0)) * 3) + 2) % 12) * 24)) + ((((int)get_group_id(0)) >> 5) * 2)))];
    }
    if (((int)get_local_id(0)) < 768) {
      matB_shared[(((((int)get_local_id(0)) * 6) + 5))] = matB[((((((((((int)get_group_id(0)) & 1) * 110592) + ((((((int)get_local_id(0)) * 3) + 2) / 12) * 576)) + (g_outer_outer * 288)) + ((((((int)get_local_id(0)) * 3) + 2) % 12) * 24)) + ((((int)get_group_id(0)) >> 5) * 2)) + 1))];
    }
    barrier(CLK_LOCAL_MEM_FENCE);
    for (int g_inner = 0; g_inner < 12; ++g_inner) {
      for (int a_c_inner = 0; a_c_inner < 2; ++a_c_inner) {
        for (int c_c_inner = 0; c_c_inner < 2; ++c_c_inner) {
          tc_abcdef_gfbc_dega_local[(((a_c_inner * 8) + (c_c_inner * 4)))] = (tc_abcdef_gfbc_dega_local[(((a_c_inner * 8) + (c_c_inner * 4)))] + (matA_shared[(((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner))] * matB_shared[((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 16))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 16))] + (matA_shared[(((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 1536))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 32))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 32))] + (matA_shared[(((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 3072))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 1))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 1))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 16))] * matB_shared[((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 17))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 17))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 16))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 1536))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 33))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 33))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 16))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 3072))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 2))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 2))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 32))] * matB_shared[((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 18))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 18))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 32))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 1536))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 34))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 34))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 32))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 3072))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 3))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 3))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 48))] * matB_shared[((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 19))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 19))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 48))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 1536))]));
          tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 35))] = (tc_abcdef_gfbc_dega_local[((((a_c_inner * 8) + (c_c_inner * 4)) + 35))] + (matA_shared[((((((g_inner * 256) + ((((int)get_local_id(0)) & 3) * 64)) + ((((int)get_local_id(0)) >> 8) * 2)) + c_c_inner) + 48))] * matB_shared[(((((((((int)get_local_id(0)) & 255) >> 2) * 24) + (g_inner * 2)) + a_c_inner) + 3072))]));
        }
      }
    }
  }
  for (int a_inner = 0; a_inner < 2; ++a_inner) {
    for (int c_inner = 0; c_inner < 2; ++c_inner) {
      for (int f_inner = 0; f_inner < 4; ++f_inner) {
        tc_abcdef_gfbc_dega[((((((((((((int)get_group_id(0)) >> 5) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 31) >> 1) * 98304)) + ((((int)get_local_id(0)) >> 8) * 12288)) + (c_inner * 6144)) + ((((int)get_group_id(0)) & 1) * 3072)) + ((((int)get_local_id(0)) & 255) * 4)) + f_inner))] = tc_abcdef_gfbc_dega_local[((((a_inner * 8) + (c_inner * 4)) + f_inner))];
        tc_abcdef_gfbc_dega[(((((((((((((int)get_group_id(0)) >> 5) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 31) >> 1) * 98304)) + ((((int)get_local_id(0)) >> 8) * 12288)) + (c_inner * 6144)) + ((((int)get_group_id(0)) & 1) * 3072)) + ((((int)get_local_id(0)) & 255) * 4)) + f_inner) + 1024))] = tc_abcdef_gfbc_dega_local[(((((a_inner * 8) + (c_inner * 4)) + f_inner) + 16))];
        tc_abcdef_gfbc_dega[(((((((((((((int)get_group_id(0)) >> 5) * 3145728) + (a_inner * 1572864)) + (((((int)get_group_id(0)) & 31) >> 1) * 98304)) + ((((int)get_local_id(0)) >> 8) * 12288)) + (c_inner * 6144)) + ((((int)get_group_id(0)) & 1) * 3072)) + ((((int)get_local_id(0)) & 255) * 4)) + f_inner) + 2048))] = tc_abcdef_gfbc_dega_local[(((((a_inner * 8) + (c_inner * 4)) + f_inner) + 32))];
      }
    }
  }
}

