inline float f(const float a_val, const float b_val) {
  return a_val * b_val;
}
__kernel void tc_abcdefg_gfbc_dega_static_1(__global float const * const restrict a, __global float const * const restrict b, __global float * const restrict res_g, __global float * const restrict int_res) {
  const size_t i_wg_l_1 = (get_group_id(2) % (3 * 24 * 4) / (24 * 4));
  const size_t i_wi_l_1 = (get_local_id(2) % (1 * 1 * 1) / (1 * 1));
  const size_t i_wg_l_2 = (get_group_id(2) % (16 * 3 * 24 * 4) / (3 * 24 * 4));
  const size_t i_wi_l_2 = (get_local_id(2) % (1 * 1 * 1 * 1) / (1 * 1 * 1));
  const size_t i_wg_l_3 = (get_group_id(2) / (16 * 3 * 24 * 4));
  const size_t i_wi_l_3 = (get_local_id(2) / (1 * 1 * 1 * 1));
  const size_t i_wg_l_4 = (get_group_id(2) % (24 * 4) / (4));
  const size_t i_wi_l_4 = (get_local_id(2) % (1 * 1) / (1));
  const size_t i_wg_l_5 = (get_group_id(2) % (4));
  const size_t i_wi_l_5 = (get_local_id(2) % (1));
  const size_t i_wg_l_6 = get_group_id(0);
  const size_t i_wi_l_6 = get_local_id(0);
  const size_t i_wg_r_1 = get_group_id(1);
  const size_t i_wi_r_1 = get_local_id(1);
  __private float res_p[((2 / 1) / (2)) + (((1 * (2 / 1)) % (1 * (2)) / 1) > 0) + (((1 * (2 / 1)) % (1 * (2)) % 1) > 0)][2][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((8 / 1) / (8)) + (((1 * (8 / 1)) % (1 * (8)) / 1) > 0) + (((1 * (8 / 1)) % (1 * (8)) % 1) > 0)][8][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1][1][((16 / 16) / (1)) + (((16 * (16 / 16)) % (16 * (1)) / 16) > 0) + (((16 * (16 / 16)) % (16 * (1)) % 16) > 0)][1];
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((24 / (3 * 1)) / (8 / 1)); ++l_step_l_1) {
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((16 / (16 * 1)) / (1 / 1)); ++l_step_l_2) {
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((16 / (16 * 1)) / (1 / 1)); ++l_step_l_3) {
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((24 / (24 * 1)) / (1 / 1)); ++l_step_l_4) {
          for (size_t l_step_l_5 = 0; l_step_l_5 < ((16 / (4 * 1)) / (2 / 1)); ++l_step_l_5) {
            for (size_t l_step_l_6 = 0; l_step_l_6 < ((16 / (1 * 16)) / (16 / 16)); ++l_step_l_6) {
              size_t l_step_r_1 = 0;
              for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 1) / (8)); ++p_step_l_1) {
                for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                  for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                    for (size_t p_step_l_4 = 0; p_step_l_4 < ((1 / 1) / (1)); ++p_step_l_4) {
                      for (size_t p_step_l_5 = 0; p_step_l_5 < ((2 / 1) / (2)); ++p_step_l_5) {
                        for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                          size_t p_step_r_1 = 0;
                          for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (8); ++p_iteration_l_1) {
                            for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                              for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                                  for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (2); ++p_iteration_l_5) {
                                    for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                      size_t p_iteration_r_1 = 0;
                                      res_p[p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][(0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)] = f(
                                          a[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 1))) * 16 * 16 * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) % 16))) * 16 * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 16 + (((l_step_l_3 * (1 / 1) + (((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 1)))],
                                          b[(((l_step_l_4 * (1 / 1) + (((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (24 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 16 * 24 * 24 + (((l_step_l_5 * (2 / 1) + (((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) / 1) * (4 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) % 1))) * 24 * 24 + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 1))) * 24 + (((l_step_l_1 * (8 / 1) + (((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (3 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))]
                                      );
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              for (l_step_r_1 = 1; l_step_r_1 < ((24 / (1 * 1)) / (1 / 1)); ++l_step_r_1) {
                for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 1) / (8)); ++p_step_l_1) {
                  for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                    for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                      for (size_t p_step_l_4 = 0; p_step_l_4 < ((1 / 1) / (1)); ++p_step_l_4) {
                        for (size_t p_step_l_5 = 0; p_step_l_5 < ((2 / 1) / (2)); ++p_step_l_5) {
                          for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                            size_t p_step_r_1 = 0;
                            for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (8); ++p_iteration_l_1) {
                              for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                                for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3) {
                                  for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4) {
                                    for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (2); ++p_iteration_l_5) {
                                      for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6) {
                                        for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                                          res_p[p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][(0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)] += f(
                                              a[(((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 1))) * 16 * 16 * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) % 16))) * 16 * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 16 + (((l_step_l_3 * (1 / 1) + (((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 1)))],
                                              b[(((l_step_l_4 * (1 / 1) + (((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (24 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 16 * 24 * 24 + (((l_step_l_5 * (2 / 1) + (((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) / 1) * (4 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) % 1))) * 24 * 24 + (((l_step_r_1 * (1 / 1) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_r_1 * 1 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 1 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 1))) * 24 + (((l_step_l_1 * (8 / 1) + (((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (3 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))]
                                          );
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              {
                if (i_wi_r_1 == 0) {
                  for (size_t p_step_l_1 = 0; p_step_l_1 < ((8 / 1) / (8)); ++p_step_l_1) {
                    for (size_t p_step_l_2 = 0; p_step_l_2 < ((1 / 1) / (1)); ++p_step_l_2) {
                      for (size_t p_step_l_3 = 0; p_step_l_3 < ((1 / 1) / (1)); ++p_step_l_3) {
                        for (size_t p_step_l_4 = 0; p_step_l_4 < ((1 / 1) / (1)); ++p_step_l_4) {
                          for (size_t p_step_l_5 = 0; p_step_l_5 < ((2 / 1) / (2)); ++p_step_l_5) {
                            for (size_t p_step_l_6 = 0; p_step_l_6 < ((16 / 16) / (1)); ++p_step_l_6) {
                              for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (8); ++p_iteration_l_1)
                                for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2)
                                  for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (1); ++p_iteration_l_3)
                                    for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (1); ++p_iteration_l_4)
                                      for (size_t p_iteration_l_5 = 0; p_iteration_l_5 < (2); ++p_iteration_l_5)
                                        for (size_t p_iteration_l_6 = 0; p_iteration_l_6 < (1); ++p_iteration_l_6)
                                          int_res[(((l_step_l_1 * (8 / 1) + (((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (3 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (8) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1))) * 16 * 16 * 24 * 16 * 16 + (((l_step_l_2 * (1 / 1) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_2 * 1 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 1 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 1))) * 16 * 24 * 16 * 16 + (((l_step_l_3 * (1 / 1) + (((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 1) * (16 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (1) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 1))) * 24 * 16 * 16 + (((l_step_l_4 * (1 / 1) + (((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (24 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (1) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 16 * 16 + (((l_step_l_5 * (2 / 1) + (((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) / 1) * (4 * 1) + i_wg_l_5 * 1 + ((((p_step_l_5 * (2) + (((p_iteration_l_5) * 1 + 0)) / 1) * 1 + i_wi_l_5 * 1 + ((((p_iteration_l_5) * 1 + 0)) % 1))) % 1))) * 16 + (((l_step_l_6 * (16 / 16) + (((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) / 16) * (1 * 16) + i_wg_l_6 * 16 + ((((p_step_l_6 * (1) + (((p_iteration_l_6) * 1 + 0)) / 1) * 16 + i_wi_l_6 * 1 + ((((p_iteration_l_6) * 1 + 0)) % 1))) % 16)))] = res_p[p_step_l_5][((p_iteration_l_5) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_3][((p_iteration_l_3) * 1 + 0)][(0)][p_step_l_6][((p_iteration_l_6) * 1 + 0)];
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
