// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gdbc_efga) {
  float tc_abcdef_gdbc_efga_local[256];
  __local float matA_shared[1536];
  __local float matB_shared[1152];
  for (int c_c_outer_inner_init = 0; c_c_outer_inner_init < 4; ++c_c_outer_inner_init) {
    tc_abcdef_gdbc_efga_local[((c_c_outer_inner_init * 64))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 1))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 2))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 3))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 16))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 17))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 18))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 19))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 32))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 33))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 34))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 35))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 48))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 49))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 50))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 51))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 4))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 5))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 6))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 7))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 20))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 21))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 22))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 23))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 36))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 37))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 38))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 39))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 52))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 53))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 54))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 55))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 8))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 9))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 10))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 11))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 24))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 25))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 26))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 27))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 40))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 41))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 42))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 43))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 56))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 57))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 58))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 59))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 12))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 13))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 14))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 15))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 28))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 29))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 30))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 31))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 44))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 45))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 46))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 47))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 60))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 61))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 62))] = 0.000000e+00f;
    tc_abcdef_gdbc_efga_local[(((c_c_outer_inner_init * 64) + 63))] = 0.000000e+00f;
  }
  vstore4(vload4(0, matA + ((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4))), 0, matA_shared + (((int)get_local_id(0)) * 4));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 3072)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 48));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 6144)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 96));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 9216)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 144));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 12288)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 192));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 15360)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 240));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 18432)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 288));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 21504)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 336));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 24576)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 384));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 27648)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 432));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 30720)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 480));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 33792)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 528));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 36864)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 576));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 39936)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 624));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 43008)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 672));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 46080)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 720));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 49152)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 768));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 52224)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 816));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 55296)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 864));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 58368)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 912));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 61440)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 960));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 64512)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1008));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 67584)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1056));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 70656)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1104));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 73728)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1152));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 76800)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1200));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 79872)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1248));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 82944)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1296));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 86016)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1344));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 89088)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1392));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 92160)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1440));
  vstore4(vload4(0, matA + (((((int)get_local_id(0)) * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4)) + 95232)), 0, matA_shared + ((((int)get_local_id(0)) * 4) + 1488));
  vstore3(vload3(0, matB + ((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3))), 0, matB_shared + (((int)get_local_id(0)) * 3));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 288)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 36));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 576)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 72));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 864)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 108));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 1152)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 144));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 1440)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 180));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 1728)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 216));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 2016)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 252));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 2304)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 288));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 2592)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 324));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 2880)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 360));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 3168)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 396));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 3456)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 432));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 3744)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 468));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 4032)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 504));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 4320)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 540));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 4608)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 576));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 4896)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 612));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 5184)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 648));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 5472)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 684));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 5760)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 720));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 6048)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 756));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 6336)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 792));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 6624)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 828));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 6912)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 864));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 7200)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 900));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 7488)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 936));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 7776)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 972));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 8064)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 1008));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 8352)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 1044));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 8640)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 1080));
  vstore3(vload3(0, matB + (((((((int)get_group_id(0)) % 24) * 9216) + (((int)get_local_id(0)) * 24)) + ((((int)get_group_id(0)) / 1536) * 3)) + 8928)), 0, matB_shared + ((((int)get_local_id(0)) * 3) + 1116));
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int c_c_outer_inner = 0; c_c_outer_inner < 4; ++c_c_outer_inner) {
    for (int f_c_outer_inner = 0; f_c_outer_inner < 4; ++f_c_outer_inner) {
      for (int g_inner = 0; g_inner < 24; ++g_inner) {
        tc_abcdef_gdbc_efga_local[(((c_c_outer_inner * 64) + (f_c_outer_inner * 4)))] = (tc_abcdef_gdbc_efga_local[(((c_c_outer_inner * 64) + (f_c_outer_inner * 4)))] + (matA_shared[((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 1))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 1))] + (matA_shared[((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 72))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 2))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 2))] + (matA_shared[((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 144))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 3))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 3))] + (matA_shared[((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 216))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 16))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 16))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 17))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 17))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 4))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 72))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 18))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 18))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 4))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 144))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 19))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 19))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 4))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 216))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 32))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 32))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 8))] * matB_shared[((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 33))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 33))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 8))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 72))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 34))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 34))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 8))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 144))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 35))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 35))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 8))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 216))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 48))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 48))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 12))] * matB_shared[((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 49))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 49))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 12))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 72))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 50))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 50))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 12))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 144))]));
        tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 51))] = (tc_abcdef_gdbc_efga_local[((((c_c_outer_inner * 64) + (f_c_outer_inner * 4)) + 51))] + (matA_shared[(((((g_inner * 64) + ((((int)get_local_id(0)) & 3) * 16)) + c_c_outer_inner) + 12))] * matB_shared[(((((f_c_outer_inner * 288) + (g_inner * 3)) + (((int)get_local_id(0)) >> 2)) + 216))]));
      }
    }
  }
  for (int c_inner = 0; c_inner < 4; ++c_inner) {
    for (int d_inner = 0; d_inner < 4; ++d_inner) {
      for (int f_inner = 0; f_inner < 16; ++f_inner) {
        tc_abcdef_gdbc_efga[((((((((((((int)get_group_id(0)) / 1536) * 4718592) + ((((int)get_local_id(0)) >> 2) * 1572864)) + (((((int)get_group_id(0)) % 1536) / 24) * 24576)) + (c_inner * 6144)) + ((((int)get_local_id(0)) & 3) * 1536)) + (d_inner * 384)) + ((((int)get_group_id(0)) % 24) * 16)) + f_inner))] = tc_abcdef_gdbc_efga_local[((((c_inner * 64) + (d_inner * 16)) + f_inner))];
      }
    }
  }
}

