__kernel void tc_abcdef_gdbc_efga_static_1(
        __global float const * const __restrict__ a_glb,
        __global float const * const __restrict__ b_glb,
        __global float * const __restrict__ _,
        __global float * const __restrict__ c_glb) {
    __private float c_prv[(1 * 1) *
                                  (1 * 1) *
                                  (1 * 4) *
                                  (1 * 4) *
                                  (1 * 1) *
                                  (1 * 1)];
    __private float a_prv[(1) *
                                  (4) *
                                  (1) *
                                  (4)];
    __private float b_prv[(1) *
                                  (1) *
                                  (1) *
                                  (1)];
    const size_t wg_1 = get_group_id(0) / (1 * 24 * 4 * 1 * 16) % (12); {
    const size_t wg_2 = get_group_id(0) / (1 * 24 * 4 * 1) % (16); {
    {
    const size_t wg_4 = get_group_id(0) / (1 * 24) % (4); {
    const size_t wg_5 = get_group_id(0) / (1) % (24); {
    {
    {
    {
    {
    {
    {
    {
    const size_t wi_6 = get_local_id(0) % (16); {
    {
    for (size_t glb_1 = 0; glb_1 < 2; ++glb_1) {
    {
    for (size_t glb_3 = 0; glb_3 < 4; ++glb_3) {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
    for (size_t prv_4 = 0; prv_4 < 4; ++prv_4) {
    {
    {
        c_prv[(0 * 1 + 0) * (1 * 1) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_3) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
            = 0.0f;
    }}}}}}}}}}}}
    }
    for (size_t glb_7 = 0; glb_7 < 24; ++glb_7) {
        {
        {
        {
        {
        {
        {
        {
            {
               
                for (size_t prv_3 = 0; prv_3 < 4; ++prv_3)
                for (size_t prv_4 = 0; prv_4 < 4; ++prv_4)
               
                    a_prv[(0) * (4) * (1) * (4) + (prv_4) * (1) * (4) + (0) * (4) + (prv_3) ]
                                 =
                    a_glb[(0 * 24 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (16) + (wg_4 * 1 * 1 * 1 * 4 + 0 * 1 * 1 * 4 + 0 * 1 * 4 + 0 * 4 + prv_4) * (16) * (16) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (0 * 4 * 1 * 1 * 4 + glb_3 * 1 * 1 * 4 + 0 * 1 * 4 + 0 * 4 + prv_3) ]
                        ;
            }
            {
               
               
               
               
                    b_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
                                 =
                    b_glb[(wg_5 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (24) * (24) + (0 * 1 * 16 * 1 * 1 + 0 * 16 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) * (24) * (24) + (0 * 24 * 1 * 1 * 1 + glb_7 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (24) + (wg_1 * 2 * 1 * 1 * 1 + glb_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) ]
                        ;
            }
            {
            {
            {
            for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
            for (size_t prv_4 = 0; prv_4 < 4; ++prv_4) {
            {
            {
            c_prv[(0 * 1 + 0) * (1 * 1) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_3) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
                +=
            a_prv[(0) * (4) * (1) * (4) + (prv_4) * (1) * (4) + (0) * (4) + (prv_3) ]
            *
            b_prv[(0) * (1) * (1) * (1) + (0) * (1) * (1) + (0) * (1) + (0) ]
            ;
            }}}}}}}
        }}}}}}}
    }
    {
    {
    {
    {
    {
    {
    {
    {
    {
    for (size_t prv_3 = 0; prv_3 < 4; ++prv_3) {
    for (size_t prv_4 = 0; prv_4 < 4; ++prv_4) {
    {
    {
        c_glb[(wg_1 * 2 * 1 * 1 * 1 + glb_1 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (16) * (24) * (16) + (wg_2 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) * (16) * (24) * (16) + (0 * 4 * 1 * 1 * 4 + glb_3 * 1 * 1 * 4 + 0 * 1 * 4 + 0 * 4 + prv_3) * (16) * (24) * (16) + (wg_4 * 1 * 1 * 1 * 4 + 0 * 1 * 1 * 4 + 0 * 1 * 4 + 0 * 4 + prv_4) * (24) * (16) + (wg_5 * 1 * 1 * 1 * 1 + 0 * 1 * 1 * 1 + 0 * 1 * 1 + 0 * 1 + 0) * (16) + (0 * 1 * 16 * 1 * 1 + 0 * 16 * 1 * 1 + wi_6 * 1 * 1 + 0 * 1 + 0) ]
                                               =
        c_prv[(0 * 1 + 0) * (1 * 1) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 4) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_3) * (1 * 4) * (1 * 1) * (1 * 1) + (0 * 4 + prv_4) * (1 * 1) * (1 * 1) + (0 * 1 + 0) * (1 * 1) + (0 * 1 + 0) ]
                           ;
    }}}}}}}}}}}}
    }
    }}}}}}
    }}}}}}}
    }}}}}}}
}
