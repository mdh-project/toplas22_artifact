// Function: default_function_kernel0
__kernel void default_function_kernel0(__global float* restrict matA, __global float* restrict matB, __global float* restrict tc_abcdef_gebc_dfga) {
  float tc_abcdef_gebc_dfga_local[4096];
  __local float matA_shared[1536];
  __local float matB_shared[1536];
  for (int c_c_outer_inner_init = 0; c_c_outer_inner_init < 4; ++c_c_outer_inner_init) {
    for (int e_c_outer_inner_init = 0; e_c_outer_inner_init < 8; ++e_c_outer_inner_init) {
      for (int f_c_outer_inner_init = 0; f_c_outer_inner_init < 8; ++f_c_outer_inner_init) {
        tc_abcdef_gebc_dfga_local[((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 1))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 16))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 17))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 1024))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 1025))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 1040))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 1041))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 2048))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 2049))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 2064))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 2065))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 3072))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 3073))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 3088))] = 0.000000e+00f;
        tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner_init * 256) + (e_c_outer_inner_init * 32)) + (f_c_outer_inner_init * 2)) + 3089))] = 0.000000e+00f;
      }
    }
  }
  for (int ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer = 0; ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer < 384; ++ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer) {
    vstore4(vload4(0, matA + ((ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 256) + (((((int)get_group_id(0)) % 1536) / 24) * 4))), 0, matA_shared + (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer * 4));
  }
  for (int ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 = 0; ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 < 384; ++ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1) {
    vstore4(vload4(0, matB + ((((((int)get_group_id(0)) % 24) * 9216) + (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 * 24)) + ((((int)get_group_id(0)) / 1536) * 4))), 0, matB_shared + (ax0_ax1_fused_ax2_fused_ax3_fused_outer_outer1 * 4));
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  for (int c_c_outer_inner = 0; c_c_outer_inner < 4; ++c_c_outer_inner) {
    for (int e_c_outer_inner = 0; e_c_outer_inner < 8; ++e_c_outer_inner) {
      for (int f_c_outer_inner = 0; f_c_outer_inner < 8; ++f_c_outer_inner) {
        for (int g_inner = 0; g_inner < 24; ++g_inner) {
          tc_abcdef_gebc_dfga_local[((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)))] = (tc_abcdef_gebc_dfga_local[((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[(((f_c_outer_inner * 192) + (g_inner * 4)))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 96))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 16))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 16))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[(((f_c_outer_inner * 192) + (g_inner * 4)))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 17))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 17))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 96))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1024))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1024))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 1))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1025))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1025))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 97))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1040))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1040))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 1))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1041))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 1041))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 97))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2048))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2048))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 2))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2049))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2049))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 98))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2064))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2064))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 2))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2065))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 2065))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 98))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3072))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3072))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 3))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3073))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3073))] + (matA_shared[((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 99))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3088))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3088))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 3))]));
          tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3089))] = (tc_abcdef_gebc_dfga_local[(((((c_c_outer_inner * 256) + (e_c_outer_inner * 32)) + (f_c_outer_inner * 2)) + 3089))] + (matA_shared[(((((g_inner * 64) + (e_c_outer_inner * 8)) + c_c_outer_inner) + 4))] * matB_shared[((((f_c_outer_inner * 192) + (g_inner * 4)) + 99))]));
        }
      }
    }
  }
  for (int a_inner = 0; a_inner < 4; ++a_inner) {
    for (int c_inner = 0; c_inner < 4; ++c_inner) {
      for (int e_inner = 0; e_inner < 16; ++e_inner) {
        for (int f_inner = 0; f_inner < 16; ++f_inner) {
          tc_abcdef_gebc_dfga[(((((((((((int)get_group_id(0)) / 1536) * 6291456) + (a_inner * 1572864)) + (((((int)get_group_id(0)) % 1536) / 24) * 24576)) + (c_inner * 6144)) + ((((int)get_group_id(0)) % 24) * 256)) + (e_inner * 16)) + f_inner))] = tc_abcdef_gebc_dfga_local[(((((a_inner * 1024) + (c_inner * 256)) + (e_inner * 16)) + f_inner))];
        }
      }
    }
  }
}

