#include "tc_abcdef_gfbc_dega_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_B[1][2][24][1];
    __shared__ float shared_C[1][2][16][1][2][4];

    for (int c3 = 0; c3 <= 23; c3 += 1)
      for (int c4 = 0; c4 <= 15; c4 += 2) {
        if (t2 == 0)
          for (int c6 = 0; c6 <= 1; c6 += 1)
            for (int c7 = t1; c7 <= 23; c7 += 2)
              shared_B[0][c6][c7][0] = B[((c3 * 16 + (c4 + c6)) * 24 + c7) * 24 + b0];
        __syncthreads();
        for (int c5 = 0; c5 <= 15; c5 += 4) {
          if (t2 <= 3)
            for (int c7 = 0; c7 <= 1; c7 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                shared_C[0][c7][c8][0][t1][t2] = C[((((b0 * 16 + (2 * b1 + c7)) * 16 + c8) * 24 + c3) * 16 + (t1 + c4)) * 16 + (t2 + c5)];
          __syncthreads();
          for (int c11 = 0; c11 <= 1; c11 += 1)
            for (int c12 = 0; c12 <= 3; c12 += 1)
              for (int c13 = 0; c13 <= 23; c13 += 1)
                shared_C[0][t1][t2][0][c11][c12] += (A[((c13 * 16 + (c5 + c12)) * 16 + (2 * b1 + t1)) * 16 + t2] * shared_B[0][c11][c13][0]);
          __syncthreads();
          if (t2 <= 3)
            for (int c7 = 0; c7 <= 1; c7 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                C[((((b0 * 16 + (2 * b1 + c7)) * 16 + c8) * 24 + c3) * 16 + (t1 + c4)) * 16 + (t2 + c5)] = shared_C[0][c7][c8][0][t1][t2];
        }
        __syncthreads();
      }
}
