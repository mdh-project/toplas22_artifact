#include "tc_abcdef_gdab_efgc_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[24][2][2][1];
    __shared__ float shared_C[2][1][16][2][2][4];

    for (int c3 = 0; c3 <= 15; c3 += 2) {
      if (t2 == 0)
        for (int c4 = 0; c4 <= 23; c4 += 1)
          for (int c6 = 0; c6 <= 1; c6 += 1)
            shared_A[c4][t0][c6][0] = A[((c4 * 16 + (t0 + c3)) * 24 + (2 * b0 + c6)) * 16 + b1];
      __syncthreads();
      for (int c4 = 0; c4 <= 23; c4 += 2)
        for (int c5 = 0; c5 <= 15; c5 += 4) {
          if (t2 <= 3)
            for (int c6 = 0; c6 <= 1; c6 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                for (int c10 = 0; c10 <= 1; c10 += 1)
                  shared_C[c6][0][c8][t0][c10][t2] = C[(((((2 * b0 + c6) * 16 + b1) * 16 + c8) * 16 + (t0 + c3)) * 24 + (c4 + c10)) * 16 + (t2 + c5)];
          __syncthreads();
          for (int c10 = 0; c10 <= 1; c10 += 1)
            for (int c11 = 0; c11 <= 1; c11 += 1)
              for (int c12 = 0; c12 <= 3; c12 += 1)
                for (int c13 = 0; c13 <= 23; c13 += 1)
                  shared_C[t0][0][t2][c10][c11][c12] += (shared_A[c13][c10][t0][0] * B[(((c4 + c11) * 16 + (c5 + c12)) * 24 + c13) * 16 + t2]);
          __syncthreads();
          if (t2 <= 3)
            for (int c6 = 0; c6 <= 1; c6 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                for (int c10 = 0; c10 <= 1; c10 += 1)
                  C[(((((2 * b0 + c6) * 16 + b1) * 16 + c8) * 16 + (t0 + c3)) * 24 + (c4 + c10)) * 16 + (t2 + c5)] = shared_C[c6][0][c8][t0][c10][t2];
        }
      __syncthreads();
    }
}
