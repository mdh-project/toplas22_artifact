#include "tc_abcdef_gfab_degc_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[24][16][2][1];
    __shared__ float shared_B[1][1][24][16];
    __shared__ float shared_C[2][1][16][1][1][16];

    {
      if (t2 == 0)
        for (int c0 = 0; c0 <= 23; c0 += 1)
          for (int c1 = t0; c1 <= 15; c1 += 2)
            for (int c2 = 0; c2 <= 1; c2 += 1)
              shared_A[c0][c1][c2][0] = A[((c0 * 16 + c1) * 24 + (2 * b0 + c2)) * 16 + b1];
      __syncthreads();
      for (int c3 = 0; c3 <= 23; c3 += 1)
        for (int c4 = 0; c4 <= 15; c4 += 1) {
          if (t0 == 0) {
            for (int c7 = 0; c7 <= 23; c7 += 1)
              shared_B[0][0][c7][t2] = B[((c3 * 16 + c4) * 24 + c7) * 16 + t2];
            for (int c5 = 0; c5 <= 1; c5 += 1)
              for (int c7 = 0; c7 <= 15; c7 += 1)
                shared_C[c5][0][c7][0][0][t2] = C[(((((2 * b0 + c5) * 16 + b1) * 16 + c7) * 24 + c3) * 16 + c4) * 16 + t2];
          }
          __syncthreads();
          for (int c12 = 0; c12 <= 15; c12 += 1)
            for (int c13 = 0; c13 <= 23; c13 += 1)
              shared_C[t0][0][t2][0][0][c12] += (shared_A[c13][c12][t0][0] * shared_B[0][0][c13][t2]);
          __syncthreads();
          if (t0 == 0)
            for (int c5 = 0; c5 <= 1; c5 += 1)
              for (int c7 = 0; c7 <= 15; c7 += 1)
                C[(((((2 * b0 + c5) * 16 + b1) * 16 + c7) * 24 + c3) * 16 + c4) * 16 + t2] = shared_C[c5][0][c7][0][0][t2];
        }
    }
}
