#include "tc_abcdef_gdbc_efga_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    for (int c8 = t1; c8 <= 15; c8 += 4)
      for (int c9 = t2; c9 <= 15; c9 += 4)
        for (int c10 = 0; c10 <= 15; c10 += 1)
          for (int c11 = 0; c11 <= 23; c11 += 1)
            for (int c12 = 0; c12 <= 15; c12 += 1)
              for (int c13 = 0; c13 <= 23; c13 += 1)
                C[((((t0 * 16 + c8) * 16 + c9) * 16 + c10) * 24 + c11) * 16 + c12] += (A[((c13 * 16 + c10) * 16 + c8) * 16 + c9] * B[((c11 * 16 + c12) * 24 + c13) * 24 + t0]);
}
