#include "tc_abcdef_gdbc_efga_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[24][2][1][16];
    __shared__ float shared_B[1][10][24][2];
    __shared__ float shared_C[2][1][16][2][1][10];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c3 = 0; c3 <= 15; c3 += 2) {
      for (int c4 = 0; c4 <= 23; c4 += 1)
        shared_A[c4][t0][0][t2] = A[((c4 * 16 + (t0 + c3)) * 16 + b1) * 16 + t2];
      __syncthreads();
      for (int c4 = 0; c4 <= 23; c4 += 1)
        for (int c5 = 0; c5 <= 15; c5 += 10) {
          if (t2 <= 1)
            for (int c7 = t0; c7 <= ppcg_min(9, -c5 + 15); c7 += 2)
              for (int c8 = 0; c8 <= 23; c8 += 1)
                shared_B[0][c7][c8][t2] = B[((c4 * 16 + (c5 + c7)) * 24 + c8) * 24 + (2 * b0 + t2)];
          if (t2 <= 9 && t2 + c5 <= 15)
            for (int c6 = 0; c6 <= 1; c6 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                shared_C[c6][0][c8][t0][0][t2] = C[(((((2 * b0 + c6) * 16 + b1) * 16 + c8) * 16 + (t0 + c3)) * 24 + c4) * 16 + (t2 + c5)];
          __syncthreads();
          for (int c10 = 0; c10 <= 1; c10 += 1)
            for (int c12 = 0; c12 <= ppcg_min(9, -c5 + 15); c12 += 1)
              for (int c13 = 0; c13 <= 23; c13 += 1)
                shared_C[t0][0][t2][c10][0][c12] += (shared_A[c13][c10][0][t2] * shared_B[0][c12][c13][t0]);
          __syncthreads();
          if (t2 <= 9 && t2 + c5 <= 15)
            for (int c6 = 0; c6 <= 1; c6 += 1)
              for (int c8 = 0; c8 <= 15; c8 += 1)
                C[(((((2 * b0 + c6) * 16 + b1) * 16 + c8) * 16 + (t0 + c3)) * 24 + c4) * 16 + (t2 + c5)] = shared_C[c6][0][c8][t0][0][t2];
        }
      __syncthreads();
    }
}
