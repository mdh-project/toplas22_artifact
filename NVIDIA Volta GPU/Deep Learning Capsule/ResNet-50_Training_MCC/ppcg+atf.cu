#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2_asymmetrical_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_in[3][9][9][1][1][4];
    __shared__ float shared_out[3][2][2][32][1][2];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 111; c2 += 2)
      for (int c3 = 0; c3 <= 63; c3 += 32)
        for (int c4 = 0; c4 <= 3; c4 += 1)
          for (int c5 = 0; c5 <= 3; c5 += 2) {
            if (t1 == 0)
              for (int c6 = 0; c6 <= ppcg_min(2, -3 * b0 + 15); c6 += 1)
                for (int c7 = 0; c7 <= 1; c7 += 1)
                  for (int c8 = 0; c8 <= 1; c8 += 1)
                    for (int c9 = t0; c9 <= 31; c9 += 3)
                      shared_out[c6][c7][c8][c9][0][t2] = out[(((((3 * b0 + c6) * 112 + (2 * b1 + c7)) * 112 + (c2 + c8)) * 64 + (c3 + c9)) * 4 + c4) * 4 + (t2 + c5)];
            for (int c6 = 0; c6 <= 2; c6 += 1) {
              if (t0 == 0 && t1 == 0)
                for (int c7 = 0; c7 <= ppcg_min(2, -3 * b0 + 15); c7 += 1)
                  for (int c8 = 0; c8 <= 8; c8 += 1)
                    for (int c9 = 0; c9 <= 8; c9 += 1)
                      for (int c12 = t2; c12 <= 3; c12 += 2)
                        shared_in[c7][c8][c9][0][0][c12] = in[(((((3 * b0 + c7) * 230 + (4 * b1 + c8)) * 230 + (2 * c2 + c9)) * 3 + c6) * 4 + c4) * 4 + c12];
              __syncthreads();
              if (3 * b0 + t0 <= 15)
                for (int c10 = 0; c10 <= 31; c10 += 1)
                  for (int c12 = 0; c12 <= 1; c12 += 1)
                    for (int c14 = 0; c14 <= 6; c14 += 1)
                      for (int c15 = 0; c15 <= 6; c15 += 1)
                        for (int c16 = 0; c16 <= 3; c16 += 1)
                          shared_out[t0][t1][t2][c10][0][c12] += (filter[(((((c3 + c10) * 7 + c14) * 7 + c15) * 3 + c6) * 4 + c16) * 4 + (c5 + c12)] * shared_in[t0][2 * t1 + c14][2 * t2 + c15][0][0][c16]);
              __syncthreads();
            }
            if (t1 == 0)
              for (int c6 = 0; c6 <= ppcg_min(2, -3 * b0 + 15); c6 += 1)
                for (int c7 = 0; c7 <= 1; c7 += 1)
                  for (int c8 = 0; c8 <= 1; c8 += 1)
                    for (int c9 = t0; c9 <= 31; c9 += 3)
                      out[(((((3 * b0 + c6) * 112 + (2 * b1 + c7)) * 112 + (c2 + c8)) * 64 + (c3 + c9)) * 4 + c4) * 4 + (t2 + c5)] = shared_out[c6][c7][c8][c9][0][t2];
            __syncthreads();
          }
}
