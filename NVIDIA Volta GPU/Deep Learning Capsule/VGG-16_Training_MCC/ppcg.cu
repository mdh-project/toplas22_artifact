#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    for (int c2 = 0; c2 <= 223; c2 += 32)
      for (int c3 = 0; c3 <= 63; c3 += 32)
        for (int c8 = t1; c8 <= 31; c8 += 4)
          for (int c9 = t2; c9 <= 31; c9 += 4)
            for (int c10 = 0; c10 <= 31; c10 += 1)
              for (int c11 = 0; c11 <= 3; c11 += 1)
                for (int c12 = 0; c12 <= 3; c12 += 1)
                  for (int c13 = 0; c13 <= 2; c13 += 1)
                    for (int c14 = 0; c14 <= 2; c14 += 1)
                      for (int c15 = 0; c15 <= 2; c15 += 1)
                        for (int c16 = 0; c16 <= 3; c16 += 1)
                          out[((((t0 * 224 + (32 * b1 + c8)) * 224 + (c2 + c9)) * 64 + (c3 + c10)) * 4 + c11) * 4 + c12] += (filter[(((((c3 + c10) * 3 + c14) * 3 + c15) * 3 + c13) * 4 + c16) * 4 + c12] * in[((((t0 * 226 + (32 * b1 + c8 + c14)) * 226 + (c2 + c9 + c15)) * 3 + c13) * 4 + c11) * 4 + c16]);
}
