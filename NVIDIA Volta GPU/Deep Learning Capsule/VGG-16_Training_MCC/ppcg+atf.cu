#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_filter[4][3][3][1][4][4];
    __shared__ float shared_in[2][5][10][1][3][4];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 223; c2 += 8)
      for (int c3 = 0; c3 <= 63; c3 += 4)
        for (int c4 = 0; c4 <= 3; c4 += 3)
          for (int c6 = 0; c6 <= 2; c6 += 1) {
            if (t2 <= 3) {
              for (int c7 = 0; c7 <= 3; c7 += 1)
                for (int c8 = 0; c8 <= 2; c8 += 1)
                  for (int c9 = 0; c9 <= 2; c9 += 1)
                    for (int c11 = t1; c11 <= 3; c11 += 3)
                      shared_filter[c7][c8][c9][0][c11][t2] = filter[(((((c3 + c7) * 3 + c8) * 3 + c9) * 3 + c6) * 4 + c11) * 4 + t2];
              if (t1 + c4 <= 3)
                for (int c7 = 0; c7 <= 1; c7 += 1)
                  for (int c8 = 0; c8 <= ppcg_min(4, -3 * b1 + 225); c8 += 1)
                    for (int c9 = 0; c9 <= 9; c9 += 1)
                      shared_in[c7][c8][c9][0][t1][t2] = in[(((((2 * b0 + c7) * 226 + (3 * b1 + c8)) * 226 + (c2 + c9)) * 3 + c6) * 4 + (t1 + c4)) * 4 + t2];
            }
            __syncthreads();
            if (3 * b1 + t1 <= 223)
              for (int c7 = 0; c7 <= 1; c7 += 1)
                for (int c10 = 0; c10 <= 3; c10 += 1)
                  for (int c11 = 0; c11 <= ppcg_min(2, -c4 + 3); c11 += 1)
                    for (int c12 = 0; c12 <= 3; c12 += 1)
                      for (int c14 = 0; c14 <= 2; c14 += 1)
                        for (int c15 = 0; c15 <= 2; c15 += 1)
                          for (int c16 = 0; c16 <= 3; c16 += 1)
                            out[(((((2 * b0 + c7) * 224 + (3 * b1 + t1)) * 224 + (t2 + c2)) * 64 + (c3 + c10)) * 4 + (c4 + c11)) * 4 + c12] += (shared_filter[c10][c14][c15][0][c16][c12] * shared_in[c7][t1 + c14][t2 + c15][0][c11][c16]);
            __syncthreads();
          }
}
