#include "mcc_capsule_nhwccmck_krscckcn_npqkcmcn_stride_2_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_filter[4][3][3][1][4][4];
    __shared__ float shared_out[1][2][44][4][1][4];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c0 = b0; c0 <= 15; c0 += 8)
      for (int c2 = 0; c2 <= 111; c2 += 44)
        for (int c3 = 0; c3 <= 31; c3 += 4)
          for (int c4 = 0; c4 <= 3; c4 += 1) {
            if (t2 <= 3)
              for (int c6 = 0; c6 <= 1; c6 += 1)
                for (int c7 = 0; c7 <= ppcg_min(43, -c2 + 111); c7 += 1)
                  for (int c8 = 0; c8 <= 3; c8 += 1)
                    shared_out[0][c6][c7][c8][0][t2] = out[((((c0 * 112 + (2 * b1 + c6)) * 112 + (c2 + c7)) * 32 + (c3 + c8)) * 4 + c4) * 4 + t2];
            for (int c6 = 0; c6 <= 2; c6 += 1) {
              if (t2 <= 3)
                for (int c7 = 0; c7 <= 3; c7 += 1)
                  for (int c8 = 0; c8 <= 2; c8 += 1)
                    for (int c9 = 0; c9 <= 2; c9 += 1)
                      for (int c11 = 0; c11 <= 3; c11 += 1)
                        shared_filter[c7][c8][c9][0][c11][t2] = filter[(((((c3 + c7) * 3 + c8) * 3 + c9) * 3 + c6) * 4 + c11) * 4 + t2];
              __syncthreads();
              for (int c8 = 0; c8 <= 1; c8 += 1)
                for (int c9 = t2; c9 <= ppcg_min(43, -c2 + 111); c9 += 26)
                  for (int c10 = 0; c10 <= 3; c10 += 1)
                    for (int c12 = 0; c12 <= 3; c12 += 1)
                      for (int c14 = 0; c14 <= 2; c14 += 1)
                        for (int c15 = 0; c15 <= 2; c15 += 1)
                          for (int c16 = 0; c16 <= 3; c16 += 1)
                            shared_out[0][c8][c9][c10][0][c12] += (shared_filter[c10][c14][c15][0][c16][c12] * in[((((c0 * 225 + (4 * b1 + 2 * c8 + c14)) * 225 + (2 * c2 + 2 * c9 + c15)) * 3 + c6) * 4 + c4) * 4 + c16]);
              __syncthreads();
            }
            if (t2 <= 3)
              for (int c6 = 0; c6 <= 1; c6 += 1)
                for (int c7 = 0; c7 <= ppcg_min(43, -c2 + 111); c7 += 1)
                  for (int c8 = 0; c8 <= 3; c8 += 1)
                    out[((((c0 * 112 + (2 * b1 + c6)) * 112 + (c2 + c7)) * 32 + (c3 + c8)) * 4 + c4) * 4 + t2] = shared_out[0][c6][c7][c8][0][t2];
            __syncthreads();
          }
}
