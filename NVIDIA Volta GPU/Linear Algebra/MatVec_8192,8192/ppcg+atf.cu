#include "matvec_row_major_n_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    float private_C[1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    if (26 * b0 + t0 <= 8191) {
      private_C[0] = C[26 * b0 + t0];
      for (int c1 = 0; c1 <= 8191; c1 += 6712)
        for (int c3 = 0; c3 <= ppcg_min(6711, -c1 + 8191); c3 += 1)
          private_C[0] += (A[(26 * b0 + t0) * 8192 + (c1 + c3)] * B[c1 + c3]);
      C[26 * b0 + t0] = private_C[0];
    }
}
