#include "matmul_batched_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_A[1][2][60];
    float private_C[1][1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0][0] = C[(b0 * 10 + (2 * b1 + t1)) * 500 + t2];
      for (int c3 = 0; c3 <= 63; c3 += 60) {
        if (t2 <= 59 && t2 + c3 <= 63)
          shared_A[0][t1][t2] = A[(b0 * 10 + (2 * b1 + t1)) * 64 + (t2 + c3)];
        __syncthreads();
        for (int c7 = 0; c7 <= ppcg_min(59, -c3 + 63); c7 += 1)
          private_C[0][0][0] += (shared_A[0][t1][c7] * B[(b0 * 64 + (c3 + c7)) * 500 + t2]);
        __syncthreads();
      }
      C[(b0 * 10 + (2 * b1 + t1)) * 500 + t2] = private_C[0][0][0];
    }
}
