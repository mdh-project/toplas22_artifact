#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[2][64];
    float private_C[1][1];

    {
      if (t1 <= 63)
        shared_A[t0][t1] = A[(2 * b0 + t0) * 64 + t1];
      __syncthreads();
      private_C[0][0] = C[(2 * b0 + t0) * 500 + (125 * b1 + t1)];
      for (int c5 = 0; c5 <= 63; c5 += 1)
        private_C[0][0] += (shared_A[t0][c5] * B[c5 * 500 + (125 * b1 + t1)]);
      C[(2 * b0 + t0) * 500 + (125 * b1 + t1)] = private_C[0][0];
    }
}
