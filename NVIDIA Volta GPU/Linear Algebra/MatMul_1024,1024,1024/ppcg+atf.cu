#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[8][120];
    float private_C[8][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[8 * b0 * 1024 + (128 * b1 + t1)];
      private_C[1][0] = C[(8 * b0 + 1) * 1024 + (128 * b1 + t1)];
      private_C[2][0] = C[(8 * b0 + 2) * 1024 + (128 * b1 + t1)];
      private_C[3][0] = C[(8 * b0 + 3) * 1024 + (128 * b1 + t1)];
      private_C[4][0] = C[(8 * b0 + 4) * 1024 + (128 * b1 + t1)];
      private_C[5][0] = C[(8 * b0 + 5) * 1024 + (128 * b1 + t1)];
      private_C[6][0] = C[(8 * b0 + 6) * 1024 + (128 * b1 + t1)];
      private_C[7][0] = C[(8 * b0 + 7) * 1024 + (128 * b1 + t1)];
      for (int c2 = 0; c2 <= 1023; c2 += 120) {
        if (t1 <= 119 && t1 + c2 <= 1023)
          for (int c3 = 0; c3 <= 7; c3 += 1)
            shared_A[c3][t1] = A[(8 * b0 + c3) * 1024 + (t1 + c2)];
        __syncthreads();
        for (int c3 = 0; c3 <= ppcg_min(119, -c2 + 1023); c3 += 1) {
          private_C[0][0] += (shared_A[0][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[1][0] += (shared_A[1][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[2][0] += (shared_A[2][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[3][0] += (shared_A[3][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[4][0] += (shared_A[4][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[5][0] += (shared_A[5][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[6][0] += (shared_A[6][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
          private_C[7][0] += (shared_A[7][c3] * B[(c2 + c3) * 1024 + (128 * b1 + t1)]);
        }
        __syncthreads();
      }
      C[8 * b0 * 1024 + (128 * b1 + t1)] = private_C[0][0];
      C[(8 * b0 + 1) * 1024 + (128 * b1 + t1)] = private_C[1][0];
      C[(8 * b0 + 2) * 1024 + (128 * b1 + t1)] = private_C[2][0];
      C[(8 * b0 + 3) * 1024 + (128 * b1 + t1)] = private_C[3][0];
      C[(8 * b0 + 4) * 1024 + (128 * b1 + t1)] = private_C[4][0];
      C[(8 * b0 + 5) * 1024 + (128 * b1 + t1)] = private_C[5][0];
      C[(8 * b0 + 6) * 1024 + (128 * b1 + t1)] = private_C[6][0];
      C[(8 * b0 + 7) * 1024 + (128 * b1 + t1)] = private_C[7][0];
    }
}
