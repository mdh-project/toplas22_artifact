#include "matmul_col_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[32][10];
    __shared__ float shared_B[32][32];
    float private_C[2][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[(32 * b1 + t1) * 10 + t0];
      if (32 * b1 + t1 <= 483)
        private_C[1][0] = C[(32 * b1 + t1 + 16) * 10 + t0];
      for (int c2 = 0; c2 <= 63; c2 += 32) {
        if (t1 <= 9)
          for (int c3 = t0; c3 <= 31; c3 += 10)
            shared_A[c3][t1] = A[(c2 + c3) * 10 + t1];
        for (int c3 = t0; c3 <= ppcg_min(31, -32 * b1 + 499); c3 += 10)
          for (int c4 = t1; c4 <= 31; c4 += 16)
            shared_B[c3][c4] = B[(32 * b1 + c3) * 64 + (c2 + c4)];
        __syncthreads();
        for (int c3 = 0; c3 <= 31; c3 += 1) {
          private_C[0][0] += (shared_A[c3][t0] * shared_B[t1][c3]);
          if (32 * b1 + t1 <= 483)
            private_C[1][0] += (shared_A[c3][t0] * shared_B[t1 + 16][c3]);
        }
        __syncthreads();
      }
      C[(32 * b1 + t1) * 10 + t0] = private_C[0][0];
      if (32 * b1 + t1 <= 483)
        C[(32 * b1 + t1 + 16) * 10 + t0] = private_C[1][0];
    }
}
