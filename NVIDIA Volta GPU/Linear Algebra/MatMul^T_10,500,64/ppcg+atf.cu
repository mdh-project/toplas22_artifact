#include "matmul_col_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[64][4];
    __shared__ float shared_B[4][64];
    float private_C[1][1];

    {
      if (4 * b0 + t1 <= 9)
        for (int c0 = t0; c0 <= 63; c0 += 4)
          shared_A[c0][t1] = A[c0 * 10 + (4 * b0 + t1)];
      for (int c1 = t1; c1 <= 63; c1 += 4)
        shared_B[t0][c1] = B[(4 * b1 + t0) * 64 + c1];
      __syncthreads();
      if (4 * b0 + t0 <= 9) {
        private_C[0][0] = C[(4 * b1 + t1) * 10 + (4 * b0 + t0)];
        for (int c5 = 0; c5 <= 63; c5 += 1)
          private_C[0][0] += (shared_A[c5][t0] * shared_B[t1][c5]);
        C[(4 * b1 + t1) * 10 + (4 * b0 + t0)] = private_C[0][0];
      }
    }
}
