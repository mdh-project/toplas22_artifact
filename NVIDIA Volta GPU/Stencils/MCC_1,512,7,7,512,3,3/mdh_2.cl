typedef struct { float values[3][3]; } weights_3x3;
__kernel void mcc_explicit_sum_3x3_nchw_kcrs_nkpq_static_2(__global float const * const restrict int_res, __global float * const restrict res_g, __global float * const restrict out) {
  const size_t i_wg_l_1 = get_group_id(0);
  const size_t i_wi_l_1 = get_local_id(0);
  const size_t i_wg_l_2 = get_group_id(1);
  const size_t i_wi_l_2 = get_local_id(1);
  const size_t i_wg_l_3 = (get_group_id(2) % (1));
  const size_t i_wi_l_3 = (get_local_id(2) % (1));
  const size_t i_wg_l_4 = (get_group_id(2) % (1 * 1) / (1));
  const size_t i_wi_l_4 = (get_local_id(2) % (1 * 1) / (1));
  const size_t i_wg_r_1 = (get_group_id(2) / (1 * 1));
  const size_t i_wi_r_1 = (get_local_id(2) / (1 * 1));
  __private float res_p[((5 / 1) / (5)) + (((1 * (5 / 1)) % (1 * (5)) / 1) > 0) + (((1 * (5 / 1)) % (1 * (5)) % 1) > 0)][5][((5 / 1) / (5)) + (((1 * (5 / 1)) % (1 * (5)) / 1) > 0) + (((1 * (5 / 1)) % (1 * (5)) % 1) > 0)][5][1][((8 / 8) / (1)) + (((8 * (8 / 8)) % (8 * (1)) / 8) > 0) + (((8 * (8 / 8)) % (8 * (1)) % 8) > 0)][1][((1 / 1) / (1)) + (((1 * (1 / 1)) % (1 * (1)) / 1) > 0) + (((1 * (1 / 1)) % (1 * (1)) % 1) > 0)][1];
  __local float l_reduction_mem[5][5][8][8][1];
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((1 / (1 * 1)) / (1 / 1)); ++l_step_l_1) {
    for (size_t l_step_l_2 = 0; l_step_l_2 < ((512 / (64 * 8)) / (8 / 8)); ++l_step_l_2) {
      for (size_t l_step_l_3 = 0; l_step_l_3 < ((5 / (1 * 1)) / (5 / 1)); ++l_step_l_3) {
        for (size_t l_step_l_4 = 0; l_step_l_4 < ((5 / (1 * 1)) / (5 / 1)); ++l_step_l_4) {
          size_t l_step_r_1 = 0;
          for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
            for (size_t p_step_l_2 = 0; p_step_l_2 < ((8 / 8) / (1)); ++p_step_l_2) {
              for (size_t p_step_l_3 = 0; p_step_l_3 < ((5 / 1) / (5)); ++p_step_l_3) {
                for (size_t p_step_l_4 = 0; p_step_l_4 < ((5 / 1) / (5)); ++p_step_l_4) {
                  size_t p_step_r_1 = 0;
                  for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                    for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                      for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (5); ++p_iteration_l_3) {
                        for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (5); ++p_iteration_l_4) {
                          size_t p_iteration_r_1 = 0;
                          res_p[p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)] = int_res[(((l_step_l_3 * (5 / 1) + (((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 1))) * 5 * 16 * 512 * 1 + (((l_step_l_4 * (5 / 1) + (((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 16 * 512 * 1 + (((l_step_r_1 * (16 / 8) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 8 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 8) * 8 + i_wg_r_1 * 8 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 8 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 8))) * 512 * 1 + (((l_step_l_2 * (8 / 8) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 8) * (64 * 8) + i_wg_l_2 * 8 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 8))) * 1 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))];
                        }
                      }
                    }
                  }
                  for (p_step_r_1 = 1; p_step_r_1 < ((16 / 8) / (1)); ++p_step_r_1) {
                    for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                      for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                        for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (5); ++p_iteration_l_3) {
                          for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (5); ++p_iteration_l_4) {
                            for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (1); ++p_iteration_r_1) {
                              res_p[p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)] += int_res[(((l_step_l_3 * (5 / 1) + (((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_3 * 1 + ((((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))) % 1))) * 5 * 16 * 512 * 1 + (((l_step_l_4 * (5 / 1) + (((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_4 * 1 + ((((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))) % 1))) * 16 * 512 * 1 + (((l_step_r_1 * (16 / 8) + (((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 8 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) / 8) * 8 + i_wg_r_1 * 8 + ((((p_step_r_1 * (1) + (((p_iteration_r_1) * 1 + 0)) / 1) * 8 + i_wi_r_1 * 1 + ((((p_iteration_r_1) * 1 + 0)) % 1))) % 8))) * 512 * 1 + (((l_step_l_2 * (8 / 8) + (((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) / 8) * (64 * 8) + i_wg_l_2 * 8 + ((((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))) % 8))) * 1 + (((l_step_l_1 * (1 / 1) + (((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))) % 1)))];
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
          {
            for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
              for (size_t p_step_l_2 = 0; p_step_l_2 < ((8 / 8) / (1)); ++p_step_l_2) {
                for (size_t p_step_l_3 = 0; p_step_l_3 < ((5 / 1) / (5)); ++p_step_l_3) {
                  for (size_t p_step_l_4 = 0; p_step_l_4 < ((5 / 1) / (5)); ++p_step_l_4) {
                    for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                      for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                        for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (5); ++p_iteration_l_3) {
                          for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (5); ++p_iteration_l_4) {
                            l_reduction_mem[((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))][((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))][(i_wi_r_1)][((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))] = res_p[p_step_l_3][((p_iteration_l_3) * 1 + 0)][p_step_l_4][((p_iteration_l_4) * 1 + 0)][(0)][p_step_l_2][((p_iteration_l_2) * 1 + 0)][p_step_l_1][((p_iteration_l_1) * 1 + 0)];
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            barrier(CLK_LOCAL_MEM_FENCE);
            size_t stride = 8 / 2;
            for (; stride > 0; stride /= 2) {
              if (i_wi_r_1 < stride) {
                for (size_t p_step_l_1 = 0; p_step_l_1 < ((1 / 1) / (1)); ++p_step_l_1) {
                  for (size_t p_step_l_2 = 0; p_step_l_2 < ((8 / 8) / (1)); ++p_step_l_2) {
                    for (size_t p_step_l_3 = 0; p_step_l_3 < ((5 / 1) / (5)); ++p_step_l_3) {
                      for (size_t p_step_l_4 = 0; p_step_l_4 < ((5 / 1) / (5)); ++p_step_l_4) {
                        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
                          for (size_t p_iteration_l_2 = 0; p_iteration_l_2 < (1); ++p_iteration_l_2) {
                            for (size_t p_iteration_l_3 = 0; p_iteration_l_3 < (5); ++p_iteration_l_3) {
                              for (size_t p_iteration_l_4 = 0; p_iteration_l_4 < (5); ++p_iteration_l_4) {
                                l_reduction_mem[((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))][((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))][(i_wi_r_1)][((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))] += l_reduction_mem[((p_step_l_3 * (5) + (((p_iteration_l_3) * 1 + 0)) / 1) * 1 + i_wi_l_3 * 1 + ((((p_iteration_l_3) * 1 + 0)) % 1))][((p_step_l_4 * (5) + (((p_iteration_l_4) * 1 + 0)) / 1) * 1 + i_wi_l_4 * 1 + ((((p_iteration_l_4) * 1 + 0)) % 1))][(i_wi_r_1 + stride)][((p_step_l_2 * (1) + (((p_iteration_l_2) * 1 + 0)) / 1) * 8 + i_wi_l_2 * 1 + ((((p_iteration_l_2) * 1 + 0)) % 1))][((p_step_l_1 * (1) + (((p_iteration_l_1) * 1 + 0)) / 1) * 1 + i_wi_l_1 * 1 + ((((p_iteration_l_1) * 1 + 0)) % 1))];
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              barrier(CLK_LOCAL_MEM_FENCE);
            }
            barrier(CLK_LOCAL_MEM_FENCE);
          }
          {
            for (size_t step = 0; step < (((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)) * ((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1))) / (1 * 8 * 1 * 1 * 8); ++step) {
              const size_t flat_index = ((((((get_local_id(2) % (1))) * (1) + ((get_local_id(2) % (1 * 1) / (1)))) * (8) + ((get_local_id(2) / (1 * 1)))) * (8) + (get_local_id(1))) * (1) + (get_local_id(0))) + step * (1 * 8 * 1 * 1 * 8);
              const size_t l_index_l_1 = flat_index / (((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)));
              const size_t l_index_l_2 = (flat_index / (((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)))) % ((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8));
              const size_t l_index_l_3 = (flat_index / (((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)))) % ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1));
              const size_t l_index_l_4 = flat_index % ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1));
              out[(((l_step_l_1 * (1 / 1) + (l_index_l_1) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((l_index_l_1) % 1))) * 512 * 5 * 5 + (((l_step_l_2 * (8 / 8) + (l_index_l_2) / 8) * (64 * 8) + i_wg_l_2 * 8 + ((l_index_l_2) % 8))) * 5 * 5 + (((l_step_l_3 * (5 / 1) + (l_index_l_3) / 1) * (1 * 1) + i_wg_l_3 * 1 + ((l_index_l_3) % 1))) * 5 + (((l_step_l_4 * (5 / 1) + (l_index_l_4) / 1) * (1 * 1) + i_wg_l_4 * 1 + ((l_index_l_4) % 1)))] =
                l_reduction_mem[l_index_l_3][l_index_l_4][(0)][l_index_l_2][l_index_l_1];
            }
            if (((((((get_local_id(2) % (1))) * (1) + ((get_local_id(2) % (1 * 1) / (1)))) * (8) + ((get_local_id(2) / (1 * 1)))) * (8) + (get_local_id(1))) * (1) + (get_local_id(0))) < (((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)) * ((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1))) % (1 * 8 * 1 * 1 * 8)) {
              const size_t flat_index = ((((((get_local_id(2) % (1))) * (1) + ((get_local_id(2) % (1 * 1) / (1)))) * (8) + ((get_local_id(2) / (1 * 1)))) * (8) + (get_local_id(1))) * (1) + (get_local_id(0))) + ((((((1 / 1) / (1)) * (1) + ((1 * (1 / 1)) % (1 * (1)) / 1)) * 1 + ((1 * (1 / 1)) % (1 * (1)) % 1)) * ((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1))) / (1 * 8 * 1 * 1 * 8)) * (1 * 8 * 1 * 1 * 8);
              const size_t l_index_l_1 = flat_index / (((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)));
              const size_t l_index_l_2 = (flat_index / (((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)) * ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)))) % ((((8 / 8) / (1)) * (1) + ((8 * (8 / 8)) % (8 * (1)) / 8)) * 8 + ((8 * (8 / 8)) % (8 * (1)) % 8));
              const size_t l_index_l_3 = (flat_index / (((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1)))) % ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1));
              const size_t l_index_l_4 = flat_index % ((((5 / 1) / (5)) * (5) + ((1 * (5 / 1)) % (1 * (5)) / 1)) * 1 + ((1 * (5 / 1)) % (1 * (5)) % 1));
              out[(((l_step_l_1 * (1 / 1) + (l_index_l_1) / 1) * (1 * 1) + i_wg_l_1 * 1 + ((l_index_l_1) % 1))) * 512 * 5 * 5 + (((l_step_l_2 * (8 / 8) + (l_index_l_2) / 8) * (64 * 8) + i_wg_l_2 * 8 + ((l_index_l_2) % 8))) * 5 * 5 + (((l_step_l_3 * (5 / 1) + (l_index_l_3) / 1) * (1 * 1) + i_wg_l_3 * 1 + ((l_index_l_3) % 1))) * 5 + (((l_step_l_4 * (5 / 1) + (l_index_l_4) / 1) * (1 * 1) + i_wg_l_4 * 1 + ((l_index_l_4) % 1)))] =
                l_reduction_mem[l_index_l_3][l_index_l_4][(0)][l_index_l_2][l_index_l_1];
            }
            barrier(CLK_LOCAL_MEM_FENCE);
          }
        }
      }
    }
  }
}
