#include "jacobi_kernel.hu"
__global__ void kernel0(float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    for (int c5 = t2; c5 <= 509; c5 += 470)
      out[(b0 * 510 + (2 * b1 + t1)) * 510 + c5] = (((((((2.0f * in[((b0 + 1) * 512 + (2 * b1 + t1 + 1)) * 512 + (c5 + 2)]) + (3.0f * in[((b0 + 1) * 512 + (2 * b1 + t1 + 1)) * 512 + c5])) + (4.0f * in[((b0 + 1) * 512 + (2 * b1 + t1 + 2)) * 512 + (c5 + 1)])) + (5.0f * in[((b0 + 1) * 512 + (2 * b1 + t1)) * 512 + (c5 + 1)])) + (6.0f * in[((b0 + 2) * 512 + (2 * b1 + t1 + 1)) * 512 + (c5 + 1)])) + (7.0f * in[(b0 * 512 + (2 * b1 + t1 + 1)) * 512 + (c5 + 1)])) - (8.0f * in[((b0 + 1) * 512 + (2 * b1 + t1 + 1)) * 512 + (c5 + 1)]));
}
