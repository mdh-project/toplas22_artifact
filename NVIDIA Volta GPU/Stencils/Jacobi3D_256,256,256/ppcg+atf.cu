#include "jacobi_kernel.hu"
__global__ void kernel0(float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;

    for (int c0 = 2 * b0; c0 <= 253; c0 += 248)
      for (int c3 = 0; c3 <= 1; c3 += 1)
        out[((c0 + c3) * 254 + (2 * b1 + t1)) * 254 + t2] = (((((((2.0f * in[((c0 + c3 + 1) * 256 + (2 * b1 + t1 + 1)) * 256 + (t2 + 2)]) + (3.0f * in[((c0 + c3 + 1) * 256 + (2 * b1 + t1 + 1)) * 256 + t2])) + (4.0f * in[((c0 + c3 + 1) * 256 + (2 * b1 + t1 + 2)) * 256 + (t2 + 1)])) + (5.0f * in[((c0 + c3 + 1) * 256 + (2 * b1 + t1)) * 256 + (t2 + 1)])) + (6.0f * in[((c0 + c3 + 2) * 256 + (2 * b1 + t1 + 1)) * 256 + (t2 + 1)])) + (7.0f * in[((c0 + c3) * 256 + (2 * b1 + t1 + 1)) * 256 + (t2 + 1)])) - (8.0f * in[((c0 + c3 + 1) * 256 + (2 * b1 + t1 + 1)) * 256 + (t2 + 1)]));
}
