#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[1][466];
    float private_C[1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      private_C[0][0] = C[b0 * 4096 + (32 * b1 + t1)];
      for (int c2 = 0; c2 <= 25087; c2 += 466) {
        for (int c4 = t1; c4 <= ppcg_min(465, -c2 + 25087); c4 += 32)
          shared_A[0][c4] = A[b0 * 25088 + (c2 + c4)];
        __syncthreads();
        for (int c5 = 0; c5 <= ppcg_min(465, -c2 + 25087); c5 += 1)
          private_C[0][0] += (shared_A[0][c5] * B[(c2 + c5) * 4096 + (32 * b1 + t1)]);
        __syncthreads();
      }
      C[b0 * 4096 + (32 * b1 + t1)] = private_C[0][0];
    }
}
