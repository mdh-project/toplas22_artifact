#include "mcc_nhwc_krsc_npqk_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_filter[32][3][3][3];

    for (int c2 = 0; c2 <= 223; c2 += 32)
      for (int c3 = 0; c3 <= 63; c3 += 32) {
        if (t0 <= 2 && t1 <= 2 && t2 <= 2)
          for (int c4 = 0; c4 <= 31; c4 += 1)
            shared_filter[c4][t0][t1][t2] = filter[(((c3 + c4) * 3 + t0) * 3 + t1) * 3 + t2];
        __syncthreads();
        for (int c6 = t1; c6 <= 31; c6 += 4)
          for (int c7 = t2; c7 <= 31; c7 += 4)
            for (int c8 = 0; c8 <= 31; c8 += 1)
              for (int c9 = 0; c9 <= 2; c9 += 1)
                for (int c10 = 0; c10 <= 2; c10 += 1)
                  for (int c11 = 0; c11 <= 2; c11 += 1)
                    out[((t0 * 224 + (32 * b1 + c6)) * 224 + (c2 + c7)) * 64 + (c3 + c8)] += (shared_filter[c8][c10][c11][c9] * in[((t0 * 226 + (32 * b1 + c6 + c10)) * 226 + (c2 + c7 + c11)) * 3 + c9]);
        __syncthreads();
      }
}
