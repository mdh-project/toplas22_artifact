#include "mcc_nhwc_krsc_npqk_stride_2_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_filter[12][3][3][3];
    __shared__ float shared_in[4][3][29][3];
    __shared__ float shared_out[4][1][14][12];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 111; c2 += 14) {
      if (t0 <= 2 && t2 <= 2)
        for (int c3 = 0; c3 <= 3; c3 += 1)
          for (int c5 = 0; c5 <= 28; c5 += 1)
            shared_in[c3][t0][c5][t2] = in[(((4 * b0 + c3) * 225 + (2 * b1 + t0)) * 225 + (2 * c2 + c5)) * 3 + t2];
      for (int c3 = 0; c3 <= 31; c3 += 12) {
        if (t0 <= 2 && t2 <= 2)
          for (int c4 = 0; c4 <= ppcg_min(11, -c3 + 31); c4 += 1)
            for (int c6 = 0; c6 <= 2; c6 += 1)
              shared_filter[c4][t0][c6][t2] = filter[(((c3 + c4) * 3 + t0) * 3 + c6) * 3 + t2];
        if (t0 == 0 && t2 <= 11 && t2 + c3 <= 31)
          for (int c4 = 0; c4 <= 3; c4 += 1)
            for (int c6 = 0; c6 <= 13; c6 += 1)
              shared_out[c4][0][c6][t2] = out[(((4 * b0 + c4) * 112 + b1) * 112 + (c2 + c6)) * 32 + (t2 + c3)];
        __syncthreads();
        for (int c8 = 0; c8 <= ppcg_min(11, -c3 + 31); c8 += 1)
          for (int c9 = 0; c9 <= 2; c9 += 1)
            for (int c10 = 0; c10 <= 2; c10 += 1)
              for (int c11 = 0; c11 <= 2; c11 += 1)
                shared_out[t0][0][t2][c8] += (shared_filter[c8][c10][c11][c9] * shared_in[t0][c10][2 * t2 + c11][c9]);
        __syncthreads();
        if (t0 == 0 && t2 <= 11 && t2 + c3 <= 31)
          for (int c4 = 0; c4 <= 3; c4 += 1)
            for (int c6 = 0; c6 <= 13; c6 += 1)
              out[(((4 * b0 + c4) * 112 + b1) * 112 + (c2 + c6)) * 32 + (t2 + c3)] = shared_out[c4][0][c6][t2];
      }
    }
}
