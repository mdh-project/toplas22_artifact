#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_A[8][88];
    __shared__ float shared_B[88][14];
    float private_C[1][1];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (14 * b1 + t1 <= 999)
        private_C[0][0] = C[(8 * b0 + t0) * 1000 + (14 * b1 + t1)];
      for (int c2 = 0; c2 <= 2047; c2 += 88) {
        for (int c4 = t1; c4 <= ppcg_min(87, -c2 + 2047); c4 += 14)
          shared_A[t0][c4] = A[(8 * b0 + t0) * 2048 + (c2 + c4)];
        if (14 * b1 + t1 <= 999)
          for (int c3 = t0; c3 <= ppcg_min(87, -c2 + 2047); c3 += 8)
            shared_B[c3][t1] = B[(c2 + c3) * 1000 + (14 * b1 + t1)];
        __syncthreads();
        if (14 * b1 + t1 <= 999)
          for (int c5 = 0; c5 <= ppcg_min(87, -c2 + 2047); c5 += 1)
            private_C[0][0] += (shared_A[t0][c5] * shared_B[c5][t1]);
        __syncthreads();
      }
      if (14 * b1 + t1 <= 999)
        C[(8 * b0 + t0) * 1000 + (14 * b1 + t1)] = private_C[0][0];
    }
}
