#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    __shared__ float shared_A[1][2048];
    float private_C[1][1];

    {
      for (int c1 = t0; c1 <= 2047; c1 += 28)
        shared_A[0][c1] = A[0 * 2048 + c1];
      __syncthreads();
      if (28 * b0 + t0 <= 999) {
        private_C[0][0] = C[0 * 1000 + (28 * b0 + t0)];
        for (int c3 = 0; c3 <= 2047; c3 += 1)
          private_C[0][0] += (shared_A[0][c3] * B[c3 * 1000 + (28 * b0 + t0)]);
        C[0 * 1000 + (28 * b0 + t0)] = private_C[0][0];
      }
    }
}
