#include <assert.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include "mcc_nhwc_krsc_npqk_stride_2_asymmetrical_kernel.hu"
int main() {
    unsigned long total_min_runtime = 0;
    #define N_VAL 16
    #define P_VAL 112
    #define Q_VAL 112
    #define K_VAL 64
    #define C_VAL 3
    #define R_VAL 7
    #define S_VAL 7

    static float in[N_VAL][2 * P_VAL + R_VAL - 1][2 * Q_VAL + S_VAL - 1][C_VAL];
    static float filter[K_VAL][R_VAL][S_VAL][C_VAL];
    static float out[N_VAL][P_VAL][Q_VAL][K_VAL];

    for (int i = 0; i < N_VAL * (2 * P_VAL + R_VAL - 1) * (2 * Q_VAL + S_VAL - 1) * C_VAL; ++i) ((float *)in)[i] = (i % 10) + 1;
    for (int i = 0; i < K_VAL * R_VAL * S_VAL * C_VAL; ++i) ((float *)filter)[i] = (i % 10) + 1;
    for (int i = 0; i < N_VAL * P_VAL * Q_VAL * K_VAL; ++i) ((float *)out)[i] = 0;

    {
#define cudaCheckReturn(ret) \
  do { \
    cudaError_t cudaCheckReturn_e = (ret); \
    if (cudaCheckReturn_e != cudaSuccess) { \
      fprintf(stderr, "CUDA error: %s\n", cudaGetErrorString(cudaCheckReturn_e)); \
      fflush(stderr); \
    } \
    assert(cudaCheckReturn_e == cudaSuccess); \
  } while(0)
    cudaCheckReturn(cudaSetDevice(0));
#define cudaCheckKernel() \
  do { \
    cudaCheckReturn(cudaGetLastError()); \
  } while(0)
    cudaCheckReturn(cudaSetDevice(0));

      float *dev_filter;
      float *dev_in;
      float *dev_out;
      
      cudaCheckReturn(cudaMalloc((void **) &dev_filter, (64) * (7) * (7) * (3) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_in, (16) * (230) * (230) * (3) * sizeof(float)));
      cudaCheckReturn(cudaMalloc((void **) &dev_out, (16) * (112) * (112) * (64) * sizeof(float)));
      
      cudaCheckReturn(cudaMemcpy(dev_filter, filter, (64) * (7) * (7) * (3) * sizeof(float), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_in, in, (16) * (230) * (230) * (3) * sizeof(float), cudaMemcpyHostToDevice));
      cudaCheckReturn(cudaMemcpy(dev_out, out, (16) * (112) * (112) * (64) * sizeof(float), cudaMemcpyHostToDevice));
      {
        dim3 k0_dimBlock(4, 4, 16);
        dim3 k0_dimGrid(4, 1);
        {
            cudaCheckReturn(cudaMemcpy(out, dev_out, (16) * (112) * (112) * (64) * sizeof(float), cudaMemcpyDeviceToHost));
            unsigned long min_runtime = ULONG_MAX;
            for (int i = 0; i < 10; ++i) {
                cudaCheckReturn(cudaMemcpy(dev_filter, filter, (64) * (7) * (7) * (3) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_in, in, (16) * (230) * (230) * (3) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_out, out, (16) * (112) * (112) * (64) * sizeof(float), cudaMemcpyHostToDevice));
                cudaEvent_t start, stop;
                cudaEventCreate(&start);
                cudaEventCreate(&stop);
                cudaEventRecord(start);
                kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_filter, dev_in, dev_out);
                cudaCheckKernel();
                cudaEventRecord(stop);
                cudaEventSynchronize(stop);
                float milliseconds = 0;
                cudaEventElapsedTime(&milliseconds, start, stop);
                unsigned long runtime = milliseconds * 1000000;
                if (runtime < min_runtime) min_runtime = runtime;
            }
            min_runtime = ULONG_MAX;
            for (int i = 0; i < 200; ++i) {
                cudaCheckReturn(cudaMemcpy(dev_filter, filter, (64) * (7) * (7) * (3) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_in, in, (16) * (230) * (230) * (3) * sizeof(float), cudaMemcpyHostToDevice));
cudaCheckReturn(cudaMemcpy(dev_out, out, (16) * (112) * (112) * (64) * sizeof(float), cudaMemcpyHostToDevice));
                cudaEvent_t start, stop;
                cudaEventCreate(&start);
                cudaEventCreate(&stop);
                cudaEventRecord(start);
                kernel0 <<<k0_dimGrid, k0_dimBlock>>> (dev_filter, dev_in, dev_out);
                cudaCheckKernel();
                cudaEventRecord(stop);
                cudaEventSynchronize(stop);
                float milliseconds = 0;
                cudaEventElapsedTime(&milliseconds, start, stop);
                unsigned long runtime = milliseconds * 1000000;
                if (runtime < min_runtime) min_runtime = runtime;
            }
            total_min_runtime += min_runtime;
        }
        cudaCheckKernel();
      }
      
      cudaCheckReturn(cudaMemcpy(out, dev_out, (16) * (112) * (112) * (64) * sizeof(float), cudaMemcpyDeviceToHost));
      cudaCheckReturn(cudaFree(dev_filter));
      cudaCheckReturn(cudaFree(dev_in));
      cudaCheckReturn(cudaFree(dev_out));
    }

#ifdef WRITE_GOLD
    WRITE_GOLD
#endif
    printf("%lu", total_min_runtime);
}
