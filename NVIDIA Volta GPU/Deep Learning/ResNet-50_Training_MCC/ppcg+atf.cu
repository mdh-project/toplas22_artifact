#include "mcc_nhwc_krsc_npqk_stride_2_asymmetrical_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.z, t1 = threadIdx.y, t2 = threadIdx.x;
    __shared__ float shared_out[1][1][56][33];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c0 = b0; c0 <= 15; c0 += 15)
      for (int c2 = 0; c2 <= 111; c2 += 56)
        for (int c3 = 0; c3 <= 63; c3 += 33) {
          if (t2 <= 32 && t2 + c3 <= 63)
            for (int c6 = 0; c6 <= 55; c6 += 1)
              shared_out[0][0][c6][t2] = out[((c0 * 112 + b1) * 112 + (c2 + c6)) * 64 + (t2 + c3)];
          __syncthreads();
          for (int c8 = 0; c8 <= ppcg_min(32, -c3 + 63); c8 += 1)
            for (int c9 = 0; c9 <= 2; c9 += 1)
              for (int c10 = 0; c10 <= 6; c10 += 1)
                for (int c11 = 0; c11 <= 6; c11 += 1)
                  shared_out[0][0][t2][c8] += (filter[(((c3 + c8) * 7 + c10) * 7 + c11) * 3 + c9] * in[((c0 * 230 + (2 * b1 + c10)) * 230 + (2 * t2 + 2 * c2 + c11)) * 3 + c9]);
          __syncthreads();
          if (t2 <= 32 && t2 + c3 <= 63)
            for (int c6 = 0; c6 <= 55; c6 += 1)
              out[((c0 * 112 + b1) * 112 + (c2 + c6)) * 64 + (t2 + c3)] = shared_out[0][0][c6][t2];
        }
}
