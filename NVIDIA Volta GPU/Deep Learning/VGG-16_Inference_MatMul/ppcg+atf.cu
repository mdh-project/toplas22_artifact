#include "matmul_row_major_nn_kernel.hu"
__global__ void kernel0(float *A, float *B, float *C)
{
    int b0 = blockIdx.x;
    int t0 = threadIdx.x;
    __shared__ float shared_A[1][896];
    float private_C[1][1];

    {
      if (28 * b0 + t0 <= 4095)
        private_C[0][0] = C[0 * 4096 + (28 * b0 + t0)];
      for (int c1 = 0; c1 <= 25087; c1 += 896) {
        for (int c3 = t0; c3 <= 895; c3 += 28)
          shared_A[0][c3] = A[0 * 25088 + (c1 + c3)];
        __syncthreads();
        if (28 * b0 + t0 <= 4095)
          for (int c3 = 0; c3 <= 895; c3 += 1)
            private_C[0][0] += (shared_A[0][c3] * B[(c1 + c3) * 4096 + (28 * b0 + t0)]);
        __syncthreads();
      }
      if (28 * b0 + t0 <= 4095)
        C[0 * 4096 + (28 * b0 + t0)] = private_C[0][0];
    }
}
