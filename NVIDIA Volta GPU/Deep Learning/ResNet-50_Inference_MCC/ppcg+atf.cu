#include "mcc_nhwc_krsc_npqk_stride_2_asymmetrical_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    float private_out[1][1][1][26];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    for (int c2 = 0; c2 <= 63; c2 += 26) {
      private_out[0][0][0][0] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + c2];
      private_out[0][0][0][1] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 1)];
      private_out[0][0][0][2] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 2)];
      private_out[0][0][0][3] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 3)];
      private_out[0][0][0][4] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 4)];
      private_out[0][0][0][5] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 5)];
      private_out[0][0][0][6] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 6)];
      private_out[0][0][0][7] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 7)];
      private_out[0][0][0][8] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 8)];
      private_out[0][0][0][9] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 9)];
      private_out[0][0][0][10] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 10)];
      private_out[0][0][0][11] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 11)];
      if (c2 <= 26) {
        private_out[0][0][0][12] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 12)];
        private_out[0][0][0][13] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 13)];
        private_out[0][0][0][14] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 14)];
        private_out[0][0][0][15] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 15)];
        private_out[0][0][0][16] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 16)];
        private_out[0][0][0][17] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 17)];
        private_out[0][0][0][18] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 18)];
        private_out[0][0][0][19] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 19)];
        private_out[0][0][0][20] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 20)];
        private_out[0][0][0][21] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 21)];
        private_out[0][0][0][22] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 22)];
        private_out[0][0][0][23] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 23)];
        private_out[0][0][0][24] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 24)];
        private_out[0][0][0][25] = out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 25)];
      }
      for (int c6 = 0; c6 <= ppcg_min(25, -c2 + 63); c6 += 1)
        for (int c7 = 0; c7 <= 2; c7 += 1)
          for (int c8 = 0; c8 <= 6; c8 += 1)
            for (int c9 = 0; c9 <= 6; c9 += 1)
              private_out[0][0][0][c6] += (filter[(((c2 + c6) * 7 + c8) * 7 + c9) * 3 + c7] * in[((0 * 230 + (2 * b0 + c8)) * 230 + (112 * b1 + 2 * t1 + c9)) * 3 + c7]);
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + c2] = private_out[0][0][0][0];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 1)] = private_out[0][0][0][1];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 2)] = private_out[0][0][0][2];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 3)] = private_out[0][0][0][3];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 4)] = private_out[0][0][0][4];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 5)] = private_out[0][0][0][5];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 6)] = private_out[0][0][0][6];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 7)] = private_out[0][0][0][7];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 8)] = private_out[0][0][0][8];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 9)] = private_out[0][0][0][9];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 10)] = private_out[0][0][0][10];
      out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 11)] = private_out[0][0][0][11];
      if (c2 <= 26) {
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 12)] = private_out[0][0][0][12];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 13)] = private_out[0][0][0][13];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 14)] = private_out[0][0][0][14];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 15)] = private_out[0][0][0][15];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 16)] = private_out[0][0][0][16];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 17)] = private_out[0][0][0][17];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 18)] = private_out[0][0][0][18];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 19)] = private_out[0][0][0][19];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 20)] = private_out[0][0][0][20];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 21)] = private_out[0][0][0][21];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 22)] = private_out[0][0][0][22];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 23)] = private_out[0][0][0][23];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 24)] = private_out[0][0][0][24];
        out[((0 * 112 + b0) * 112 + (56 * b1 + t1)) * 64 + (c2 + 25)] = private_out[0][0][0][25];
      }
    }
}
