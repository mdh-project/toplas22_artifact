#include "mcc_nhwc_krsc_npqk_stride_1_kernel.hu"
__global__ void kernel0(float *filter, float *in, float *out)
{
    int b0 = blockIdx.y, b1 = blockIdx.x;
    int t0 = threadIdx.y, t1 = threadIdx.x;
    __shared__ float shared_filter[4][3][3][3];
    __shared__ float shared_in[1][5][12][3];
    float private_out[1][1][1][4];

    #define ppcg_min(x,y)    ({ __typeof__(x) _x = (x); __typeof__(y) _y = (y); _x < _y ? _x : _y; })
    {
      if (t1 <= 2)
        for (int c1 = 0; c1 <= ppcg_min(4, -3 * b0 + 225); c1 += 1)
          for (int c2 = t0; c2 <= ppcg_min(11, -10 * b1 + 225); c2 += 3)
            shared_in[0][c1][c2][t1] = in[((0 * 226 + (3 * b0 + c1)) * 226 + (10 * b1 + c2)) * 3 + t1];
      for (int c2 = 0; c2 <= 63; c2 += 4) {
        if (t1 <= 2)
          for (int c3 = 0; c3 <= 3; c3 += 1)
            for (int c4 = 0; c4 <= 2; c4 += 1)
              shared_filter[c3][c4][t0][t1] = filter[(((c2 + c3) * 3 + c4) * 3 + t0) * 3 + t1];
        __syncthreads();
        if (3 * b0 + t0 <= 223 && 10 * b1 + t1 <= 223) {
          private_out[0][0][0][0] = out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + c2];
          private_out[0][0][0][1] = out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 1)];
          private_out[0][0][0][2] = out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 2)];
          private_out[0][0][0][3] = out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 3)];
          for (int c6 = 0; c6 <= 3; c6 += 1)
            for (int c7 = 0; c7 <= 2; c7 += 1)
              for (int c8 = 0; c8 <= 2; c8 += 1)
                for (int c9 = 0; c9 <= 2; c9 += 1)
                  private_out[0][0][0][c6] += (shared_filter[c6][c8][c9][c7] * shared_in[0][t0 + c8][t1 + c9][c7]);
          out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + c2] = private_out[0][0][0][0];
          out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 1)] = private_out[0][0][0][1];
          out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 2)] = private_out[0][0][0][2];
          out[((0 * 224 + (3 * b0 + t0)) * 224 + (10 * b1 + t1)) * 64 + (c2 + 3)] = private_out[0][0][0][3];
        }
        __syncthreads();
      }
    }
}
