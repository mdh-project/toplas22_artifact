inline void combine_in_p_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __global long const * const match_id_res_b,
              __global double const * const match_weight_res_b,
              __global int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void g_combine_in_p_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __global long const * const match_id_res_b,
              __global double const * const match_weight_res_b,
              __global int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void l_combine_in_p_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __local long const * const match_id_res_b,
              __local double const * const match_weight_res_b,
              __local int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void p_combine_in_p_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __private long const * const match_id_res_b,
              __private double const * const match_weight_res_b,
              __private int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void combine_in_l_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __global long const * const match_id_res_b,
              __global double const * const match_weight_res_b,
              __global int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void p_combine_in_l_cb(__global long * const match_id_res_a,
                __global double * const match_weight_res_a,
                __global int * const id_measure_res_a,
                __global long const * const match_id_res_b,
                __global double const * const match_weight_res_b,
                __global int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void combine_in_g_cb(__global long * const match_id_res_a,
              __global double * const match_weight_res_a,
              __global int * const id_measure_res_a,
              __global long const * const match_id_res_b,
              __global double const * const match_weight_res_b,
              __global int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
inline void combine_in_l_mem(__local long * const match_id_res_a,
              __local double * const match_weight_res_a,
              __local int * const id_measure_res_a,
              __local long const * const match_id_res_b,
              __local double const * const match_weight_res_b,
              __local int const * const id_measure_res_b) {
  if (((*id_measure_res_a) != 14 && (*id_measure_res_b) == 14)
    || (*match_weight_res_b) > (*match_weight_res_a)) {
    *match_id_res_a = *match_id_res_b;
    *match_weight_res_a = *match_weight_res_b;
    *id_measure_res_a = *id_measure_res_b;
  }
}
__kernel void prl_static_2(__global long const * const restrict match_id_int_res,
                   __global double const * const restrict match_weight_int_res,
                   __global int const * const restrict id_measure_int_res,
                   __global long * const restrict match_id_res_g,
                   __global double * const restrict match_weight_res_g,
                   __global int * const restrict id_measure_res_g,
                   __global long * const restrict match_id_result,
                   __global double * const restrict match_weight_result,
                   __global int * const restrict id_measure_result) {
  const size_t i_wg_l_1 = get_group_id(0);
  const size_t i_wi_l_1 = get_local_id(0);
  const size_t i_wg_r_1 = get_group_id(1);
  const size_t i_wi_r_1 = get_local_id(1);
  size_t l_cb_offset_l_1;
  size_t l_cb_offset_r_1;
  size_t p_cb_offset_l_1;
  size_t p_cb_offset_r_1;
  long tmp_match_id;
  double tmp_match_weight;
  int tmp_id_measure;
  l_cb_offset_l_1 = i_wg_l_1 * 256;
  for (size_t l_step_l_1 = 0; l_step_l_1 < ((32768 / (64 * 256)) / (512 / 256)); ++l_step_l_1) {
    l_cb_offset_r_1 = i_wg_r_1 * 1;
    size_t l_step_r_1 = 0;
    p_cb_offset_l_1 = i_wi_l_1 * 1;
    for (size_t p_step_l_1 = 0; p_step_l_1 < ((512 / 256) / (1)); ++p_step_l_1) {
      p_cb_offset_r_1 = i_wi_r_1 * 1;
      size_t p_step_r_1 = 0;
      for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
        size_t p_iteration_r_1 = 0;
        match_id_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))] = match_id_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))];
        match_weight_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))] = match_weight_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))];
        id_measure_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))] = id_measure_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))];
        for (p_iteration_r_1 = 1; p_iteration_r_1 < (128); ++p_iteration_r_1) {
          g_combine_in_p_cb(&match_id_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                            &match_weight_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                            &id_measure_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                            &match_id_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))],
                            &match_weight_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))],
                            &id_measure_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))]
          );
        }
      }
      p_cb_offset_r_1 += 1 * (128);
      p_cb_offset_l_1 += 256 * (1);
    }
    l_cb_offset_r_1 += 1 * (128 / 1);
    for (l_step_r_1 = 1; l_step_r_1 < ((256 / 1) / (128 / 1)); ++l_step_r_1) {
      p_cb_offset_l_1 = i_wi_l_1 * 1;
      for (size_t p_step_l_1 = 0; p_step_l_1 < ((512 / 256) / (1)); ++p_step_l_1) {
        p_cb_offset_r_1 = i_wi_r_1 * 1;
        size_t p_step_r_1 = 0;
        for (size_t p_iteration_l_1 = 0; p_iteration_l_1 < (1); ++p_iteration_l_1) {
          for (size_t p_iteration_r_1 = 0; p_iteration_r_1 < (128); ++p_iteration_r_1) {
            g_combine_in_p_cb(&match_id_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                              &match_weight_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                              &id_measure_result[((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + 0)) / 256) * (64 * 256) + i_wi_l_1))],
                              &match_id_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))],
                              &match_weight_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))],
                              &id_measure_int_res[((l_cb_offset_r_1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) / 1) * 1 + (((p_cb_offset_r_1 + (((p_iteration_r_1)) / 1) * 1 + (((p_iteration_r_1)) % 1))) % 1))) * 32768 + ((l_cb_offset_l_1 + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) / 256) * (64 * 256) + (((p_cb_offset_l_1 + (((p_iteration_l_1)) / 1) * 256 + (((p_iteration_l_1)) % 1))) % 256)))]
            );
          }
        }
        p_cb_offset_r_1 += 1 * (128);
        p_cb_offset_l_1 += 256 * (1);
      }
      l_cb_offset_r_1 += 1 * (128 / 1);
    }
    l_cb_offset_l_1 += (64 * 256) * (512 / 256);
  }
}
